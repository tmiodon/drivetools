from utilsLibrary import Utils


def SYST_1102(drive1):
    # Clearing faults
    drive1.ClearFault(0)
    drive1.ClearFaultQueue(0)

    drive1.ClearAlarm(0)
    drive1.ClearAlarmQueue(0)

    # Test configuration
    drive1.WriteParameter(694, "P10 Motor NP Amps")
    drive1.WriteParameter(650, "P10 Motor NP Power")
    drive1.WriteParameter(460, "P10 Motor NP Volts")
    drive1.WriteParameter(60, "P10 Motor NP Hertz")
    drive1.WriteParameter(1785, "P10 Motor NP RPM")
    drive1.WriteParameter(4, "P10 Motor Poles")
    drive1.WriteParameter(0, "P10 Mtr NP Pwr Units")
#    drive1.WriteParameter(1, "P0 Pri MtrCtrl Mode")
    drive1.WriteParameter(101801, "P10 VRef A Sel")
    drive1.WriteParameter(1, "P10 Direction Mode")
    drive1.WriteParameter(1, "P10 FluxUpTm C/U Sel")

    drive1.WriteParameter(10, "P10 VRef A Stpt")
    drive1.VerifyParameterValue(0, "P10 Output Frequency", tolerance=0.5)
    driveStarted = drive1.Start()
    Utils.VerifyValue(driveStarted, True)
    Utils.Wait(20)
    drive1.VerifyParameterValue(10, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(45, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(45, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(60, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(60, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(-10, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-10, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(-45, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-45, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(-60, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-60, "P10 Output Frequency", tolerance=0.5)

    drive1.Stop()
    Utils.Wait(20)