2019_03_22__13_04_56. - TEST INFO AND RESULTS

Sample number = 1000

Type of message 	| Mean 		| Median 	| Min 		| Max
--------------------+-----------+-----------+-----------+-----------
Read Parameter 		| 0.004352 	| 0.003372 	| 0.002837 	| 0.017392
Write Parameter 	| 0.007295 	| 0.006114 	| 0.001067 	| 0.021443
Pccc Command 		| 0.005022 	| 0.004863 	| 0.003828 	| 0.015894
Pccc Status 		| 0.002460 	| 0.002383 	| 0.001940 	| 0.010350
