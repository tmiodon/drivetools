2019_03_22__12_30_14. - TEST INFO AND RESULTS

Sample number = 100

Type of message 	| Mean 		| Median 	| Min 		| Max
--------------------+-----------+-----------+-----------+-----------
Read Parameter 		| 0.004915 	| 0.003977 	| 0.003128 	| 0.016615
Write Parameter 	| 0.007677 	| 0.006626 	| 0.005716 	| 0.018658
Pccc Command 		| 0.005903 	| 0.005568 	| 0.003902 	| 0.015798
Pccc Status 		| 0.002723 	| 0.002713 	| 0.001922 	| 0.006573
