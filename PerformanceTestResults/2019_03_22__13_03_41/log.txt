2019_03_22__13_03_41. - TEST INFO AND RESULTS

Sample number = 1000

Type of message 	| Mean 		| Median 	| Min 		| Max
--------------------+-----------+-----------+-----------+-----------
Read Parameter 		| 0.004072 	| 0.003220 	| 0.002744 	| 0.159378
Write Parameter 	| 0.007187 	| 0.006034 	| 0.002864 	| 0.031383
Pccc Command 		| 0.004922 	| 0.004698 	| 0.003799 	| 0.027824
Pccc Status 		| 0.002544 	| 0.002288 	| 0.001792 	| 0.020209
