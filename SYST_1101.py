from utilsLibrary import Utils


def SYST_1101(drive1):
    # Clearing faults
    drive1.ClearFault(0)
    drive1.ClearFaultQueue(0)
    # Clearing alarms
    drive1.ClearAlarm(0)
    drive1.ClearAlarmQueue(0)

    # Set speed reference
    drive1.WriteParameter(101801, "P10 VRef A Sel")
    drive1.WriteParameter(30, "P10 VRef A Stpt")
    # Verify if motor not running
    drive1.VerifyParameterBitValue(0, 16, "P10 Motor Side Sts 1")
    # Verify if motor not stopping
    drive1.VerifyParameterBitValue(0, 18, "P10 Motor Side Sts 1")
    # Verify if motor not at speed
    drive1.VerifyParameterBitValue(0, 8, "P10 Motor Side Sts 1")
    # Verify iw output frequency is 0hz
    drive1.VerifyParameterValue(0, "P10 Output Frequency", tolerance = 1)

    # Start drive
    drive1.Start()
    Utils.Wait(20)

    # Verify if motor running
    drive1.VerifyParameterBitValue(1, 16, "P10 Motor Side Sts 1")
    # Verify if motor not stopping
    drive1.VerifyParameterBitValue(0, 18, "P10 Motor Side Sts 1")
    # Verify if motor at speed
    drive1.VerifyParameterBitValue(1, 8, "P10 Motor Side Sts 1")
    # Verify iw output frequency is 30hz
    drive1.VerifyParameterValue(30, "P10 Output Frequency", tolerance = 0.5)

    # Verify if AtReference bit is set
    drive1.VerifyParameterBitValue(1, 8, "P10 Motor Side Sts 1")

    # Stop the drive
    drive1.Stop()
    drive1.WaitTillParameterHasSteadyValue("P10 Output Frequency")

    # Verify if motor not running
    drive1.VerifyParameterBitValue(0, 16, "P10 Motor Side Sts 1")
    # Verify if motor not stopping
    drive1.VerifyParameterBitValue(0, 18, "P10 Motor Side Sts 1")
    # Verify if motor not at speed
    drive1.VerifyParameterBitValue(0, 8, "P10 Motor Side Sts 1")
    # Verify iw output frequency is 0hz
    drive1.VerifyParameterValue(0, "P10 Output Frequency", tolerance = 1)
    # Verify if AtReference bit is cleared
    drive1.VerifyParameterBitValue(0, 8, "P10 Motor Side Sts 1")

    # Wait
    Utils.Wait(10)

    # Verify if motor not running
    drive1.VerifyParameterBitValue(0, 16, "P10 Motor Side Sts 1")
    # Verify if motor not stopping
    drive1.VerifyParameterBitValue(0, 18, "P10 Motor Side Sts 1")
    # Verify if motor not at speed
    drive1.VerifyParameterBitValue(0, 8, "P10 Motor Side Sts 1")
    # Verify iw output frequency is 0hz
    drive1.VerifyParameterValue(0, "P10 Output Frequency", tolerance = 1)
    # Verify if AtReference bit is cleared
    drive1.VerifyParameterBitValue(0, 8, "P10 Motor Side Sts 1")