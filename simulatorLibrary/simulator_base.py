# -------------------------------------------------------------------
#
# FILE NAME:    simulator_base.py
# VERSION:      0.1
# CONTRIBUTORS: Tomasz Miodonski
# START DATE:   12/18/2018
#
# DESCRIPTION:  Definitions of parent simulator class and child
#               classes specific for different simulators.
# NOTES:
#
# -------------------------------------------------------------------

import struct
import socket

import loggingLibrary.loggingdt as logging
import Configuration.TestEnvironment as TestEnviornment


# ---------------< Parent simulator class >-------------
class SimulatorBase:
    """
    Base class for Smart Daughter Card (SDC) family

    This class provides common functionalities to all SDC types. This includes
    connection management and methods related to sim board.

    """

    # ---------------< Socket data >-------------
    SIM_TCP_PORT = 6443
    SIM_TCP_BUFFER = 512
    # ---------------------------------------------------------------

    # ---------------< Primary instruction - param0 >-------------
    # Data set command
    SIM_SET = 100
    # Data get command
    SIM_GET = 101
    # Special function command
    SIM_SFC = 103
    # ---------------------------------------------------------------

    # ---------------< ENET SET/GET parameters - param1 >-------------
    SIM_FW_INFO = 1000
    SIM_IPV_INFO = 1001
    SIM_BB_INFO = 1002
    SIM_OP_INFO = 1003
    # ---------------------------------------------------------------




    # ---------------< Method: gets info on firmware of simulator >-------------
    def GetFirmwareInfo(self):
        """
        Gets firmware revision information.

        Received firmware information contains major, minor and build number of
        boot & application code as well as CRC number for both.

        Attributes:
            None

        Returns:
            :_firmware_data:    Dictionary containing firmware boot revision and application revision.
                                It containf following entries:

                                * _firmware_data['Boot'] = boot revision (str)
                                * _firmware_data['Application'] = application revision (str)

        """

        param0 = self.SIM_GET
        param1 = self.SIM_FW_INFO
        param2 = 0
        param3 = 0

        try:
            packed_data = struct.pack('LLLL', param0, param1, param2, param3)
            self.s.sendall(packed_data)
            data = self.s.recv(self.SIM_TCP_BUFFER)
            logging.Info("Firmware data received.")
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not received. Error message: " + exc)
            TestEnviornment.RESULT = False
            return
        fmt = 'LLLLLLLL'
        firmware_data = struct.unpack(fmt, data)

        boot_bits = '{0:032b}'.format(firmware_data[4])
        boot_major = int(boot_bits[24:], 2)
        boot_minor = int(boot_bits[16:24], 2)
        boot_build = int(boot_bits[:16], 2)
        boot_revision = str(boot_major) + '.' + str(boot_minor) + '.' + str(boot_build)

        application_bits = '{0:032b}'.format(firmware_data[6])
        application_major = int(application_bits[24:], 2)
        application_minor = int(application_bits[16:24], 2)
        application_build = int(application_bits[:16], 2)
        application_revision = str(application_major) + '.' + str(application_minor) + '.' + str(application_build)
        _firmware_data = {}
        _firmware_data['Boot'] = boot_revision
        _firmware_data['Application'] = application_revision
        return _firmware_data

    # ---------------< >-------------
    def GetIPConf(self):
        """
        Gets Ethernet IP configuration info

        Extracts information on IP configuration of SDC card. This includes IP address, gateway address,
        network mask, port number, MAC address

        Attributes:
            None
        Returns:
            :_ipv_data: Dictionary containing IP configuration data.
                        It contains following entries:

                        * _ipv_data['IP'] = IP address (str)
                        * _ipv_data['Gateway'] = Gateway address (str)
                        * _ipv_data['Mask'] = Network mask (str)
                        * _ipv_data['Port'] = Port number (str)
                        * _ipv_data['Name'] = Name (str)
                        * _ipv_data['MAC'] = MAC address (str)

        """

        _ipv_data = {}

        param0 = self.SIM_GET
        param1 = self.SIM_IPV_INFO
        param2 = 0
        param3 = 0

        try:
            packed_data = struct.pack('LLLL', param0, param1, param2, param3)
            self.s.sendall(packed_data)
            data = self.s.recv(self.SIM_TCP_BUFFER)
            logging.Info("IP config data received.")
            TestEnviornment.RESULT = True
        except:
            logging.Error("Data not received.")
            TestEnviornment.RESULT = False
            return

        fmt = 'LLLLLLLLLLLLLLLLLLLLL'
        ipv_data = struct.unpack(fmt, data)

        ip_bits = '{0:032b}'.format(ipv_data[4])
        first_octet = int(ip_bits[24:], 2)
        second_octet = int(ip_bits[16:24], 2)
        third_octet = int(ip_bits[8:16], 2)
        fourth_octet = int(ip_bits[:8], 2)
        ip_address = str(first_octet) + '.' + str(second_octet) + '.' + str(third_octet) + '.' + str(fourth_octet)
        _ipv_data["IP"] = ip_address

        gate_bits = '{0:032b}'.format(ipv_data[5])
        first_octet = int(gate_bits[24:], 2)
        second_octet = int(gate_bits[16:24], 2)
        third_octet = int(gate_bits[8:16], 2)
        fourth_octet = int(gate_bits[:8], 2)
        gate_address = str(first_octet) + '.' + str(second_octet) + '.' + str(third_octet) + '.' + str(fourth_octet)
        _ipv_data["Gateway"] = gate_address

        mask_bits = '{0:032b}'.format(ipv_data[6])
        first_octet = int(mask_bits[24:], 2)
        second_octet = int(mask_bits[16:24], 2)
        third_octet = int(mask_bits[8:16], 2)
        fourth_octet = int(mask_bits[:8], 2)
        mask_address = str(first_octet) + '.' + str(second_octet) + '.' + str(third_octet) + '.' + str(fourth_octet)
        _ipv_data["Mask"] = mask_address

        _ipv_data["Port"] = ipv_data[7]
        _ipv_data["Name"] = data[32:41]
        _ipv_data["MAC"] = data[64:81]

        return _ipv_data

    # ---------------< >-------------
    def GetBBConf(self):
        """
        Gets base board configuration info.

        -placeholder for extended description-

        Attributes:
            None
        Returns:
            :placeholder:
        """

        _bb_data = {}

        param0 = self.SIM_GET
        param1 = self.SIM_BB_INFO
        param2 = 0
        param3 = 0

        try:
            packed_data = struct.pack('LLLL', param0, param1, param2, param3)
            self.s.sendall(packed_data)
            data = self.s.recv(self.SIM_TCP_BUFFER)
            logging.Info("Base board config data received.")
            TestEnviornment.RESULT = True
        except:
            logging.Error("Data not received.")
            TestEnviornment.RESULT = False
            return

        fmt = 'LLLLbbbLL'
        bb_data = struct.unpack(fmt, data)

        _bb_data['Hw Version'] = bb_data[4]
        _bb_data['BB ID'] = bb_data[5]
        if bb_data[6] == 0:
            _bb_data['Voltage Class'] = '480V'
        if bb_data[6] == 1:
            _bb_data['Voltage Class'] = '690V'
        _bb_data['Frame Rating ID'] = bb_data[7]
        if bb_data[8] == 0:
            _bb_data['Drive Type'] = 'Single Drive'
        if bb_data[8] == 1:
            _bb_data['Drive Type'] = 'Multi Drive'
        
        return _bb_data
       
    # ---------------< >-------------
    def GetOPMode(self):
        """
        Gets current eSIM operation mode.

        Extracts information on operation mode of SDC card. SDC can be either on Service Mode or Application Mode.
        This is determined by jumper on board.

        Attributes:
            None
        Returns:
            :_op_data:  String containing info on operation mode.
                        Possible values;

                        * _op_data = 'Service Mode'
                        * _op_data = 'Application mode'

        """

        param0 = self.SIM_GET
        param1 = self.SIM_OP_INFO
        param2 = 0
        param3 = 0

        try:
            packed_data = struct.pack('LLLL', param0, param1, param2, param3)
            self.s.sendall(packed_data)
            data = self.s.recv(self.SIM_TCP_BUFFER)
            logging.Info("Operation mode received.")
            TestEnviornment.RESULT = True
        except:
            logging.Error("Data not received.")
            TestEnviornment.RESULT = False
            return

        fmt = 'LLLLL'
        op_data = struct.unpack(fmt, data)

        if op_data[4] == 0:
            _op_data = 'Service Mode'
        if op_data[4] == 1:
            _op_data = 'Application Mode'
        return _op_data


    # ---------------< Method: gets value of DC bus set in simulator >-------------
    # This needs rework; only DC bus value is needed as output, not all data



