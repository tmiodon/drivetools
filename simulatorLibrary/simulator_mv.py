#####################################################################
#
# FILE NAME:    simulator_mv.py
# VERSION:      0.1
# CONTRIBUTORS: Tomasz Miodonski
# START DATE:   05/20/2019
#
# DESCRIPTION:  Definitions of child class for medium voltage HPC drive
# NOTES:
#
#####################################################################

import socket
import struct

import loggingLibrary.loggingdt as logging
import Configuration.TestEnvironment as TestEnviornment
from simulator_base import SimulatorBase

#####################################################################
class MVSimulator(SimulatorBase):
    """
    PF6000T MV drive simulator class.

    """

    #####################################################################
    SIM_NTC_INFO = 1004
    SIM_FAN_INFO = 1005
    SIM_BUS_INFO = 1006
    SIM_OFST_INFO = 1007
    SIM_IFDBK_GAIN_INFO = 1008
    SIM_GATEID_INFO = 1009
    SIM_DIGIN_INFO = 1010
    SIM_DIGOUT_INFO = 1011
    SIM_AUXANAL_INFO = 1012
    SIM_ALL_INFO = 1013
    SIM_AC_INFO = 1020

    def __init__(self, ipAddress):
        """
        Initializes simulator instance. Connects at the same time

        Creates socket on port 6443 & ipAddress. This enables sending commands to simulator
        writing and reading available parameters.

        Attributes:
            :param ipAddress: IP address for simulator
        Returns:
            None
        """

        self.ipAddress = ipAddress

        try:
            logging.Info("Connecting to simulator.")
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.settimeout(10)
            self.socket.connect((self.ipAddress, self.SIM_TCP_PORT))
            logging.Info("Simulator connected at " + ipAddress +'.')
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Connection failed: " + str(exc))
            TestEnviornment.RESULT = False

        param0 = self.SIM_GET
        param1 = self.SIM_ALL_INFO
        param2 = 0
        param3 = 0

        try:
            packed_data = struct.pack('LLLL', param0, param1, param2, param3)
            self.socket.sendall(packed_data)
            data = self.socket.recv(self.SIM_TCP_BUFFER)
            logging.Info("Firmware data received.")
            TestEnviornment.RESULT = True
        except:
            logging.Error("Data not received.")
            TestEnviornment.RESULT = False
            return

        fmt = 'LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL'
        all_data = struct.unpack(fmt, data)

        # Better now, but still may need some correction

        self.IGBT_U_NTC = all_data[4]
        self.IGBT_V_NTC = all_data[5]
        self.IGBT_W_NTC = all_data[6]
        self.PIB_NTC = all_data[7]
        self.Intake_Air_NTC = all_data[8]
        self.LOW_STIRRING_FAN1 = all_data[9]
        self.HIGH_STIRRING_FAN1 = all_data[10]
        self.LOW_STIRRING_FAN2 = all_data[11]
        self.HIGH_STIRRING_FAN2 = all_data[12]
        self.LOW_MAIN_BLOWER_FAN = all_data[13]
        self.HIGH_MAIN_BLOWER_FAN = all_data[14]
        self.DC_BUS = all_data[15]
        self.MID_BUS = all_data[16]
        self.HRD_B2G = all_data[17]
        self.IFDBK_ID_OFFSET = all_data[18]
        self.IFDBK_IQ_OFFSET = all_data[19]
        self.GROUND_CURRENT_OFFSET = all_data[20]
        self.BIPOLAR_OFFSET = all_data[21]
        self.U_IFDBK_GAIN = all_data[22]
        self.V_IFDBK_GAIN = all_data[23]
        self.W_IFDBK_GAIN = all_data[24]
        self.DIGITAL_INPUT_WORD = all_data[25]
        self.GATE_ID = all_data[26]
        self.AC_EANBLE = all_data[27]
        self.AC_FREQ = all_data[28]
        self.AC_AMPL_U = all_data[29]
        self.AC_AMPL_V = all_data[30]
        self.AC_AMPL_W = all_data[31]
        self.AC_PHASE_U = all_data[32]
        self.AC_PHASE_V = all_data[33]
        self.AC_PHASE_W = all_data[34]
        self.CTRL_VOLT_FREQ = all_data[35]
        self.CTRL_VOLT_AMPL = all_data[36]
        self.AUX_ANAL1_AMP = all_data[37]
        self.AUX_ANAL2_AMP = all_data[38]
        self.AUX_ANAL3_AMP = all_data[39]
        self.AUX_ANAL4_AMP = all_data[40]
        return


    def Disconnect(self):

        try:
            self.socket.close()
            self.connected = False
            logging.Info('Instrument disconnected successfully.')
        except socket.error, exc:
            logging.Error("Connection failed: " + str(exc))
        finally:
            return

    #####################################################################
    # Temperature related commands
    #####################################################################

    def GetIGBTTemp(self, phase):

        if isinstance(phase, basestring):
            if phase == 'U':
                temp = self.IGBT_U_NTC
            elif phase == 'V':
                temp = self.IGBT_V_NTC
            elif phase == 'W':
                temp = self.IGBT_W_NTC
            else:
                logging.Error("There is no " + phase + " phase.")
                TestEnviornment.RESULT = False
            logging.Info(phase + " phase temperature is: " + str(temp) + '.')
            TestEnviornment.RESULT = True
        else:
            logging.Error("Incorrect input.")
            TestEnviornment.RESULT = False

    #################################################
    def SetIGBTTempU(self, temp):
        
        param0 = self.SIM_SET
        param1 = self.SIM_NTC_INFO
        param2 = 0
        param3 = 0

        self.IGBT_U_NTC = temp

        try:
            packed_data = struct.pack('LLLLLLLLL', param0, param1, param2, param3, self.IGBT_U_NTC, self.IGBT_V_NTC,
                                      self.IGBT_W_NTC, self.PIB_NTC, self.Intake_Air_NTC)
            self.socket.sendall(packed_data)
            logging.Info("IGBT U temperature changed to " + str(temp))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error message:" + exc)
            TestEnviornment.RESULT = False

    #################################################
    def SetIGBTTempV(self, temp):
        
        param0 = self.SIM_SET
        param1 = self.SIM_NTC_INFO
        param2 = 0
        param3 = 0

        self.IGBT_V_NTC = temp

        try:
            packed_data = struct.pack('LLLLLLLLL', param0, param1, param2, param3, self.IGBT_U_NTC, self.IGBT_V_NTC,
                                      self.IGBT_W_NTC, self.PIB_NTC, self.Intake_Air_NTC)
            self.socket.sendall(packed_data)
            logging.Info("IGBT V temperature changed to " + str(temp))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error:" + exc)
            TestEnviornment.RESULT = False
        return TestEnviornment.RESULT

    #################################################
    def SetIGBTTempW(self, temp):
        
        param0 = self.SIM_SET
        param1 = self.SIM_NTC_INFO
        param2 = 0
        param3 = 0

        self.IGBT_W_NTC = temp


        try:
            packed_data = struct.pack('LLLLLLLLL', param0, param1, param2, param3, self.IGBT_U_NTC, self.IGBT_V_NTC,
                                      self.IGBT_W_NTC, self.PIB_NTC, self.Intake_Air_NTC)
            self.socket.sendall(packed_data)
            logging.Info("IGBT W temperature changed to " + str(temp))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error:" + exc)
            TestEnviornment.RESULT = False
        return TestEnviornment.RESULT

    #################################################
    def SetPSTemp(self, temp):
        
        param0 = self.SIM_SET
        param1 = self.SIM_NTC_INFO
        param2 = 0
        param3 = 0

        self.PIB_NTC = temp

        try:
            packed_data = struct.pack('LLLLLLLLL', param0, param1, param2, param3, self.IGBT_U_NTC, self.IGBT_V_NTC,
                                      self.IGBT_W_NTC, self.PIB_NTC, self.Intake_Air_NTC)
            self.socket.sendall(packed_data)
            logging.Info("PS temperature changed to " + str(temp))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error:" + exc)
            TestEnviornment.RESULT = False
        return TestEnviornment.RESULT

    #################################################
    def SetIntakeAirTemp(self, temp):
        
        param0 = self.SIM_SET
        param1 = self.SIM_NTC_INFO
        param2 = 0
        param3 = 0

        self.Intake_Air_NTC = temp

        try:
            packed_data = struct.pack('LLLLLLLLL', param0, param1, param2, param3, self.IGBT_U_NTC, self.IGBT_V_NTC,
                                      self.IGBT_W_NTC, self.PIB_NTC, self.Intake_Air_NTC)
            self.socket.sendall(packed_data)
            logging.Info("Intake air temperature changed to " + str(temp))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error:" + exc)
            TestEnviornment.RESULT = False
        return TestEnviornment.RESULT

    #####################################################################
    # Fan control related commands
    #####################################################################
    def placeholder(self):
        return

    #####################################################################
    # DC Bus control related commands
    #####################################################################

    #####################################################################
    def SetDCBusVolts(self, voltage):
        
        param0 = self.SIM_SET
        param1 = self.SIM_BUS_INFO
        param2 = 0
        param3 = 0

        self.DC_BUS = voltage

        try:
            packed_data = struct.pack('LLLLlll', param0, param1, param2, param3, self.DC_BUS, self.MID_BUS, self.HRD_B2G)
            self.socket.sendall(packed_data)
            logging.Info("IGBT U temperature changed to " + str(voltage))
            TestEnviornment.RESULT = True
        except socket.error, exc:
            logging.Error("Data not sent. Error:" + exc)
            TestEnviornment.RESULT = False
        return TestEnviornment.RESULT

    #################################################
    def SetDCBusMid(self, vlotage):
        return

    #################################################
    def SetHRGroundVoltage(self, voltage):
        return

    #####################################################################
    # Offset control
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # IFDBK Gain control
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # Gate ID control
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # Digital Input info
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # Digital Output control
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # AC control
    #####################################################################
    def placeholder(self):
        return


    #####################################################################
    # Aux Analog control
    #####################################################################
    def placeholder(self):
        return
