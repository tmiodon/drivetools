from databaseParser import *

def CreateDatabase(driveType = '', workbook = None, listOfSheets = None):
    file = open('parametersByNumber' + driveType + '.py', 'w')
    file.write(
        '# This is stored as dictionary\n# In future maybe new method will be introduced to keep parameter database\n\n')
    file.write('parametersByNumber' + driveType + '= { 0:\n')
    file.close()

    port0 = ParameterSet(workbook=workbook, driveType=driveType)
    port0.AppendCoreInfo(listOfSheets[2], port = 0)
    port0.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('7:\n')
    file.close()
    port7 = ParameterSet(workbook=workbook, driveType=driveType)
    port7.AppendCoreInfo(listOfSheets[4], port=7)
    port7.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('9:\n')
    file.close()
    port9 = ParameterSet(workbook=workbook, driveType=driveType)
    port9.AppendCoreInfo(listOfSheets[6], port = 9)
    port9.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('10:\n')
    file.close()
    port10 = ParameterSet(workbook=workbook, driveType=driveType)
    port10.AppendCoreInfo(listOfSheets[8], port = 10)
    port10.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('12:\n')
    file.close()
    port12 = ParameterSet(workbook=workbook, driveType=driveType)
    port12.AppendCoreInfo(listOfSheets[12], port = 12)
    port12.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('13:\n')
    file.close()
    port13 = ParameterSet(workbook=workbook, driveType=driveType)
    port13.AppendCoreInfo(listOfSheets[10], port = 13)
    port13.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('14:\n')
    file.close()
    port14 = ParameterSet(workbook=workbook, driveType=driveType)
    port14.AppendCoreInfo(listOfSheets[16], port = 14)
    port14.CreateParametersByNumber('parametersByNumber' + driveType)

    file = open('parametersByNumber' + driveType + '.py', 'a')
    file.write('\t}\n')
    print "File for " + driveType + " created."
    file.close()
    return