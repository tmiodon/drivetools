# -------------------------------------------------------------------
#
# FILE NAME:    databaseParser.py
# VERSION:      0.4
# CONTRIBUTORS: Tomasz Miodonski
# START DATE:   12/19/2018
#
# DESCRIPTION:  Methods to extract parameter data and save them to Python
#               friendly form.
#
# NOTES:        Yet another version of this tool. This time I try to design class
#               that will create file for single drive, single port.
#               Creating multiple instances of this will create whole database.
#
# -------------------------------------------------------------------

class ParameterSet():

    def __init__(self, workbook = None, driveType = ''):
        # Define properties related to parameters
        self.workbook = workbook
        self.driveType = driveType
        self.port = []
        self.number = []
        self.name = []
        self.format = []
        self.isWritable = []
        self.isWritableWhenRun = []
        self.isEnum = []
        self.enumsInput = []
        self.enumsOutput = []

        self.driveTypeColumnAddress = ''

        self.DetermineDriveTypeColumnAddress()
        return

    ###############################################################
    # This method determines column in sheet that is linked to drive.
    ###############################################################
    def DetermineDriveTypeColumnAddress(self):
        if self.driveType == 'AFE':
            self.driveTypeColumnAddress = 'AS'
        elif self.driveType == 'CBI':
            self.driveTypeColumnAddress = 'AT'
        elif self.driveType == 'RBS':
            self.driveTypeColumnAddress = 'AU'
        elif self.driveType == 'PMD':
            self.driveTypeColumnAddress = 'AV'
        elif self.driveType == 'PMS':
            self.driveTypeColumnAddress = 'AW'
        elif self.driveType == '6000':
            self.driveTypeColumnAddress = 'AY'
        elif self.driveType == '753':
            self.driveTypeColumnAddress = 'AZ'
        elif self.driveType == '755LP':
            self.driveTypeColumnAddress = 'BA'
        elif self.driveType == '755HP':
            self.driveTypeColumnAddress = 'BB'
        else:
            print "Unknown drive type!!"

        return

    ###############################################################
    # This method fills parameter names, numbers, formats, info
    # about writability, if parameters are enum.
    ###############################################################
    def AppendCoreInfo(self, parameterSheet, port):
        sheet = self.workbook[parameterSheet]

        parameterNumber = 'C2'
        i = 2
        j = i - 2
        while sheet[parameterNumber].value != None:
            parameterName = 'D' + str(i)
            parameterFormat = 'H' + str(i)
            parameterIsWritable = 'AE' + str(i)
            parameterIsWritableWhenRun = 'AF' + str(i)
            parameterIsEnum = 'AD' + str(i)
            if sheet[self.driveTypeColumnAddress + str(i)].value == 'Yes':
                if sheet[parameterNumber].value in self.number:
                    self.port[j-1] = port
                    self.number[j-1] = sheet[parameterNumber].value
                    self.name[j-1] = sheet[parameterName].value
                    self.format[j-1] = sheet[parameterFormat].value
                    self.isWritable[j-1] = sheet[parameterIsWritable].value
                    self.isWritableWhenRun[j-1] = sheet[parameterIsWritableWhenRun].value
                    self.isEnum[j-1] = sheet[parameterIsEnum].value
                else:
                    self.port.append(port)
                    self.number.append(sheet[parameterNumber].value)
                    self.name.append(sheet[parameterName].value)
                    self.format.append(sheet[parameterFormat].value)
                    self.isWritable.append(sheet[parameterIsWritable].value)
                    self.isWritableWhenRun.append(sheet[parameterIsWritableWhenRun].value)
                    self.isEnum.append(sheet[parameterIsEnum].value)
                    j = j + 1
            i = i + 1
            parameterNumber = 'C' + str(i)
        return

    ###############################################################
    # This method appends enumerable values and names
    # to parameters that are enumerable.
    ###############################################################
    def AppendEnums(self, enumSheet):
        sheet = self.workbook[enumSheet]
        enumParameterNumber = 'A3'
        enumParameterName = 'B3'
        for k in range(len(self.number)):
            if self.isEnum[k] == 1:
                i = 3
                while sheet[enumParameterNumber].value != self.number[k]:
                    i = i + 1
                    enumParameterNumber = 'A' + str(i)
                inputDict = {}
                outputDict={}
                j = i + 1
                enumValue = 'C' + str(j)
                enumName = 'D' + str(j)
                while sheet[enumValue].value != None:
                    if sheet[enumName].value != 'Reserved':
                        inputDict[sheet[enumName].value] = sheet[enumValue].value
                        outputDict[sheet[enumValue].value] = sheet[enumName].value
                    j = j + 1
                    enumValue = 'C' + str(j)
                    enumName = 'D' + str(j)
                if sheet[enumParameterName].value in self.name:
                    if sheet['D' + str(i)].value == 'FOR PF755TM CBIs' and self.driveType == 'CBI':
                        self.enumsInput.append([sheet[enumParameterNumber].value, inputDict])
                        self.enumsOutput.append([sheet[enumParameterNumber].value, outputDict])
                    elif sheet['D' + str(i)].value == 'FOR PF755TM BUS SUPPLIES' and (self.driveType == 'RBS' or self.driveType == 'PMS'):
                        self.enumsInput.append([sheet[enumParameterNumber].value, inputDict])
                        self.enumsOutput.append([sheet[enumParameterNumber].value, outputDict])
                    elif sheet['D' + str(i)].value == 'FOR PF755TR AND PF755TL DRIVES' and (self.driveType == 'PMD' or self.driveType == 'AFE' or self.driveType == '6000'
                            or self.driveType == '733' or self.driveType == '755HP' or self.driveType == '755LP'):
                        self.enumsInput.append([sheet[enumParameterNumber].value, inputDict])
                        self.enumsOutput.append([sheet[enumParameterNumber].value, outputDict])
                    elif sheet['D' + str(i)].value == None:
                        self.enumsInput.append([sheet[enumParameterNumber].value, inputDict])
                        self.enumsOutput.append([sheet[enumParameterNumber].value, outputDict])
        return
    ###############################################################
    # This method
    ###############################################################
    def CreateParametersByName(self, fileName):
        file = open(fileName + '.py', 'a')
        for i in range(len(self.number)):
            file.write('\t' + '\'P' + str(self.port[i]) + ' ' + str(self.name[i]) + '\'' + ': ' + '{\'port\': ' + str(self.port[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'number\': ' + str(self.number[i]) + ',\n')
            if self.format[i] == 'BOOL[16]':
                file.write('\t\t\t\t\t' + '\'type\': \'WORD\',\n')
            elif self.format[i] == 'BOOL[32]':
                file.write('\t\t\t\t\t' + '\'type\': \'DWORD\',\n')
            else:
                file.write('\t\t\t\t\t' + '\'type\': ' + '\'' + str(self.format[i]) + '\'' + ',\n')
            file.write('\t\t\t\t\t' + '\'isWritable\': ' + str(self.isWritable[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'isWritableWhenRun\': ' + str(self.isWritableWhenRun[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'isEnum\': ' + str(self.isEnum[i]) + ',\n')
            for j in range(len(self.enumsInput)):
                if self.number[i] == self.enumsInput[j][0]:
                    file.write('\t\t\t\t\t' + '\'enumsInput\': ' + str(self.enumsInput[j][1]).replace(' ', '') + ',\n')
                    file.write('\t\t\t\t\t' + '\'enumsOutput\': ' + str(self.enumsOutput[j][1]).replace(' ', '') + '\n')
            file.write('\t\t\t\t\t},\n')
        file.close()
        return

        ###############################################################
        # This method
        ###############################################################
    def CreateParametersByNumber(self, fileName):
        file = open(fileName + '.py', 'a')
        file.write('\t {\n')
        for i in range(len(self.number)):
            if self.format[i] == 'BOOL[16]' or self.format[i] == 'BOOL[32]':
                dataFormat = str(self.format[i])
                file.write('\t\t' + str(self.number[i]) + ': \'' + 'DWORD' + '\',\n')
            else:
                file.write('\t\t' + str(self.number[i]) + ': \'' + str(self.format[i]) + '\',\n')
        file.write('\t },\n')
        file.close()
        return

