from parametersByNameAFE import parametersByNameAFE as parametersAFE
from parametersByNameCBI import parametersByNameCBI as parametersCBI
from parametersByNameRBS import parametersByNameRBS as parametersRBS
from parametersByNamePMD import parametersByNamePMD as parametersPMD
from parametersByNamePMS import parametersByNamePMS as parametersPMS
from parametersByName6000 import parametersByName6000 as parameters6000
from parametersByName753 import parametersByName753 as parameters753
from parametersByName755LP import parametersByName755LP as parameters755LP
from parametersByName755HP import parametersByName755HP as parameters755HP


def testingDatabaseAFE():
    for each in parametersAFE:
        if parametersAFE[each]['isEnum'] == 1:
            try:
                type(parametersAFE[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabaseCBI():
    for each in parametersCBI:
        if parametersCBI[each]['isEnum'] == 1:
            try:
                type(parametersCBI[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabaseRBS():
    for each in parametersRBS:
        if parametersRBS[each]['isEnum'] == 1:
            try:
                type(parametersRBS[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabasePMD():
    for each in parametersPMD:
        if parametersPMD[each]['isEnum'] == 1:
            try:
                type(parametersPMD[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabasePMS():
    for each in parametersPMS:
        if parametersPMS[each]['isEnum'] == 1:
            try:
                type(parametersPMS[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabase6000():
    for each in parametersAFE:
        if parameters6000[each]['isEnum'] == 1:
            try:
                type(parameters6000[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabase753():
    for each in parametersAFE:
        if parameters753[each]['isEnum'] == 1:
            try:
                type(parameters753[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabase755LP():
    for each in parametersAFE:
        if parameters755LP[each]['isEnum'] == 1:
            try:
                type(parameters755LP[each]['enums'])
                result = True
            except:
                result = False

    return result

def testingDatabase755HP():
    for each in parameters755HP:
        if parameters755HP[each]['isEnum'] == 1:
            try:
                type(parameters755HP[each]['enums'])
                result = True
            except:
                result = False

    return result