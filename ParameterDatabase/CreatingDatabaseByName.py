from databaseParser import *

def CreateDatabase(driveType = '', workbook = None, listOfSheets = None):
    file = open('parametersByName' + driveType + '.py', 'w')
    file.write(
        '# This is stored as dictionary\n# In future maybe new method will be introduced to keep parameter database\n\n')
    file.write('parametersByName' + driveType + '= {\n')
    file.close()

    port0 = ParameterSet(workbook=workbook, driveType=driveType)
    port0.AppendCoreInfo(listOfSheets[2], port = 0)
    port0.AppendEnums(listOfSheets[3])
    port0.CreateParametersByName('parametersByName' + driveType)

    port7 = ParameterSet(workbook=workbook, driveType=driveType)
    port7.AppendCoreInfo(listOfSheets[4], port=7)
    port7.AppendEnums(listOfSheets[5])
    port7.CreateParametersByName('parametersByName' + driveType)

    port9 = ParameterSet(workbook=workbook, driveType=driveType)
    port9.AppendCoreInfo(listOfSheets[6], port = 9)
    port9.AppendEnums(listOfSheets[7])
    port9.CreateParametersByName('parametersByName' + driveType)

    port10 = ParameterSet(workbook=workbook, driveType=driveType)
    port10.AppendCoreInfo(listOfSheets[8], port = 10)
    # port10.AppendEnums(listOfSheets[10])
    port10.CreateParametersByName('parametersByName' + driveType)

    port12 = ParameterSet(workbook=workbook, driveType=driveType)
    port12.AppendCoreInfo(listOfSheets[12], port = 12)
    port12.AppendEnums(listOfSheets[13])
    port12.CreateParametersByName('parametersByName' + driveType)

    port13 = ParameterSet(workbook=workbook, driveType=driveType)
    port13.AppendCoreInfo(listOfSheets[10], port = 13)
    port13.AppendEnums(listOfSheets[11])
    port13.CreateParametersByName('parametersByName' + driveType)

    port14 = ParameterSet(workbook=workbook, driveType=driveType)
    port14.AppendCoreInfo(listOfSheets[16], port = 14)
    port14.AppendEnums(listOfSheets[17])
    port14.CreateParametersByName('parametersByName' + driveType)

    file = open('parametersByName' + driveType + '.py', 'a')
    file.write('\t}\n')
    print "File for " + driveType + " created."
    file.close()
    return