from openpyxl import load_workbook
import os, warnings
import CreatingDatabaseByName, CreatingDatabaseByNumber
import time

def DeleteFiles(device):
    try:
        os.remove(device + '.py')
        print "Files for " + str(device) +  " deleted!"
    except:
        print "No files for " + str(device) + "!"
    return


#####################################
# Choosing operation options
# 0 - only delete files
# 1 - only create files
# 2 - delete old files and create new
flag = 2
#####################################

#####################################
# Determining for which configuration
# database will be created
createForAFE = True
createForCBI = True
createForRBS = True
createForPMD = True
createForPMS = True
createFor6000 = False
createFor753 = False
createFor755LP = False
createFor755HP = False
#####################################

start = time.time()

if flag == 0:
    print "We are now in DELETE mode!"
elif flag == 1:
    print "We are now in CREATE mode."
elif flag == 2:
    print "We are now in COMBINED mode."
else:
    print "WTF?!"


# Removing old files to make sure that new are created
if flag == 0 or flag == 2:
    DeleteFiles(device = 'parametersByName753')
    DeleteFiles(device = 'parametersByName755HP')
    DeleteFiles(device = 'parametersByName755LP')
    DeleteFiles(device = 'parametersByName6000')
    DeleteFiles(device = 'parametersByNameAFE')
    DeleteFiles(device = 'parametersByNameCBI')
    DeleteFiles(device = 'parametersByNamePMD')
    DeleteFiles(device = 'parametersByNamePMS')
    DeleteFiles(device = 'parametersByNameRBS')
    DeleteFiles(device='parametersByNumber753')
    DeleteFiles(device='parametersByNumber755HP')
    DeleteFiles(device='parametersByNumber755LP')
    DeleteFiles(device='parametersByNumber6000')
    DeleteFiles(device='parametersByNumberAFE')
    DeleteFiles(device='parametersByNumberCBI')
    DeleteFiles(device='parametersByNumberPMD')
    DeleteFiles(device='parametersByNumberPMS')
    DeleteFiles(device='parametersByNumberRBS')

# Creating new database
if flag == 1 or flag == 2:
    print "Opening excel file."
    warnings.simplefilter("ignore")
    wb = load_workbook('HPC Database - Draft.xlsm')
    warnings.simplefilter("default")
    listOfSheets = wb.sheetnames
    print "File is opened."

    if createForAFE:
        CreatingDatabaseByName.CreateDatabase(driveType ='AFE', workbook = wb, listOfSheets = listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='AFE', workbook=wb, listOfSheets=listOfSheets)

    if createForCBI:
        CreatingDatabaseByName.CreateDatabase(driveType='CBI', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='CBI', workbook=wb, listOfSheets=listOfSheets)

    if createForRBS:
        CreatingDatabaseByName.CreateDatabase(driveType='RBS', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='RBS', workbook=wb, listOfSheets=listOfSheets)

    if createForPMD:
        CreatingDatabaseByName.CreateDatabase(driveType='PMD', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='PMD', workbook=wb, listOfSheets=listOfSheets)

    if createForPMS:
        CreatingDatabaseByName.CreateDatabase(driveType='PMS', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='PMS', workbook=wb, listOfSheets=listOfSheets)

    if createFor6000:
        CreatingDatabaseByName.CreateDatabase(driveType='6000', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='6000', workbook=wb, listOfSheets=listOfSheets)

    if createFor753:
        CreatingDatabaseByName.CreateDatabase(driveType='753', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='753', workbook=wb, listOfSheets=listOfSheets)

    if createFor755LP:
        CreatingDatabaseByName.CreateDatabase(driveType='755LP', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType='755LP', workbook=wb, listOfSheets=listOfSheets)

    if createFor755HP:
        CreatingDatabaseByName.CreateDatabase(driveType='755HP', workbook=wb, listOfSheets=listOfSheets)
        CreatingDatabaseByNumber.CreateDatabase(driveType ='755HP', workbook = wb, listOfSheets = listOfSheets)
    stop = time.time()

    print (stop - start)
