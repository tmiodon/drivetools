# -------------------------------------------------------------------
#
# FILE NAME:
# VERSION:
# CONTRIBUTORS:
#
# START DATE:
#
# DESCRIPTION:
#
# -------------------------------------------------------------------

from datetime import datetime
import Configuration.TestEnvironment as TestEnviornment

# For each kind of log entry elements constructing message are similar
# with difference being entry type and entry message. Those can be passed
# as parameters.
def CreateLogEntry(type, message):
    time = "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") "
    logged_message = type + time + message
    TestEnviornment.LOG_RECORDS.append(logged_message)
    if TestEnviornment.ENABLE_LOG_PRINTING is True:
        print logged_message
    return

def Warning(message=''):
    type = '[WARN]'
    CreateLogEntry(type, message)
    TestEnviornment.WARNING_COUNT += 1
    return

def Error(message=''):
    type = '[ERRO]'
    CreateLogEntry(type, message)
    TestEnviornment.ERROR_COUNT += 1
    return

def Info(message=''):
    type = '[INFO]'
    CreateLogEntry(type, message)
    return

def Debug(message=''):
    type = '[DBUG]'
    CreateLogEntry(type, message)
    return

def Pass(message=''):
    type = '[PASS]'
    CreateLogEntry(type, message)
    TestEnviornment.PASS_COUNT += 1
    return

def Fail(message=''):
    type = '[FAIL]'
    CreateLogEntry(type, message)
    TestEnviornment.FAIL_COUNT += 1
    return

def LogValue(value, message=''):
    """ Use to log specific value in report.



    :param value: Input value that will be logged in report
    :return:
    """
    type = '[INFO]'
    message = 'Logged value: ' + str(value) + ". Additional message: " + message
    CreateLogEntry(type, message)
    return

# This probably won't be needed as each function can be accessible by test designer
def LogMessage(message, type='Info'):
    """ Use to log any message in report.



    :param message:
    :param type:
    :return:
    """
    if type == 'Info':
        _type = '[INFO]'
    elif type == 'Debug':
        _type = '[DBUG]'
    elif type == 'Warning':
        _type = '[WARN]'
    elif type == 'Error':
        _type = '[ERRO]'
    else:
        raise ValueError('Log type is incorrect')

    CreateLogEntry(_type, message)
    return True


def GenerateReport():
    test_name = 'placehloder'
    test_start_time = TestEnviornment.START_TIME
    firmware_version = TestEnviornment.INPUT_FIRMWARE_VERSION
    report_file = open(TestEnviornment.TEMP_LOG_DIRECTORY, 'w')

    report_file.write('Test name: ' + test_name + '\n')
    report_file.write('Start date: ' + test_start_time + '\n')
    report_file.write('Firmware revision: ' + firmware_version + '\n')
    report_file.write('===========================\n')

    for line in TestEnviornment.LOG_RECORDS:
        report_file.write(line + '\n')
    report_file.write('===========================\n')


    if TestEnviornment.FAIL_COUNT != 0:
        report_file.write("Test failed!\n")
    else:
        report_file.write('Test passed!\n')

    report_file.write('Pass counts: ' + str(TestEnviornment.PASS_COUNT) + '\n')
    report_file.write('Fail counts: ' + str(TestEnviornment.FAIL_COUNT) + '\n\n')
    report_file.write('Warning counts: ' + str(TestEnviornment.WARNING_COUNT) + '\n')
    report_file.write('error counts: ' + str(TestEnviornment.ERROR_COUNT) + '\n')

    report_file.close()
    return
