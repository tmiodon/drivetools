from drivesLibrary import driveClass as Drives
from utilsLibrary import Utils
import time

Drive01 = Drives.driveBase()

Probe_amount = 100

T1min = 10000.0
T1max = 0.0
T2min = 10000.0
T2max = 0.0
T3min = 10000.0
T3max = 0.0
T4min = 10000.0
T4max = 0.0
T5min = 10000.0
T5max = 0.0
T6min = 10000.0
T6max = 0.0
T7min = 10000.0
T7max = 0.0
T8min = 10000.0
T8max = 0.0

Utils.SectionStart("StatusWord")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.ReadStatusWord()
    temp = (time.clock()-temp)*1000.0

    if T1min > temp:
        T1min = temp
    if T1max < temp:
        T1max = temp
T1 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("WriteRefference")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.WriteRefference(30.0)
    temp = (time.clock()-temp)*1000.0
    if T2min > temp:
        T2min = temp
    if T2max < temp:
        T2max = temp
T2 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("ReadParameter")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.ReadParameter(200,0)
    temp = (time.clock()-temp)*1000.0
    if T3min > temp:
        T3min = temp
    if T3max < temp:
        T3max = temp
T3 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("WriteParameter")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.WriteParameter(0,702,0)
    temp = (time.clock()-temp)*1000.0
    if T4min > temp:
        T4min = temp
    if T4max < temp:
        T4max = temp
T4 = ((time.clock()-TStart)/Probe_amount)*1000.0

Drive01.Disconnect()
Utils.Wait(1)
Drive01 = Drives.driveBase(connection=True)

Utils.SectionStart("StatusWord")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.ReadStatusWord()
    temp = (time.clock()-temp)*1000.0
    if T5min > temp:
        T5min = temp
    if T5max < temp:
        T5max = temp
T5 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("WriteRefference")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.WriteRefference(30.0)
    temp = (time.clock()-temp)*1000.0
    if T6min > temp:
        T6min = temp
    if T6max < temp:
        T6max = temp
T6 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("ReadParameter")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.ReadParameter(200,0)
    temp = (time.clock()-temp)*1000.0
    if T7min > temp:
        T7min = temp
    if T7max < temp:
        T7max = temp
T7 = ((time.clock()-TStart)/Probe_amount)*1000.0

Utils.SectionStart("WriteParameter")
TStart = time.clock()
for x in xrange(Probe_amount):
    temp = time.clock()
    Drive01.WriteParameter(0,702,0)
    temp = (time.clock()-temp)*1000.0
    if T8min > temp:
        T8min = temp
    if T8max < temp:
        T8max = temp
T8 = ((time.clock()-TStart)/Probe_amount)*1000.0
print "#########################################################################"
print "Unconnected StatusWord - \t\t average " + str(round(T1,3)) + " ms \t\t max " + str(round(T1max,3)) + " ms \t\t min " + str(round(T1min,3)) + " ms "
print "Unconnected WriteRefference - \t average " + str(round(T2,3)) + " ms \t\t max " + str(round(T2max,3)) + " ms \t\t min " + str(round(T2min,3)) + " ms "
print "Unconnected ReadParameter - \t average " + str(round(T3,3)) + " ms \t\t max " + str(round(T3max,3)) + " ms \t\t min " + str(round(T3min,3)) + " ms "
print "Unconnected WriteParameter - \t average " + str(round(T4,3)) + " ms \t\t max " + str(round(T4max,3)) + " ms \t\t min " + str(round(T4min,3)) + " ms "
print "Connected StatusWord - \t\t\t average " + str(round(T5,3)) + " ms \t\t max " + str(round(T5max,3)) + " ms \t\t min " + str(round(T5min,3)) + " ms "
print "Connected WriteRefference - \t average " + str(round(T6,3)) + " ms \t\t max " + str(round(T6max,3)) + " ms \t\t min " + str(round(T6min,3)) + " ms "
print "Connected ReadParameter - \t\t average " + str(round(T7,3)) + " ms \t\t max " + str(round(T7max,3)) + " ms \t\t min " + str(round(T7min,3)) + " ms "
print "Connected WriteParameter - \t\t average " + str(round(T8,3)) + " ms \t\t max " + str(round(T8max,3)) + " ms \t\t min " + str(round(T8min,3)) + " ms "

print "#########################################################################"
print " BUG : Min/max check can be fixed after migration to Python 3.6. It will encahnce precision on Windows platform."
print " https://www.webucator.com/blog/2015/08/python-clocks-explained/";
print "#########################################################################"