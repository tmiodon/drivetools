from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.drivers.ethernet_ip import ChannelCfg
from ratools.cip.drivers.ethernet_ip_secure import SocketType
from time import sleep
import struct
import atexit

# -------------------------------------------------------------------
# AUTHOR: Bartosz Moskwik
# DATE:   11/04/2019
# Yep, it works !
# -------------------------------------------------------------------
class IO_connection():
    """
    This Class implements IO connection to 1734-AENTER Point.
    So far its kind of dummy, as it use hardcoded point values.
    """

    def __init__(self, ipAddress=None, RPI = 20):
        if ipAddress is None:
            exit(30)
        self.ipAddress = ipAddress
        self.conn_path = r'AB_ETHIP-1\\' + self.ipAddress
        self.OT_RPI = RPI *1000
        self.TO_RPI = RPI *1000
        self.multiplier = 4
        self.Opened_extra_connection = []
        atexit.register(self.exit_handler)

    def exit_handler(self):
        for connection in self.Opened_extra_connection:
            connection.close()
        self.connection.close()
        pass

    def dummy_function(conn, ret,msg):
        print msg

    def Listen_Only_FW_open(self):
        OT_CONN_PARAMS = ForwardOpenConnectionParameters(
            connection_size=18,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            redundant_owner=False)

        TO_CONN_PARAMS = ForwardOpenConnectionParameters(
            connection_size=14,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            redundant_owner=False)
        conn_params = {
            'transport_class': 1,
            'trigger': 0,
            'direction': 1,
            'to_rpi': self.TO_RPI,
            'ot_rpi': self.OT_RPI,
            'connection_path': [(0x34, [0x0001, 0x000c, 0x00c4, 0x85, 0x01]),
                                (0x21, 0x031c),
                                (0x24, 0x00),
                                (0x2d, 0x8402),
                                (0x2d, 0x0402)
                                ],
            'connection_timeout_multiplier': self.multiplier,
            'to_connection_parameters': TO_CONN_PARAMS,
            'ot_connection_parameters': OT_CONN_PARAMS
        }

        channel_cfg = ChannelCfg(path=self.conn_path, connected=False, socket_type=SocketType.UDP)
        self.channel = channel_cfg.create_channel()
        self.connection = self.channel.create_connection(
            receive_thread=self.dummy_function,
            send_thread=True,
            open_connection=True,
            **conn_params)

    def OB8E_FW_Open(self, Link_addr):
        conn_params = {
            'transport_class': 1,
            'trigger': 0,
            'direction': 1,
            'to_rpi': self.TO_RPI,
            'ot_rpi': self.OT_RPI,
            'connection_path': [(0x21, 0x031c),
                                (0x24, 0x11),
                                (0x01, Link_addr),
                                (0x34, [0x0001, 0x0007, 0x00da, 0x83,0x01]),
                                (0x20, 0x04),
                                (0x24, 0x7b),
                                (0x2c, 0xc2),
                                (0x2c, 0xc1),
                                (0x80, '0100000000000000ff000000')
                                ],
            'connection_timeout_multiplier': self.multiplier,
            'to_connection_parameters': 0x0803,
            'ot_connection_parameters': 0x0803
        }

        self.Opened_extra_connection.append(self.channel.create_connection(
            receive_thread=False,
            send_thread=False,
            open_connection=True,
            **conn_params))

    def IB8_FW_Open(self, Link_addr):
        conn_params = {
            'transport_class': 1,
            'trigger': 0,
            'direction': 1,
            'to_rpi': self.TO_RPI,
            'ot_rpi': self.OT_RPI,
            'connection_path': [(0x21, 0x031c),
                                (0x24, 0x11),
                                (0x01, Link_addr),
                                (0x34, [0x0001, 0x0007, 0x00d8, 0x83,0x01]),
                                (0x20, 0x04),
                                (0x24, 0x67),
                                (0x2c, 0xc2),
                                (0x2c, 0xc1),
                                # Really ?? from where this comes?
                                (0x80, '01000000e803e803e803e803e803e803e803e803e803e803e803e803e803e803e803e803')
                                ],
            'connection_timeout_multiplier': self.multiplier,
            'to_connection_parameters': 0x0803,
            'ot_connection_parameters': 0x0802
        }

        self.Opened_extra_connection.append(self.channel.create_connection(
            receive_thread=False,
            send_thread=False,
            open_connection=True,
            **conn_params))

if __name__ == '__main__':
    test = IO_connection(ipAddress="192.168.1.100",RPI = 5)
    test.Listen_Only_FW_open()
    # sleep(1)
    test.OB8E_FW_Open(0x01)
    test.OB8E_FW_Open(0x02)
    test.IB8_FW_Open(0x03)

    output_data = [0x00,0x00]

    for x in xrange(255):
        sleep(0.1)
        output_data[1] += 1
        test.connection.sent = test.connection.pack_data([0x00000001] +[0xfff1,0xffff,0xffff,0xffff] + [0x00] +
                                                         output_data,['DWORD']+['WORD']*4 +['BYTE']+['BYTE']*output_data.__len__())

