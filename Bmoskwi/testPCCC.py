from drivesLibrary import driveClass as Drives
from utilsLibrary import Utils
import time

def TestCase(setfunction=None,clearfunction=None, testfunction=None):
    global Passed, Failed
    setfunction()
    WaitTillParameter(testfunction,True) # be aware that this timer is needed for Datalinks cycle to make changes.
    if Utils.VerifyValue(testfunction(), True):
        Passed +=1
    else:
        Failed += 1
        print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Here"
    clearfunction()
    WaitTillParameter(testfunction, False) # be aware that this timer is needed for Datalinks cycle to make changes.
    if Utils.VerifyValue(testfunction(), False):
        Passed += 1
    else:
        Failed += 1
        print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Here"

def TestContent():
    Drive01.ClearFaults()
    TestCase(Drive01.setManualbit, Drive01.clearManualbit, Drive01.isManual)
    Drive01.ClearFaults()
    TestCase(Drive01.setJog1bit, Drive01.clearJog1bit, Drive01.isJogging)
    Drive01.ClearFaults()
    TestCase(Drive01.setJog2bit, Drive01.clearJog2bit, Drive01.isJogging)


def WaitTillParameter(variable, Logigstate, interval=0.1, timeout = 5):
    TestStartTime = time.time()
    test = variable()
    while test != Logigstate:
        time.sleep(interval)
        test = variable()
        if  time.time() - TestStartTime > timeout:
            Utils.LogFail("Timeout set variable !")
            break


Drive01 = Drives.driveBase(ipAddress="192.168.1.101")
Passed=0
Failed=0
Probe_amount =10

# print "###################################################################"
# print "Test case 1  PCCC"""
# Drive01.Connect()
# for x in xrange(Probe_amount):
#     TestContent()
# Drive01.Disconnect()
#
# print "###################################################################"
# print "Test case 2  Connected messages with PCCC"
# Drive01.Connect(connected=True)
# for x in xrange(Probe_amount):
#     TestContent()
# Drive01.Disconnect()

print "###################################################################"
print "Test case 3  Class 1 connection"
Drive01.ConnectIOConnection(pullTime=2)
for x in xrange(Probe_amount):
    TestContent()
Drive01.DisconnectIOconnection()

print "###################################################################"
print "Test finished with result:"
if Failed == 0 and Passed <> 0:
    Utils.LogPass("Test Passed")
elif  Failed <> 0:
    Utils.LogFail("Test Failed")

print " Passed counter: " + str(Passed)+ " Failed counter: " +str(Failed)
print "###################################################################"