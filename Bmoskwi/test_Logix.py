from drivesLibrary import driveClass as Drives
from ratools.lnx.logixservices import open_project
import ratools.cip.channel as channel
import ratools.cip.act_vars as act_vars

# PLC_PATH = r'AB_ETHIP-1\192.168.1.31\Backplane\0'
chan = channel.create(path=r'AB_ETHIP-1\192.168.1.11')
tags = act_vars.TagVariableTarget(chan)

#project = Logix.open_project('test.acd',version="32")
#print project

# Logix.Controller.set_comm_path(comm_path=PLC_PATH, keys=755)
# print PLC
Logix.download('test.acd',run=True,version="32")

print"Project downloaded"
