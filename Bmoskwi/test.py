from ratools.lnx.logixservices import Dispatch,constants

iLogixServices = Dispatch()

# Create an L63 project
iController = iLogixServices.Create("temp.ACD",constants.lgxProcType_ControlLogix5563)

# Set comms paths
iController.ProjectCommPath = r'AB_ETHIP-1\192.168.0.43\Backplane\9'
iController.CurrentCommPath = iController.ProjectCommPath

# Set slot number of controller
iController.SetProperty(0,"Controller/Modules/Module[@name='local']/Ports/Port[@Id='1']","Address","9")

# Create a tag
iController.ImportRawXMLFromString(0,'Controller/Tags','Tag',False,r'''
<Tag Name="someGlobalDint" TagType="Base" DataType="DINT" Radix="Decimal">
<Description>
<![CDATA[Tag description for someGlobalDint]]>
</Description>
<Data>00 00 00 00</Data>
</Tag>
''',0,'',False)

# Download project to controller (put it in run mode)
iController.GoConnected()
iController.Mode = constants.lgxMode_Program
iController.Download(False,True,constants.lgxMode_Run,False)

# Save ACD file
iController.Save()

# Save it in XML format as well
iController.ExportXMLToFile(0,'',0,False,r'C:\temp.xml')