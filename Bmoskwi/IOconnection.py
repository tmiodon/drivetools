from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.drivers.ethernet_ip import ChannelCfg
from ratools.cip.drivers.ethernet_ip_secure import SocketType
from drivesLibrary import driveClass as Drives
from time import sleep
# -------------------------------------------------------------------
# AUTHOR: Bartosz Moskwik
# START DATE:   03/15/2019
# DESCRIPTION:  ENIP stack performance test, developed to test EDLT implementation in PF755T
# -------------------------------------------------------------------
static_counter = 0
Drive01 = Drives.driveBase()

def dummy_function(conn, msg):
    print "##########################"
    # y = bytearray(msg)
    # print ''.join('{:02x}'.format(x) for x in Drive01.ReadStatusWord())
    #
    # for x in range(y.__len__()):
    #     if len(y[x*4:]) >= struct.calcsize('<L'):
    #         print "DL " + str(x) + " " + str(struct.unpack_from('<L', y, x * 4))
    #
    # print "##########################"
def start():
    OT_RPI = 2000
    TO_RPI = 2000
    multiplier = 4

    OT_CONN_PARAMS = ForwardOpenConnectionParameters(
        connection_size=78,
        connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
        priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
        redundant_owner=False)

    TO_CONN_PARAMS = ForwardOpenConnectionParameters(
        connection_size=78,
        connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
        priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
        redundant_owner=False)

    conn_params = {
        'transport_class': 1,
        'trigger': 0,
        'direction': 0,
        'to_rpi': TO_RPI,
        'ot_rpi': OT_RPI,
        'connection_path': [(0x34, [0x0001, 0x0000, 0x000, 0x00, 0x00]),
                            (0x20, 0x04),
                            (0x24, 0x06),
                            (0x2c, 0x02),
                            (0x2c, 0x01),],
        'connection_timeout_multiplier': multiplier,
        'to_connection_parameters': TO_CONN_PARAMS,
        'ot_connection_parameters': OT_CONN_PARAMS
    }

    conn_path = r'AB_ETHIP-1\192.168.1.101'

    channel_cfg = ChannelCfg(path=conn_path, connected=False, socket_type=SocketType.UDP)
    channel = channel_cfg.create_channel()
    connection = channel.create_connection(
        receive_thread=dummy_function,
        send_thread=True,
        open_connection=True,
        **conn_params)
    output_data = 1
    while True:
        connection.close()
        sleep(0.2)
        connection.sent = connection.pack_data([1] + [0x00000001,30.0,output_data], ['DWORD'] +['DWORD','REAL','DWORD'])
        output_data += 1
        connection.open()

start()