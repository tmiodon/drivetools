from drivesLibrary import driveClass as Drives
from datetime import datetime
from utilsLibrary import Utils

import time
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import os
# -------------------------------------------------------------------
# AUTHOR: Bartosz Moskwik
# START DATE:   03/04/2019
## DESCRIPTION:  CIP stack performance test, developed to test EDLT implementation for PF755T
# -------------------------------------------------------------------
TestStartTime = time.time()
##########Global vars###############################################
Probe_amount =100
Outcomes = [[None] * 0] * 0
PlotTable = [[None] * 0] * 0
NameTable = []
##########Global vars###############################################
Drive01 = Drives.driveBase(ipAddress="192.168.1.11")
def MSG_test( SectionName, Probe_amount, Tested_message, *args):
    global PlotTable
    global NameTable
    Utils.SectionStart(SectionName)
    Temp_Table = []
    TStart = time.clock()
    for x in xrange(Probe_amount):
        temp = time.clock()
        Tested_message(*args)########### Method to test
        temp = (time.clock() - temp) * 1000.0   # message time request/response. Resolution around 12 us
        Temp_Table.append(temp)
    T_Average = ((time.clock() - TStart) / Probe_amount) * 1000.0 # average calculation
    Temp_Table.sort()
    Tmin = Temp_Table[0]
    Tmax = Temp_Table[Temp_Table.__len__()-1]
    T_median = Temp_Table[int(round(Probe_amount / 2))]
    T_10percentyl = Temp_Table[int(round(Probe_amount / 10))]
    T_90percentyl = Temp_Table[int(round(Probe_amount / 10)) * 9]
    PlotTable.append(Temp_Table)
    NameTable.append(SectionName.split(' - ')[0])
    return [T_Average, Tmax, Tmin, T_median, T_10percentyl, T_90percentyl, SectionName]

def MSG_test_print(T):   # just pringting method
    print T[6] + "average " + str(round(T[0], 2)) + " \tms\tmax " + str(
        round(T[1], 2)) + " \tms\tmin " + str(round(T[2], 2)) + " \tms\tmedian " + str(
        round(T[3], 2)) + " \tms\t10th_percentyl " + str(
        round(T[4], 2)) + " \tms\t90th_percentyl " + str(round(T[5], 2)) + " \tms"
def MSG_Open_Close(Tested_message=None,*args):
    Drive01.Connect(connected=True,PCCC=False)
    Tested_message(*args)
    Drive01.Disconnect(PCCC=False)
    return

# ###################################################################
# Drive01.Connect(PCCC=False)
# Outcomes.append(MSG_test("Unconn StatusWord PCCC - \t\t\t", Probe_amount, Drive01.ReadStatusWord,))
# Outcomes.append(MSG_test("Unconn WriteRefference PCCC - \t\t", Probe_amount, Drive01.WriteRefference, 30.0))
# Outcomes.append(MSG_test("Unconn ReadParameter - \t\t\t\t", Probe_amount, Drive01.ReadParameter, 200, 0))
# Outcomes.append(MSG_test("Unconn WriteParameter - \t\t\t", Probe_amount, Drive01.WriteParameter, 0, 702, 0))
# Drive01.Disconnect(PCCC=False)
# ###################################################################
# Drive01.Connect(connected=True, PCCC=False)
# Outcomes.append(MSG_test("Conn StatusWord PCCC - \t\t\t\t", Probe_amount, Drive01.ReadStatusWord))
# Outcomes.append(MSG_test("Conn WriteRefference PCCC - \t\t", Probe_amount, Drive01.WriteRefference, 35.0))
# Outcomes.append(MSG_test("Conn ReadParameter - \t\t\t\t", Probe_amount, Drive01.ReadParameter, 3, 0))
# Outcomes.append(MSG_test("Conn WriteParameter - \t\t\t\t", Probe_amount, Drive01.WriteParameter, 0, 703, 0))
# Drive01.Disconnect(PCCC=False)
###################################################################
#issue is that we are closing also socket, not only connection. Need some fix here but still valid as opening and closing
# socket takes no more than 1 ms and should be same value on previous stack.
Outcomes.append(MSG_test("Conn not catched StatusWord PCCC - \t", Probe_amount, MSG_Open_Close, Drive01.ReadStatusWord,))
Outcomes.append(MSG_test("Conn not catched WriteReff PCCC - \t", Probe_amount, MSG_Open_Close, Drive01.WriteRefference, 25.0))
Outcomes.append(MSG_test("Conn not catched ReadParameter - \t", Probe_amount, MSG_Open_Close, Drive01.ReadParameter, 25, 0 ))
Outcomes.append(MSG_test("Conn not catched WriteParameter - \t", Probe_amount, MSG_Open_Close, Drive01.WriteParameter, 0, 700, 0))
###################################################################
#To do  Class 0/1 connection. Covered by ENET_034.

testTime = datetime.utcnow().strftime('%Y_%m_%d__%H_%M_%S.%f')[:-6]
accesRights = 0o755
path = os.getcwd()
os.chdir('EDLTresults/')
os.mkdir(str(testTime), accesRights)

fig1, ax = plt.subplots(nrows=1, ncols=1, sharey='all', figsize=(26, 6))
ax.boxplot(PlotTable, notch=True, widths=0.7)
ax.grid(True)
ax.set_ylabel('Time from request to response [ms]')
ax.set_title("Performance test - Sample rate: " + str(Probe_amount))
pylab.savefig(str(testTime) + '/boxplots.png')

print "#########################################################################"
print "Sample rate: " + str(Probe_amount)

for x in range(Outcomes.__len__()):
    MSG_test_print(Outcomes[x])
print "#########################################################################"
print "Performance test take:" + str(round(time.time() - TestStartTime,2)) + " seconds."
print "Error count: " + str(Drive01.error_counter)
print "https://www.webucator.com/blog/2015/08/python-clocks-explained/"
print "#########################################################################"



