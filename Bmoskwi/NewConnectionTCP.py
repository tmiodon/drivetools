from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.drivers.ethernet_ip import ChannelCfg
from ratools.cip.drivers.ethernet_ip_secure import SocketType
import ratools.cip.messaging as messaging
from time import sleep

static_counter = 0

def dummy_function(conn, msg):
    print msg

def start():
    OT_RPI = 0x01C9C380
    TO_RPI = 0x01C9C380
    multiplier = 0

    OT_CONN_PARAMS = ForwardOpenConnectionParameters(
        connection_size=500,
        connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
        priority=ForwardOpenConnectionParameters.PRIORITY_LOW,
        redundant_owner=False)

    # if connection_type is .CONNECTION_TYPE_POINT_TO_POINT everything is OK
    TO_CONN_PARAMS = ForwardOpenConnectionParameters(
        connection_size=500,
        connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
        priority=ForwardOpenConnectionParameters.PRIORITY_LOW,
        redundant_owner=False)

    conn_params = {
        'transport_class': 3,
        'trigger': 2,
        'direction': 1,
        'to_rpi': TO_RPI,
        'ot_rpi': OT_RPI,
        'connection_path': [(0x20, 0x02),
                            (0x24, 0x01),],
        'connection_timeout_multiplier': multiplier,
        'to_connection_parameters': TO_CONN_PARAMS,
        'ot_connection_parameters': OT_CONN_PARAMS
    }

    conn_path = r'AB_ETHIP-1\192.168.1.101'

    channel_cfg = ChannelCfg(path=conn_path, connected=False, socket_type=SocketType.TCP,authenticate=[])
    channel = channel_cfg.create_channel()
    connection = channel.create_connection(
        receive_thread=False,
        send_thread=False,
        open_connection=True)

    # output_data = 1
    #
    # for x in range(30):
    #
    #     data_to_send = connection.pack_data([0, 1,2,3,4,5,6,7], ['DWORD', 'DWORD', 'DWORD', 'DWORD', 'DWORD', 'DWORD', 'DWORD', 'DWORD'])
    #     while connection.send(data_to_send, wait_for_response= True):
    #         pass
    #     output_data <<= 1
    sleep(3)
    connection.close()
    sleep(5)



start()