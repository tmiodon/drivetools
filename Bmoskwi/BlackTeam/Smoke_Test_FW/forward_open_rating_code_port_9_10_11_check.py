from lvd.test.testers.nose.init import *
from setup_channel import TestBase
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardCloseRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import ratools.cip.messaging as messaging
from ratools.cip.objects.common.identity import Identity
import ratools.cip.utils as utils


triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
                                                  )

ot = ForwardOpenConnectionParameters(
                    connection_size=14,
                    fixed=True,
                    priority= ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
                    connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
                )

to = ot

forward_open_request = ForwardOpenRequest(
    connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
    transport_class=1,
    connection_size=0,
    ot_connection_id=0x00000000,
    to_connection_id=0x002c48ae,
    connection_serial_number=0x0002,
    transport_class_trigger = triger,
    originator_serial_number = 0x12345678,
    connection_timeout_multiplier = 2,
    ot_connection_parameters = ot,
    to_connection_parameters = to
)

forward_close_request = ForwardCloseRequest(
    connection_serial_number=0x0002,
    originator_serial_number = 0x12345678
)


# ObnConnMgr_AopDataDrive structure representing data field of forward open request


# typedef struct
# {
#     /// The configuration revision number
#     uint8_t  configurationRevision;
#     /// The bit host value
#     uint8_t  bitHost;
#     /// Reserved bytes
#     uint8_t  reserved1[ 2 ];
#     /// The drive rating (little endian)
#     uint32_t driveRatingLe;
#     /// The port 9 (Application Port) Device Type (little endian)
#     uint16_t applicationPortDeviceTypeLe;
#     /// The port 9 (Application Port) Product Code (little endian)
#     uint16_t applicationPortProductCodeLe;
#     /// The port 10 (Primary Motor Control Port) Device Type (little endian)
#     uint16_t primaryMotorControlPortDeviceTypeLe;
#     /// The port 10 (Primary Motor Control Port) Product Code (little endian)
#     uint16_t primaryMotorControlPortProductCodeLe;
#     /// The port 11 (Secondary Motor Control Port) Device Type (little endian)
#     uint16_t secondaryMotorControlPortDeviceTypeLe;
#     /// The port 11 (Secondary Motor Control Port) Product Code (little endian)
#     uint16_t secondaryMotorControlPortProductCodeLe;
#     /// The from net datalinks configuration (little endian)
#     uint32_t datalinkFromNetCfg[ NUM_DATALINKS ];
#     /// The to net datalinks configuration (little endian)
#     uint32_t datalinkToNetCfg[ NUM_DATALINKS ];
#     /// Reserved bytes
#     uint8_t  reserved2[ 16 ];
#     /// The configuration signature
#     uint8_t  configSig[ CONFIG_SIGNATURE_SIZE ];
#     /// Reserved bytes
#     uint8_t  reserved3[ 16 ];
# }
# ObnConnMgr_AopDataDrive;


def send_forward_open_request(channel, forward_open_request):
    error_response = None

    try:
        channel.send(forward_open_request.pack())
    except utils.CipResponseError as cipresponseerrorobj:
        error_response = (cipresponseerrorobj.extendedErrorDictionary)

    return error_response

def send_forward_close_request(channel, forward_close_request):
    error_response = None

    try:
        forward_close_response = channel.send(str(forward_close_request.pack()))
    except utils.CipResponseError as cipresponseerrorobj:
        error_response = (cipresponseerrorobj.extendedStatus)

    return error_response

@attr('edlt', 'fw_rating_code')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def _private_method_find_device_ports(self):
        identity = Identity(channel=self.channel)
        arr = []
        for i in range(1, 15 + 1):
            name = identity[i].product_name.get()['string']
            if ('Not Present' not in name) and len(name)>6:
                arr.append(i - 1)
        return arr

    def _private_method_get_configuration_signature(self, port):
        port_ov = port
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_get_single = 0x0E  # GET ATTR SINGLE

        reply = messaging.cipmessage(utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))
        unpacked = utils.unpackData(reply, ['SINT'] * 16)

        return unpacked

    def _private_method_array_to_string(self, conf_array):
        out = ''
        for a in conf_array:
            c = str(hex(a&0xFF)).replace('0x', '').upper()
            if len(c) == 1:
                out = out + '0' + c
            else:
                out = out + c
        return out

    def test_simple_forward_open(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, no electronic key, no data')
        forward_open_request.connection_path =  [
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        assert error_response is None

        error_response = send_forward_close_request(self.channel, forward_close_request)

        assert error_response is None

    def test_forward_open_with_electronic_key(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, contain electronic key, no data')
        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        assert error_response is None

        error_response = send_forward_close_request(self.channel, forward_close_request)

        assert error_response is None

    def test_forward_open_with_electronic_key_and_data(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, contain electronic key and data')

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                (0x80, '0100000006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        assert error_response is None

        error_response = send_forward_close_request(self.channel, forward_close_request)

        assert error_response is None

    def test_forward_open_rating_code_error(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, invalid rating code')

        identity_con = Identity(self.channel)
        vendor_id = identity_con[1].atr.vendor_id
        device_type = identity_con[1].atr.device_type
        product_code = identity_con[1].atr.product_code

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)


        forward_open_request.connection_path =  [
                                                (0x34, (vendor_id,device_type,product_code,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                # 5th byte representing driveRatingLe changed
                                                (0x80, '0100000016000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED, CFGERR_RATING_CODE_INVALID
        assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 4))

    def test_forward_open_configuration_revision_error(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, configuration revision invalid')

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                # 1st byte representing configurationRevision changed
                                                (0x80, '0000000006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED, CFGERR_CONFIGURATION_REVISION_INVALID
        assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 1))

    def test_forward_open_application_port_error(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, application port empty')

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                # 9th byte representing applicationPortDeviceTypeLe changed
                                                (0x80, '0100000006000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        if 'ethernet_ip' not in self.channel.get_type():
            print(r'TEST AVALIBLE ONLY FOR ethernet_ip')
            print(r'PLEASE DEFINE PATH AS ethernet_ip\<ip_address>')

        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED, CFGERR_APPLICATION_PORT_EMPTY
        device_ports = self._private_method_find_device_ports()

        if 9 not in device_ports:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 5))
        else:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 6))

    def test_forward_open_primary_motor_port_error(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, primary motor port empty')

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                # 13th byte representing primaryMotorControlPortDeviceTypeLe changed
                                                (0x80, '0100000006000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        if 'ethernet_ip' not in self.channel.get_type():
            print(r'TEST AVALIBLE ONLY FOR ethernet_ip')
            print(r'PLEASE DEFINE PATH AS ethernet_ip\<ip_address>')

        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED, CFGERR_PRIMARY_MOTOR_CONTROL_PORT_EMPTY
        device_ports = self._private_method_find_device_ports()

        if 10 not in device_ports:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 9))
        else:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 10))

    def test_forward_open_secondary_motor_port_error(self):
        # ------------------------------------------------------------------------------
        print('Forward open class 1, to drive, secondary motor port empty')

        configuration_signature = self._private_method_get_configuration_signature(0)
        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        forward_open_request.connection_path =  [
                                                (0x34, (0,0,0,0,0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                # 13th byte representing secondaryMotorControlPortDeviceTypeLe changed
                                                (0x80, '0100000006000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'+configuration_signature_string)
        ]
        error_response = send_forward_open_request(self.channel, forward_open_request)

        if 'ethernet_ip' not in self.channel.get_type():
            print(r'TEST AVALIBLE ONLY FOR ethernet_ip')
            print(r'PLEASE DEFINE PATH AS ethernet_ip\<ip_address>')

        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED, CFGERR_SECONDARY_MOTOR_CONTROL_PORT_EMPTY
        device_ports = self._private_method_find_device_ports()

        if 11 not in device_ports:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 13))
        else:
            assert ((error_response['extendedStatus'][0] == 272) and (error_response['extendedStatus'][1] == 14))
