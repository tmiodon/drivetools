#############################################################################
### @file
###
### This file contains configuration data that is shared between pytest files.
###
### @par Copyright (c) 2019 Rockwell Automation, Inc. All rights reserved.
#############################################################################

MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS = 15
MAX_NUMBER_OF_CLASS_3_LARGE_CONNECTIONS = 1 
MAX_NUMBER_OF_CIP_SAFETY_CONNECTIONS = 4
TOTAL_NUMBER_OF_CLASS_1_CONNECTIONS = 1 + MAX_NUMBER_OF_CIP_SAFETY_CONNECTIONS

TOTAL_NUMBER_OF_CLASS_3_CONNECTIONS = MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS + MAX_NUMBER_OF_CLASS_3_LARGE_CONNECTIONS                                      
TOTAL_NUMBER_OF_CONNECTIONS = TOTAL_NUMBER_OF_CLASS_1_CONNECTIONS + TOTAL_NUMBER_OF_CLASS_3_CONNECTIONS

# 2048 is a number that EDLT is configured as maximum connection size for large connections.
# this number includes eip header = 24  and cpf header for send unit data = 22 . That is why 44 is subtracted.
LARGE_FORWARD_OPEN_MAX_CONNECTION_SIZE = 2048 - 46