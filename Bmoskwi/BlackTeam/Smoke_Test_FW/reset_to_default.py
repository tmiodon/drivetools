from lvd.test.testers.nose.init import *
from setup_channel import TestBase
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardCloseRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import ratools.cip.messaging as messaging
from ratools.cip.objects.common.tcp_ip_interface import TcpIpInterface
from ratools.cip.objects.common.qos import QoS
import ratools.cip.utils as utils
from ratools.cip.channel import create as create_channel
from ratools.cip.messaging import cipmessage
from ratools.cip.utils import CipResponseError


@attr('edlt', 'fw_reset_to_defaults')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass
    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    # Set qos attrributes to different than defaults
    def test_1_qos_set_attributes(self):
        # ------------------------------------------------------------------------------
        qos = QoS(self.channel)
        try:
            qos[1].svc.set_attribute_single(4,54)
            qos[1].svc.set_attribute_single(5,46)
            qos[1].svc.set_attribute_single(6,42)
            qos[1].svc.set_attribute_single(7,30)
            qos[1].svc.set_attribute_single(8,26)
        except CipResponseError as cipresponseerrorobj:
            print('ERROR:\n' + cipresponseerrorobj.errorDescription)
            assert(False)

    # Set tcp attributes to different than defaults
    def test_2_tcpip_set_attributes(self):
        # ------------------------------------------------------------------------------
        tcp = TcpIpInterface(self.channel)
        try:
            tcp[1].svc.set_attribute_single(6,'exam')
            tcp[1].svc.set_attribute_single(8,2)
            tcp[1].svc.set_attribute_single(10,0)
            tcp[1].svc.set_attribute_single(13,100)
        except CipResponseError as cipresponseerrorobj:
            print('ERROR:\n' + cipresponseerrorobj.errorDescription)
            assert(False)

    # Perform reset to defaults request - dpi Parameter class, instance 0, attribute 2,
    # command 0x3 - Load Ram with default values
    def test_3_reset(self):
        # ------------------------------------------------------------------------------
        class_id = 0x67
        instance = 1
        service = 0x4b
        msg = '074d00234ef1150f000400950a81006303000000020003'

        format_arr = ['USINT']*(len(msg)/2)
        msg_hex = ','.join(msg[i:i+2] for i in range(0, len(msg), 2))
        arr_hex = map(lambda x: int(x,16), msg_hex.split(','))
        formated_data = utils.packdata(arr_hex, format_arr)
        try:
            cipmessage(utils.fmtadr(svc=service, cls=class_id, ins=instance) + formated_data)
        except CipResponseError as cipresponseerrorobj:
            print('ERROR:\n' + cipresponseerrorobj.errorDescription)
            assert(False)

    # Check if QoS attributes are default, set them to non-default values, check if
    # they changed
    def test_4_qos(self):
        # ------------------------------------------------------------------------------
        qos = QoS(self.channel)
        ans = qos[1].svc.get_attribute_single(4)
        assert(ans == 55)
        qos[1].svc.set_attribute_single(4,54)
        ans = qos[1].svc.get_attribute_single(4)
        assert(ans == 54)

        ans = qos[1].svc.get_attribute_single(5)
        assert(ans == 47)
        qos[1].svc.set_attribute_single(5,46)
        ans = qos[1].svc.get_attribute_single(5)
        assert(ans == 46)

        ans = qos[1].svc.get_attribute_single(6)
        assert(ans == 43)
        qos[1].svc.set_attribute_single(6,42)
        ans = qos[1].svc.get_attribute_single(6)
        assert(ans == 42)

        ans = qos[1].svc.get_attribute_single(7)
        assert(ans == 31)
        qos[1].svc.set_attribute_single(7,30)
        ans = qos[1].svc.get_attribute_single(7)
        assert(ans == 30)

        ans = qos[1].svc.get_attribute_single(8)
        assert(ans == 27)
        qos[1].svc.set_attribute_single(8,26)
        ans = qos[1].svc.get_attribute_single(8)
        assert(ans == 26)

    # Check if Tcp attributes are default, set them to non-default values, check if
    # they changed
    def test_4_tcp(self):
        # ------------------------------------------------------------------------------
        tcp = TcpIpInterface(self.channel)

        ans = tcp[1].svc.get_attribute_single(6)
        assert(ans == '')
        tcp[1].svc.set_attribute_single(6,'exam')
        ans = tcp[1].svc.get_attribute_single(6)
        assert(ans == 'exam')

        ans = tcp[1].svc.get_attribute_single(8)
        assert(ans == 1)
        tcp[1].svc.set_attribute_single(8,2)
        ans = tcp[1].svc.get_attribute_single(8)
        assert(ans == 2)

        ans = tcp[1].svc.get_attribute_single(10)
        assert(ans == 1)
        tcp[1].svc.set_attribute_single(10,0)
        ans = tcp[1].svc.get_attribute_single(10)
        assert(ans == 0)

        ans = tcp[1].svc.get_attribute_single(13)
        assert(ans == 120)
        tcp[1].svc.set_attribute_single(13,100)
        ans = tcp[1].svc.get_attribute_single(13)
        assert(ans == 100)

    # Perform reset to defaults request - dpi Parameter class, instance 0, attribute 2,
    # command 0x3 - Load Ram with default values
    def test_5_reset(self):
        self.test_3_reset()