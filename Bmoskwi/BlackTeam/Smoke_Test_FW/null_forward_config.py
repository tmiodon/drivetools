from lvd.test.testers.nose.init import *
from setup_channel import TestBase
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import ratools.cip.messaging as messaging
from ratools.cip.objects.common.identity import Identity
import ratools.cip.utils as utils


def global_send_forward_open_request(channel, forward_open_request):
    error_response = None

    try:
        channel.send(forward_open_request.pack())
    except utils.CipResponseError as cipresponseerrorobj:
        error_response = (cipresponseerrorobj.generalStatus,
                          cipresponseerrorobj.extendedStatus,
                          cipresponseerrorobj.classCode,
                          cipresponseerrorobj.message)
    return error_response

@attr('edlt', 'null_fw_config')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        if 'ethernet_ip' not in self.channel.get_type():
            print(r'TEST AVALIBLE ONLY FOR ethernet_ip')
            print(r'PLEASE DEFINE PATH AS ethernet_ip\<ip_address>')


    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def _private_method_array_to_string(self, conf_array):
        out = ''
        for a in conf_array:
            c = str(hex(a&0xFF)).replace('0x', '').upper()
            if len(c) == 1:
                out = out + '0' + c
            else:
                out = out + c
        return out

    def _private_method_get_configuration_signature(self, port):
        port_ov = port
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_get_single = 0x0E  # GET ATTR SINGLE

        reply = messaging.cipmessage(utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))
        unpacked = utils.unpackData(reply, ['SINT'] * 16)

        return unpacked

    def _private_method_set_configuration_signature(self, port_ov, data_arr):
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_set_single = 0x10  # SET ATTR SINGLE

        # SET CONFIG_SIGNATURE
        # data_arr = []
        format_arr = []
        for i in range(16):
            format_arr.append('SINT')

        formated_data = utils.packdata(data_arr, format_arr)
        reply = messaging.cipmessage(
            utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)
        return reply


    #   Null forward open signature check with data to class other than assembly
    def null_forward_signature_check_data_bad_class(self):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive, Assembly object')

        trigger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        o2t = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        t2o = o2t

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=trigger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=0x07,
            ot_connection_parameters=o2t,
            to_connection_parameters=t2o,
            ot_rpi=0,
            to_rpi=0
        )

        # Port Override 0x01
        port_ovverride = 0x0c
        configuration_signature = '05050505050505050505050505050505'

        #configuration_signature = '11121305050505050505050505050505'

        forward_open_request.connection_path = [(0x01, (port_ovverride)),
                                                (0x34, (0, 0, 0, 0, 0)),
                                                (0x20, 0xF4),   # Class Id 0xF4
                                                (0x24, 0x01),   # Instance Id 0x01
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                (0x80, ('01000000%s'%configuration_signature) )
                                                # (0x80, configuration_signature)
                                                ]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response

    def null_forward_signature_check_data_zero_port_bad_data_size(self):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive, Assembly object')

        trigger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        o2t = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        t2o = o2t

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=trigger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=0x07,
            ot_connection_parameters=o2t,
            to_connection_parameters=t2o,
            ot_rpi=0,
            to_rpi=0
        )

        request_data = "0A0B0C0D"

        # Port Override 0x01
        forward_open_request.connection_path = [(0x01, (0x00)),
                                                (0x34, (0, 0, 0, 0, 0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                (0x80, request_data)
                                                ]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response


    def null_forward_signature_check_data_zero_port(self, configuration_signature_string):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive, Assembly object')

        trigger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        o2t = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        t2o = o2t

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=trigger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=0x07,
            ot_connection_parameters=o2t,
            to_connection_parameters=t2o,
            ot_rpi=0,
            to_rpi=0
        )

        request_data = '0100000006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
        request_data = request_data + configuration_signature_string

        # Port Override 0x01
        forward_open_request.connection_path = [(0x01, (0x00)),
                                                (0x34, (0, 0, 0, 0, 0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                (0x80, request_data)
                                                ]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response

    #   Null forward open signature check with data
    def null_forward_signature_check_data(self, configuration_signature_string, device_port):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive, Assembly object')

        trigger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        o2t = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        t2o = o2t

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=trigger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=0x07,
            ot_connection_parameters=o2t,
            to_connection_parameters=t2o,
            ot_rpi=0,
            to_rpi=0
        )

        # Port Override 0x01
        port_ovverride = device_port
        configuration_signature = configuration_signature_string

        forward_open_request.connection_path = [(0x01, (port_ovverride)),
                                                (0x34, (0, 0, 0, 0, 0)),
                                                (0x20, 0x04),
                                                (0x24, 0x06),
                                                (0x2c, 0x02),
                                                (0x2c, 0x01),
                                                (0x80, ('01000000%s' % configuration_signature))
                                                # (0x80, configuration_signature)
                                                ]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response

    def _private_method_test_port_zero_request(self):
        print("GET PORT 0 CONFIGURATION SIGNATURE")
        configuration_signature = self._private_method_get_configuration_signature(0)
        print("PORT 0 CONFIGURATION SIGNATURE:")
        print(configuration_signature)

        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        print("SENDING CORRECT SIGNATURE")
        print("EXPECTING CORRECT ANSWER:")
        ret = self.null_forward_signature_check_data_zero_port(configuration_signature_string)
        assert ret is None
        print("RECEIVED CORRECT ANSWER:")
        print("GET PORT 0 CONFIGURATION SIGNATURE OK \r\n")

    def _private_method_test_device_port_request(self, device_port):
        print("GET PORT %s CONFIGURATION SIGNATURE" % device_port)
        configuration_signature = self._private_method_get_configuration_signature(device_port)
        print("PORT %s CONFIGURATION SIGNATURE:" % device_port)
        print(configuration_signature)

        configuration_signature_string = self._private_method_array_to_string(configuration_signature)

        print("SENDING CORRECT SIGNATURE")
        print("EXPECTING CORRECT ANSWER:")
        ret = self.null_forward_signature_check_data(configuration_signature_string, device_port)
        assert ret is None
        print("RECEIVED CORRECT ANSWER:")
        print("GET PORT %s CONFIGURATION SIGNATURE OK \r\n" % device_port)
        pass

    def _private_method_test_device_port_request_bad_signature(self, device_port):
        print("GET PORT %s CONFIGURATION SIGNATURE" % device_port)
        recover = self._private_method_get_configuration_signature(device_port)
        print("PORT %s CONFIGURATION SIGNATURE TO RECOVER:" % device_port)
        print(recover)

        fake_conf_sig = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16]

        print("SET PORT %s CONFIGURATION SIGNATURE" % device_port)
        self._private_method_set_configuration_signature(device_port, fake_conf_sig)
        print("PORT %s CONFIGURATION SIGNATURE:" % device_port)
        print(self._private_method_get_configuration_signature(device_port))

        bad_configuration_string = r'05050505050505050505050505050505'
        print("SENDING BAD SIGNATURE")
        print("EXPECTING BAD ANSWER:")

        if device_port == 0:
            ret = self.null_forward_signature_check_data_zero_port(bad_configuration_string)
        else:
            ret = self.null_forward_signature_check_data(bad_configuration_string, device_port)

        print("RECOVER PORT %s CONFIGURATION SIGNATURE" % device_port)
        self._private_method_set_configuration_signature(device_port, recover)
        print("PORT %s CONFIGURATION SIGNATURE:" % device_port)
        print(self._private_method_get_configuration_signature(device_port))

        print("ASSERTING INVALID ATTRIBUTE VALUE")
        assert ret[0] == 0x09
        print("EXPECTED ERROR RECEIVED")
        print("\r\n")
        pass

    def _private_method_find_device_ports(self):
        identity = Identity(channel=self.channel)
        arr = []
        for i in range(1, 15 + 1):
            name = identity[i].product_name.get()['string']
            if 'Not Present' not in name and 'Port' in name:
                arr.append(i - 1)
        return arr

    def test_fw_open_configuration_signature_port_zero_bad_length(self):
        print('===test_port_zero_bad_request_size==')
        self.channel.set_timeout(50000)
        response = self.null_forward_signature_check_data_zero_port_bad_data_size()
        assert response[0] == 1
        assert response[1] == 272

    def test_fw_open_configuration_signature_port_zero(self):
        print('===test_port_zero==')

        for i in range(5):
            self._private_method_test_port_zero_request()

        for i in range(5):
            self._private_method_test_device_port_request_bad_signature(0)

    def test_fw_open_configuration_signature_device_port(self):
        print('===test_device_port==')

        device_ports = self._private_method_find_device_ports()

        for device_port in device_ports:
            self._private_method_test_device_port_request(device_port)

        for device_port in device_ports:
            self._private_method_test_device_port_request_bad_signature(device_port)

    def test_bad_request_path(self):
        print('===test_bad_request_path==')

        for i in range(5):
            response = self.null_forward_signature_check_data_bad_class()
            assert response[0] == 1
            assert response[1] == 789