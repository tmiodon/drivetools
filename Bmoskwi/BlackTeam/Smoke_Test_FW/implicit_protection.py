from lvd.test.testers.nose.init import *
from ratools.cip.channel import create as create_channel
from setup_channel import TestBase
from ratools.cip.objects.common.identity import Identity
from ratools.cip.objects.common.ethernet_link import EthernetLink
from ratools.cip.objects.common.ethernet_link import AdminState
from ratools.cip.objects.common.identity import ResetType
from ratools.cip.objects.common.identity.constants import ExtendedDeviceStatus
from ratools.cip.messaging import cipmessage
from ratools.cip.messaging import getcurrentchannel
from ratools.cip.messaging import restorechannel
from ratools.cip.messaging import savechannel
from ratools.cip.messaging import sethandler
from ratools.cip.utils import CipResponseError
from ratools.cip.objects.exceptions import CnvResponseError
from ratools.cip.constants import Driver
import ratools.cip.drivers.ethernet_ip as ethernet_ip_driver

from ratools.cip.channel._channelcfg import ChannelCfg
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger

import binascii
import os
import struct
import time

from pprint import pprint

import ratools.cip.utils as utils

'''
    TEST DESCRIPTION:
        This test tests if its possible to send reset or update service during opened I/O connection
        Additionally it checks if Identity protection attribute is set correctly
        Warning if this test fails it will change machine state so the rest of the tests will fail.
        If this test is failing it can crash other tests as it will set update state on the machine or perform reset.
'''
@attr('edlt', 'implicit_protection')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        self.continue_testing = True
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def close_class_one_connection(self):
        global send_output
        if self.class_1_connection is not None and self.class_1_connection.is_open():
            send_output = False  # Disable sending
            print("CONNECTION CLOSE")
            self.class_1_connection.close()  # Close connection
            print("CONNECTION DELETE")
            self.class_1_connection.delete()  # Delete connection

    def open_class_one_connection_with_no_datalinks(self):
        ot = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        to = ForwardOpenConnectionParameters(
            connection_size=14,  # No Data Link (14-No Data Link 18-One Data Link)
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_CLIENT)

        # AOP Structure containing Data Links
        aop_data_py = [
            ['configurationRevision', "01"],
            ['bitHost', "00"],
            ['reserved1', "0000"],
            ['driveRatingLe', "0e000000"],
            ['applicationPortDeviceTypeLe', "0000"],
            ['applicationPortProductCodeLe', "0000"],
            ['primaryMotorControlPortDeviceTypeLe', "0000"],
            ['primaryMotorControlPortProductCodeLe', "0000"],
            ['secondaryMotorControlPortDeviceTypeLe', "0000"],
            ['secondaryMotorControlPortProductCodeLe', "0000"],
            ['datalinkFromNetCfg', "00000000" * 16],
            ['datalinkToNetCfg', "00000000" * 16],
            ['reserved2', "00" * 16],
            ['configSig', "00" * 16],
            ['reserved3', "00" * 16]
        ]
        # Set first Datalink to port 0 attribute 3 - DC Bus Volts
        # aop_data_py[11][1] = "03000000" + ("00000000" * 15)

        aop_data_str = ""
        for segment in aop_data_py:
            aop_data_str += segment[1]

        conn_param = {
            # electronic key = 0x34, 0's are used for controller electronic key values
            'connection_path': [(0x34, (0, 0, 0, 0, 0)),
                                (0x20, 0x04),
                                (0x24, 0x06),
                                (0x2c, 0x02),
                                (0x2c, 0x01),
                                (0x80, aop_data_str)],
            'connection_timeout_multiplier': 1,
            'ot_connection_size': 3,
            'ot_rpi': 100 * 1000,  # .1 Sec
            'to_connection_size': 2,
            'to_rpi': 100 * 1000,  # .1 Sec
            'transport_class': 1,  #
            'ot_connection_parameters': ot,
            'to_connection_parameters': to,
            'transport_class_trigger': triger
        }

        global received
        received = 0x00

        global run_mode
        run_mode = "01"

        global verbose_output
        verbose_output = True

        global send_output
        send_output = True

        def recieve_thread_test(connection_obj, received_data):
            global received
            received = received_data
            received_data_str = binascii.b2a_hex(received_data)
            if verbose_output:
                print("received data: %s" % received_data_str)
            pass

        # If send thread returns None send won't be called
        def send_thread_test(connection_obj):
            global send_output

            if verbose_output:
                print("run mode: %s" % run_mode)

            str_message = run_mode + "0000000000000000000000"

            if send_output:
                return binascii.a2b_hex(str_message)
            else:
                return None

                self.class_1_connection = None

        try:
            self.class_1_connection = self.channel.create_connection(
                send_thread=send_thread_test,
                receive_thread=recieve_thread_test,
                open_connection=True,
                **conn_param)
        except CipResponseError as cre:
            print("CIP CONNECTION ERROR")
            cip_general_status = cre.generalStatus
            cip_hex_general_status = format(cre.generalStatus, '02x')
            print("CONNECTION ERROR: %d (0x%s)" % (cip_general_status, cip_hex_general_status))

    def wait_seconds(self, seconds):
        now = start = time.time()
        while now - start < seconds:
            now = time.time()

    def check_if_machine_in_right_state(self, expected_state):
        hpc_identity = Identity(channel=self.channel)
        hpc_identity = hpc_identity[1].status.get()['extended_device_status']
        assert expected_state == hpc_identity
        return hpc_identity

    def check_protection_mode(self, expected_state):
        identity = Identity(self.channel)
        implicit_protection = identity[1].atr[19]['implicit_protection_setting']['implicit_protection'].get()
        assert expected_state == implicit_protection
        return implicit_protection

    def send_update(self, rfw_size):
        # Construct the message
        size = rfw_size
        base_address = 0x00000000
        cls = 0xA1  # CIP_CLASS_NVS
        ins = 1
        service = 0x4B  # SERVICE_NVS_UPDATE

        data = [service, [[cls, ins]], size, base_address]
        fmt = ['SINT', 'IOI', 'UDINT', 'UDINT']
        msg = utils.packData(data, fmt)

        # send message
        try:
            reply = cipmessage(msg)
            return 0x00
        except CipResponseError as cip_exception:
            cip_general_status = cip_exception.generalStatus
            cip_hex_general_status = format(cip_exception.generalStatus, '02x')
            print("UPDATE ERROR: %d (0x%s)" % (cip_general_status, cip_hex_general_status))
            return cip_general_status

    def send_reset(self):
        identity = Identity(self.channel)
        try:
            response = identity[1].svc.reset(reset_type=ResetType.power_cycle)
            return 0x00
        except CipResponseError as cip_exception:
            cip_general_status = cip_exception.generalStatus
            cip_hex_general_status = format(cip_exception.generalStatus, '02x')
            print("RESET ERROR: %d (0x%s)" % (cip_general_status, cip_hex_general_status))
            return cip_general_status

    def send_disable_ethernet_link(self):
        eth_link = EthernetLink(self.channel)
        try:
            eth_link[1].atr.admin_state = AdminState.disable
            return 0x00
        except CnvResponseError as cip_exception:
            status = cip_exception.attributes.get(9).status
            return status.value

    def test_implicit_protection(self):
        if self.continue_testing is False:
            print("TEST SKIPPED")
            return

        DEVICE_STATE_CONFLICT = 0x10

        self.check_if_machine_in_right_state(ExtendedDeviceStatus.no_io_connections_established)

        #   Open class one connection
        self.class_1_connection = None
        self.open_class_one_connection_with_no_datalinks()
        self.wait_seconds(2)
        self.check_if_machine_in_right_state(ExtendedDeviceStatus.at_least_one_io_connection_in_run_mode)

        #   Check protection mode
        self.check_protection_mode(True)

        #   Check if update will be prevented
        update_error = self.send_update(0xFFFF)
        assert update_error == DEVICE_STATE_CONFLICT

        #   Check if reset will be prevented
        reset_error = self.send_reset()
        assert reset_error == DEVICE_STATE_CONFLICT

        #   Check if change of ethernet link will be prevented
        response = self.send_disable_ethernet_link()
        assert response == DEVICE_STATE_CONFLICT

        self.close_class_one_connection()
