from lvd.test.testers.nose.init import *

from ratools.cip.objects.common.port import Port
from setup_channel import TestBase
import time
from ratools.cip.utils import CipResponseError
from ratools.cip.messaging import cipmessage
from ratools.cip.constants import Driver
import ratools.cip.drivers.ethernet_ip as ethernet_ip_driver
from ratools.cip.channel._channelcfg import ChannelCfg
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import binascii
import ratools.cip.utils as utils
import ratools.cip.messaging as messaging

from pprint import pprint


@attr('edlt', 'class_one_conf_sig')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):

        print("SAVE MACHINE CONFIGURATION SIGNATURE")

        self.original_configuration_signature = self.get_configuration_signature(0)
        print(self.array_to_string(self.original_configuration_signature))

        print("SETUP PREDICTABLE CONFIGURATION SIGNATURE")

        self.set_configuration_signature(0,([0x01] * 16))

        print("CONFIGURATION SIGNATURE SET AS: %s" %  self.array_to_string([0x01] * 16) )

        pass

    # teardown() after each test method"
    def teardown(self):
        print("RECOVER MACHINE CONFIGURATION SIGNATURE")
        self.set_configuration_signature(0,self.original_configuration_signature)
        temp = self.get_configuration_signature(0)
        print("RECOVERED SIGNATURE")
        print(self.array_to_string(temp))
        pass

    def open_class_one_connection_with_no_datalinks(self, configuration_signature=None):

        ot = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        to = ForwardOpenConnectionParameters(
            connection_size=14,  # 14 - Zero Data Link 18 - One Data Link
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_CLIENT)

        # AOP Structure containing Data Links
        aop_data_py = [
            ['configurationRevision', "01"],
            ['bitHost', "00"],
            ['reserved1', "0000"],
            ['driveRatingLe', "0e000000"],
            ['applicationPortDeviceTypeLe', "0000"],
            ['applicationPortProductCodeLe', "0000"],
            ['primaryMotorControlPortDeviceTypeLe', "0000"],
            ['primaryMotorControlPortProductCodeLe', "0000"],
            ['secondaryMotorControlPortDeviceTypeLe', "0000"],
            ['secondaryMotorControlPortProductCodeLe', "0000"],
            ['datalinkFromNetCfg', "00000000" * 16],
            ['datalinkToNetCfg', "00000000" * 16],
            ['reserved2', "00" * 16],
            ['configSig', "00" * 16],  # Configuration Signature
            ['reserved3', "00" * 16]
        ]

        # If we would like to establish a Datalink we would have to add it to Datalinks in AOP data
        # For example to set first Datalink to port 0 attribute 3 - DC Bus Volts we would do:
        # aop_data_py[11][1]="03000000" + ("00000000" * 15)
        # "03000000" is Representation of Integer "3" ( we are using Little-Endian)
        # "00000000" * 15 will result in string with 120 zeros which represent 15 zero integers (Empty/Not Established Datalinks)
        # (each "00" is one byte each "00000000" represents 4-byte Integer). We then can concatenate those by using + operator
        # aop_data_py[11] is datalinkToNetCfg, in HPC it is a table of sixteen 32bit Integers
        # Without this there will be no established Datalinks

        if configuration_signature is not None:
            aop_data_py[13][1] = configuration_signature

        aop_data_str = ""
        for segment in aop_data_py:
            aop_data_str += segment[1]

        conn_param = {
            # electronic key = 0x34, 0's are used for controller electronic key values
            'connection_path': [(0x34, (0, 0, 0, 0, 0)), (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01),
                                (0x80, aop_data_str)],
            'connection_timeout_multiplier': 1,
            'ot_connection_size': 3,
            'ot_rpi': 100 * 1000,  # .1 Sec
            'to_connection_size': 2,
            'to_rpi': 100 * 1000,  # .1 Sec
            'transport_class': 1,  #
            'ot_connection_parameters': ot,
            'to_connection_parameters': to,
            'transport_class_trigger': triger
        }

        path = self.channel.get_path_string()
        driver = Driver('ethernet_ip')

        channel_cfg = ChannelCfg(path=path, driver=driver, connected=False)
        channel = ethernet_ip_driver.Channel(path, None, channel_cfg)

        channel.set_timeout(50000)
        global recieved
        recieved = 0x00

        global run_mode
        run_mode = "01"

        connection = None

        try:
            connection = channel.create_connection(
                open_connection=True,
                **conn_param)

            print("Connection Close")

            connection.close()

        except CipResponseError as cipresponseerrorobj:
            error_response = (cipresponseerrorobj.generalStatus, cipresponseerrorobj.extendedStatus)
            return(error_response)

        return None

    def array_to_string(self, conf_array):
        out = ''
        for a in conf_array:
            c = str(hex(a & 0xFF)).replace('0x', '').upper()
            if len(c) == 1:
                out = out + '0' + c
            else:
                out = out + c
        return out

    def get_configuration_signature(self, port):
        port_ov = port
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_get_single = 0x0E  # GET ATTR SINGLE

        reply = cipmessage(utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))
        unpacked = utils.unpackData(reply, ['SINT'] * 16)

        return unpacked

    def set_configuration_signature(self, port_ov, data_arr):
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_set_single = 0x10  # SET ATTR SINGLE

        # SET CONFIG_SIGNATURE
        format_arr = []
        for i in range(16):
            format_arr.append('SINT')

        formated_data = utils.packdata(data_arr, format_arr)
        reply = messaging.cipmessage(
            utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)
        return reply

    def test_class_one_conf_sig(self):

        # Test Class setup method sets configuration signature to 01*16
        print("OPENING CLASS ONE CONECTION WITH BAD CONFIGURATION SIGNATURE EXPECTING ERROR")
        error_code = self.open_class_one_connection_with_no_datalinks(self.array_to_string([0x02] * 16))

        CIP_STATUS_INVALID_ATTRIBUTE_VALUE = 0x09

        if error_code is not None:
            assert error_code[0] == CIP_STATUS_INVALID_ATTRIBUTE_VALUE
        else:
            assert False # No error even with bad configuration signature we should got INVALID ATTRIBUTE VALUE

        print("OPENING CLASS ONE CONECTION WITH CORRECT CONFIGURATION SIGNATURE EXPECTING PASS")
        error_code = self.open_class_one_connection_with_no_datalinks(self.array_to_string([0x01] * 16))

        assert error_code == None

        return True
