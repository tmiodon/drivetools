from lvd.test.testers.nose.init import *
from ratools.cip.channel import create as create_channel
from setup_channel import TestBase
from ratools.cip.objects.common.identity import Identity
from ratools.cip.objects.common.identity.constants import ExtendedDeviceStatus
from ratools.cip.messaging import cipmessage
from ratools.cip.messaging import getcurrentchannel
from ratools.cip.messaging import restorechannel
from ratools.cip.messaging import savechannel
from ratools.cip.messaging import sethandler
from ratools.cip.utils import CipResponseError

from ratools.cip.constants import Driver
import ratools.cip.drivers.ethernet_ip as ethernet_ip_driver

from ratools.cip.channel._channelcfg import ChannelCfg
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger

import binascii
import os
import struct
import time

import ratools.cip.utils as utils

'''
    TEST DESCRIPTION:
        This test tests if its possible to change established datalinks if HPC is connected to PC acting like plc
        Test saves existing datalinks from HPC.
        Then sets own datalink table in HPC and tries to pen class 1 connection to HPC.
        After establishing the connection it tries to change working datalinks expecting this operation to fail.
        Then it closes class 1 connection changes datalinks table and tries to onece again open class 1 connection.
        This time it expects the forward open to fail as the datalinks will differ between data in Forward Open
        and datalinks HPC. Then it set up datalinks on device to match Forward Opens and once again try to establish
        class 1 connection.
        If everything is as expected it recovers Datalinks saved in First step
'''
@attr('edlt', 'datalink_locking')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        self.continue_testing = False
        if 'ethernet_ip' in self.channel.get_path_string():
            self.continue_testing = True
        else:
            print("Please Specify ethernet_ip Driver to run this test for example: ethernet_ip\\192.168.1.163  ")

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def private_method_set_datalink_to_net(self, i, value):
        if i < 0 or i > 15:
            return
        class_id = 0x93  # CIP_CLASS_DPI_PARAMETER 0x93U
        instance = 340 + i
        service_set_single = 0x10  # GET ATTR SINGLE
        attribute = 9

        format_arr = ['UDINT']
        data_arr = [value]
        formated_data = utils.packdata(data_arr, format_arr)

        cipmessage(utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)
        return

    def private_method_get_datalink_to_net(self, i):
        if i < 0 or i > 15:
            return
        class_id = 0x93  # CIP_CLASS_DPI_PARAMETER 0x93U
        instance = 340 + i
        service_get_single = 0x0E  # GET ATTR SINGLE
        attribute = 9

        reply = cipmessage(utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))
        unpacked = utils.unpackData(reply, ['UDINT'])
        return unpacked[0]

    def private_method_get_datalink_to_net_array(self):
        stored_data_links = []

        for i in range(16):
            link_value = self.private_method_get_datalink_to_net(i)
            stored_data_links.append(link_value)

        return stored_data_links

    def private_method_set_datalink_to_net_array(self, set_array):
        for i in range(16):
            link_value = self.private_method_set_datalink_to_net(i, set_array[i])

    def open_class_one_connection_with_one_datalink(self):

        ot = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        to = ForwardOpenConnectionParameters(
            connection_size=18,  # One Data Link
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT
        )

        triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_CLIENT)

        # AOP Structure containing Data Links
        aop_data_py = [
            ['configurationRevision', "01"],
            ['bitHost', "00"],
            ['reserved1', "0000"],
            ['driveRatingLe', "0e000000"],
            ['applicationPortDeviceTypeLe', "0000"],
            ['applicationPortProductCodeLe', "0000"],
            ['primaryMotorControlPortDeviceTypeLe', "0000"],
            ['primaryMotorControlPortProductCodeLe', "0000"],
            ['secondaryMotorControlPortDeviceTypeLe', "0000"],
            ['secondaryMotorControlPortProductCodeLe', "0000"],
            ['datalinkFromNetCfg', "00000000" * 16],
            ['datalinkToNetCfg', "00000000" * 16],
            ['reserved2', "00" * 16],
            ['configSig', "00" * 16],
            ['reserved3', "00" * 16]
        ]
        # Set first Datalink to port 0 attribute 3 - DC Bus Volts
        aop_data_py[11][1] = "03000000" + ("00000000" * 15)

        aop_data_str = ""
        for segment in aop_data_py:
            aop_data_str += segment[1]

        conn_param = {
            # electronic key = 0x34, 0's are used for controller electronic key values
            'connection_path': [(0x34, (0, 0, 0, 0, 0)),
                                (0x20, 0x04),
                                (0x24, 0x06),
                                (0x2c, 0x02),
                                (0x2c, 0x01),
                                (0x80, aop_data_str)],
            'connection_timeout_multiplier': 1,
            'ot_connection_size': 3,
            'ot_rpi': 100 * 1000,  # .1 Sec
            'to_connection_size': 2,
            'to_rpi': 100 * 1000,  # .1 Sec
            'transport_class': 1,  #
            'ot_connection_parameters': ot,
            'to_connection_parameters': to,
            'transport_class_trigger': triger
        }

        connection_channel_path = self.channel.get_path_string()
        driver = Driver('ethernet_ip')

        channel_cfg = ChannelCfg(path=connection_channel_path, driver=driver, connected=False)
        connection_channel = ethernet_ip_driver.Channel(connection_channel_path, None, channel_cfg)

        global received
        received = 0x00

        global run_mode
        run_mode = "01"

        global verbose_output
        verbose_output = True

        def recieve_thread_test(connection_obj, received_data):
            global received
            received = received_data
            received_data_str = binascii.b2a_hex(received_data)

            data_link_from_device = received_data_str[24:]
            data_link_swap = data_link_from_device[6:8]
            data_link_swap += data_link_from_device[4:6]
            data_link_swap += data_link_from_device[2:4]
            data_link_swap += data_link_from_device[0:2]

            dc_bus_voltage = struct.unpack('!f', data_link_swap.decode('hex'))[0]

            if verbose_output:
                print("received data: %f" % dc_bus_voltage)
            pass

        def send_thread_test(connection_obj):
            if verbose_output:
                print("run mode: %s" % run_mode)
            str_message = run_mode + "0000000000000000000000"
            return binascii.a2b_hex(str_message)

        class_1_connection = connection_channel.create_connection(
            send_thread=send_thread_test,
            receive_thread=recieve_thread_test,
            open_connection=True,
            **conn_param)

        self.class_1_connection = class_1_connection

    def close_class_one_connection(self):
        if self.class_1_connection is not None:
            self.class_1_connection.close()

    def wait_seconds(self, seconds):
        now = start = time.time()
        while now - start < seconds:
            now = time.time()

    def check_if_machine_in_right_state(self, expected_state):
        hpc_identity = Identity(channel=self.channel)
        hpc_identity = hpc_identity[1].status.get()['extended_device_status']
        assert expected_state == hpc_identity
        return hpc_identity

    def test_class_one(self):

        if self.continue_testing is False:
            return

        # Check if there is no opened class 1 connection so we can send Forward Open
        self.check_if_machine_in_right_state(ExtendedDeviceStatus.no_io_connections_established)

        # Store existing datalinks from HPC
        stored_data_links = self.private_method_get_datalink_to_net_array()

        # Setup datalinks matching our Forward Open
        print("SETTING UP TEST DATALINK")
        self.private_method_set_datalink_to_net_array([3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        # Open class one connection with one datalink (3)
        self.open_class_one_connection_with_one_datalink()

        # Wait 2 seconds to be sure that connection was established
        self.wait_seconds(2)
        # Check if according to HPC we have opened class one connection
        self.check_if_machine_in_right_state(ExtendedDeviceStatus.at_least_one_io_connection_in_run_mode)

        # Define statuses that we can expect
        CIP_ERROR_DEVICE_STATE_CONFLICT = 0x10
        CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED  = 0x0110
        CIP_STATUS_CONNECTION_FAILURE = 0x01

        # Try to set established datalink to 0 and expect to fail with CIP_ERROR_DEVICE_STATE_CONFLICT status
        try:
            print("TRYING TO DISABLE ESTABLISHED DATALINK")
            self.private_method_set_datalink_to_net(0, 0)
            assert False
        except CipResponseError as cipresponseerrorobj:
            assert (cipresponseerrorobj.generalStatus == CIP_ERROR_DEVICE_STATE_CONFLICT)
            print("FAILED AS EXPECTED")

        # Try to set established datalink to 25 and expect to fail with CIP_ERROR_DEVICE_STATE_CONFLICT status
        try:
            print("TRYING TO CHANGE ESTABLISHED DATALINK")
            self.private_method_set_datalink_to_net(0, 25)
            assert False
        except CipResponseError as cipresponseerrorobj:
            assert (cipresponseerrorobj.generalStatus == CIP_ERROR_DEVICE_STATE_CONFLICT)
            print("FAILED AS EXPECTED")

        # Change data that is sent from the PC to simulate going into program mode
        print("DISABLING RUN MODE CHANGING INTO PROGRAM MODE")
        global run_mode
        run_mode = "00"
        # Wait 2 seconds to be sure that HPC received at least one packet suggesting that we are in program mode
        self.wait_seconds(2)

        # As we are in program mode we can change datalinks
        print("TRYING TO CHANGE ESTABLISHED DATALINK")
        self.private_method_set_datalink_to_net(0, 25)
        datalink_from_net = self.private_method_get_datalink_to_net(0)
        assert 25 == datalink_from_net

        print("DATALINK CHANGED SUCCESSFULLY AFTER GOING INTO PROGRAM MODE")
        print("CLOSE DATALINK")
        self.close_class_one_connection()

        # Try to open class 1 connection with not matching datalinks and expect to fail with
        # CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED substatus
        print("TRY TO OPEN CONNECTION WITH WRONG DATALINK TABLE")
        try:
            self.open_class_one_connection_with_one_datalink()
            assert False
        except CipResponseError as cipresponseerrorobj:
            from pprint import pprint
            assert cipresponseerrorobj.generalStatus == CIP_STATUS_CONNECTION_FAILURE
            assert cipresponseerrorobj.extendedStatus == CIP_EXT_STATUS_DEVICE_NOT_CONFIGURED

        # Change datalinks on device to match ones in Forward Open
        print("CHANGING TO CORRECT DATALINKS")
        self.private_method_set_datalink_to_net_array([3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        print("OPENING CONNECTION")
        # Establish class one connection
        self.open_class_one_connection_with_one_datalink()
        # Simulate going into run mode
        run_mode = "01"
        self.wait_seconds(2)
        self.close_class_one_connection()

        # Recover datalinks saved from device before start of test
        print("RECOVERING DATALINKS")
        self.private_method_set_datalink_to_net_array(stored_data_links)
