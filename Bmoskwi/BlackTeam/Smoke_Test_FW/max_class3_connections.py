#############################################################################
### @file
###
### Smoke test for opening max number of class 3 connections and sends through
### them identity requests for 10s.
###
### @par Copyright (c) 2019 Rockwell Automation, Inc. All rights reserved.
#############################################################################
from lvd.test.testers.nose.init import *
from ratools.cip.objects.common.identity import Identity
from ratools.cip.objects.common.message_router import MessageRouter
import ratools.cip.utils as utils
import time
from ratools.cip.objects.constants import ZzStatusCode
from setup_channel import TestBase
from eiptoolkit_pytest_config import MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS
import time

from ratools.cip.classes.connection_manager import (
    ForwardOpenRequest,
    ForwardCloseRequest,
    ForwardOpenTransportClassTrigger,
    ForwardOpenConnectionParameters)
import ratools.cip.sim.connUtils as conn_utils

'''
    INSTRUCTION HOW TO RUN THIS TEST :
    
    To run this test you need:
         - 0 active connections
'''
@attr('edlt', 'max_class3_connections')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def test_max_class3_connections(self):
        self.try_to_setup_channel(False)
        unconnected_channel = self.channel
        messageRouter = MessageRouter(unconnected_channel)

        print("Check if there is no active connections.")
        assert int(messageRouter[1].atr.number_active) == 0

        connection_channels = []
        identity_obj = []

        for i in range(MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS) :
            self.try_to_setup_channel(True)
            connection_channels.append(self.channel)
            identity_obj.append(Identity(connection_channels[i]))

        print("Check if all connections has been established.")

        assert int(messageRouter[1].atr.number_active) == MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS

        print("Test of class 3 connections, after 10s all connections will be closed.")

        start_time = time.time()

        while( time.time() - start_time < 10 ):
            for i in range(MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS) :
                identity_obj[i][1].atr.vendor_id
            print("."),

        #close all connections
        for i in range(MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS) :
            connection_channels[i].close()

        print("\nCheck if all connections has been closed.")
        assert int(messageRouter[1].atr.number_active) == 0
