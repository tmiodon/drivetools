from lvd.test.testers.nose.init import *
from setup_channel import TestBase
from ratools.cip.classes.connection_manager import ForwardOpenRequest
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import ratools.cip.messaging as messaging
from ratools.cip.objects.common.identity import Identity
import ratools.cip.utils as utils
from random import randint
import re


def global_send_forward_open_request(channel, forward_open_request):
    error_response = None

    try:
        channel.send(forward_open_request.pack())
    except utils.CipResponseError as cipresponseerrorobj:
        error_response = (cipresponseerrorobj.generalStatus,
                          cipresponseerrorobj.extendedStatus,
                          cipresponseerrorobj.classCode,
                          cipresponseerrorobj.message)
    return error_response


@attr('edlt', 'null_fw_ping')
class TestUM(TestBase):

    defines = '''
        #define CIP_EXT_ERROR_VENDOR_PRODUCT_CODE_MISMATCH       0x0114U
        #define CIP_EXT_ERROR_PRODUCT_TYPE_MISMATCH              0x0115U
        #define CIP_EXT_ERROR_REVISION_MISMATCH                  0x0116U
    '''

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def _private_method_get_class_define(self, name):
        defines = TestUM.defines.replace('\n','').replace('\r','')
        defines = defines.strip()
        defines_aray_temp = defines.split('#define')
        defines_dict = {}
        for t in defines_aray_temp:
            t = t.strip()
            if len(t) > 0:
                r = re.sub('[\s]+', ';', t)
                splited = r.split(';')
                defines_dict[splited[0]] = splited[1].replace('U','')

        if defines_dict.has_key(name) is False:
            return None

        value = defines_dict[name]

        int_value = 0
        if '0x' in value:
            value = value.replace('0x','')
            int_value = int(value, 16)
        else:
            int_value = int(value, 0)

        return int_value

    def _private_method_any_other_value(self, input):
        output = randint(2, 9)
        while input == output:
            output = randint(2, 9)
        return output

    def _private_method_array_to_string(self, conf_array):
        out = ''
        for a in conf_array:
            c = str(hex(a)).replace('0x', '').upper()
            if len(c) == 1:
                out = out + '0' + c
            else:
                out = out + c
        return out

    def _private_method_get_electronic_key(self):

        identity = Identity(channel=self.channel)
        iVendorId = identity[1].vendor_id.get()
        iDeviceType = identity[1].device_type.get()
        iProductCode =  identity[1].product_code.get()
        revisionComposite = identity[1].revision.get()
        bMajorRev = revisionComposite.get('major_revision')
        bMinorRev = revisionComposite.get('minor_revision')
        composite_key = (iVendorId, iDeviceType, iProductCode, bMajorRev, bMinorRev)

        return composite_key

    #   This forward open to class 1 - it's ping
    def null_forward_open_ping_zero_key(self):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive')

        triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        ot = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        to = ot

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=triger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=2,
            ot_connection_parameters=ot,
            to_connection_parameters=to,
            ot_rpi=0,
            to_rpi=0
        )

        forward_open_request.connection_path = [(0x34, (0, 0, 0, 0, 0)), (0x20, 0x01), (0x24, 0x01)]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response

    #   This forward open to class 1 - it's ping
    def null_forward_open_ping_key(self, composite_key = None):
        # ------------------------------------------------------------------------------
        print('Null forward open, to drive')

        if composite_key is None:
            composite_key = self._private_method_get_electronic_key()

        triger = ForwardOpenTransportClassTrigger(transport_class=1,
                                                  trigger=ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC,
                                                  direction=ForwardOpenTransportClassTrigger.DIRECTION_SERVER
                                                  )

        ot = ForwardOpenConnectionParameters(
            connection_size=14,
            fixed=True,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        )

        to = ot

        forward_open_request = ForwardOpenRequest(
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL,
            transport_class=1,
            connection_size=0,
            to_connection_id=0x002c48ae,
            connection_serial_number=0x0001,
            transport_class_trigger=triger,
            originator_serial_number=0x00e7d47f,
            connection_timeout_multiplier=2,
            ot_connection_parameters=ot,
            to_connection_parameters=to,
            ot_rpi=0,
            to_rpi=0
        )

        forward_open_request.connection_path = [(0x34, composite_key), (0x20, 0x01), (0x24, 0x01)]

        error_response = global_send_forward_open_request(self.channel, forward_open_request)

        return error_response


    def _private_method_find_device_ports(self):
        identity = Identity(channel=self.channel)
        arr = []
        for i in range(1, 15 + 1):
            name = identity[i].product_name.get()['string']
            if 'Not Present' not in name and 'Port' in name:
                arr.append(i - 1)
        return arr

    def test_ping_correct(self):
        print('===null_forward_open_ping==')

        for i in range(5):
            # Testing Null Forward Open Ping with key downloaded from device
            return_value = self.null_forward_open_ping_key()
            assert return_value == None

        for i in range(5):
            # Testing Null Forward Open Ping with zero array as key
            return_value = self.null_forward_open_ping_zero_key()
            assert return_value == None

    def test_ping_bad_key(self):
        #return
        print('===null_forward_open_ping_bad_key==')

        correct_key = self._private_method_get_electronic_key()

        for i in range(0,5):
            composite_key = list(correct_key)
            composite_key[i] = self._private_method_any_other_value(composite_key[i])

            print("correct key %s" %  str(correct_key))
            print("sending key %s" % str(composite_key))

            return_value = self.null_forward_open_ping_key(composite_key=tuple(composite_key))

            expected_value = 0
            if i == 0 or i == 2:
                expected_value = self._private_method_get_class_define('CIP_EXT_ERROR_VENDOR_PRODUCT_CODE_MISMATCH')
            elif i == 1:
                expected_value = self._private_method_get_class_define('CIP_EXT_ERROR_PRODUCT_TYPE_MISMATCH')
            elif i == 3 or i == 4:
                expected_value = self._private_method_get_class_define('CIP_EXT_ERROR_REVISION_MISMATCH')

            str_error_value_from_device = hex(list(return_value)[1])
            expected_value_error_value = hex(list(return_value)[1])

            print("Expected Error: %s " % str_error_value_from_device)
            print("Recieved Error: %s " % str_error_value_from_device)

            assert str_error_value_from_device == expected_value_error_value
