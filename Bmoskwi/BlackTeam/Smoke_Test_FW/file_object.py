from lvd.test.testers.nose.init import *
from ratools.cip.objects.common.file import File
from setup_channel import TestBase
import ratools.cip.utils as ciputils
from ratools.cip.messaging import cipmessage


@attr('edlt', 'file_object')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    '''
    Create a CIP connection with a target and send requested CIP message.

    Requested parameters:
    class_id    - Class ID
    instance    - Class instance
    attribute   - Class attribute
    service     - Service
    format_arr  - Array with datatypes of data in data_arr (default: empty)
    data_arr    - Array with data values to be send (default: empty)
    packed_data - format_arr data_arr already packed via ciputils.packData()

    Returns:
    CIP reply packet
    '''
    def sendCipMessage(self, service, class_id, instance, attribute=None, format_arr=[], data_arr=[], packed_data=None):
        if (packed_data == None):
            # Pack data to send
            packed_data = ciputils.packdata(data_arr, format_arr)

        # Send message
        try:
            reply = cipmessage(ciputils.fmtadr(
                svc=service, cls=class_id, ins=instance, atr=attribute) + packed_data)
        except ciputils.CipResponseError as cipRespErr:
            raise RuntimeError('CIP Error occured while sending CIP message: Class: {}, Inst: {}, Att: {} Srv: {}. Err Code: {}, Err Ext: {}'.format(
                class_id, instance, attribute, service, cipRespErr.generalStatus, cipRespErr.extendedStatus))

        return reply

    # Check if DeviceLogix feature is enabled on target, by checking a value of port 0 parameter 73 (Emb Logic Actual)
    def isDeviceLogixEnabled(self):
        print 'Checking if DeviceLogix is enabled:'

        reply = self.sendCipMessage(
            class_id = 0x93,    # CIP class DPI_PARAMETER 0x93U
            instance = 73,      # Parameter 73 - Emb Logic Actual
            attribute = 9,      # DPI attribute: Parameter Value
            service = 0x0E      # Get Attribute Single
        )

        unpacked = ciputils.unpackData(reply, ['UDINT'])
        parameterValue = unpacked[0]

        if parameterValue == 0:
            print 'Emb Logic Actual = ' + \
                repr(parameterValue) + ' (Emb Logic disabled)'
            return False
        elif parameterValue == 1:
            print 'Emb Logic Actual = ' + \
                repr(parameterValue) + ' (DeviceLogix enabled)'
            return True
        else:
            print 'Emb Logic Actual = ' + \
                repr(parameterValue) + ' (Other application selected)'
            return False

    def test_file_object(self):
        file = File(self.channel)

        assert file[0].atr.revision == 2

        if self.isDeviceLogixEnabled():
            # DLX is enabled
            assert file[0].atr.max_instance == 354
            assert file[0].atr.number_of_instances == 4
        else:
            # DLX is disabled
            assert file[0].atr.max_instance == 201
            assert file[0].atr.number_of_instances == 2

        assert file[200].atr.file_size.get() > 0
        assert file[200].atr.file_checksum.get() != 0

        assert file[201].atr.file_size.get() > 0
        assert file[201].atr.file_checksum.get() != 0

        pass