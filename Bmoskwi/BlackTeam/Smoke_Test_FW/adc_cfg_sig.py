#############################################################################
### @file
###
### Smoke test for verification of properly storing configuration signature
### in NVS when an ADC occurs
###
### @par Copyright (c) 2019 Rockwell Automation, Inc. All rights reserved.
#############################################################################

from lvd.test.testers.nose.init import *

from ratools.cip.objects.common.port import Port
from setup_channel import TestBase
import time
from ratools.cip.utils import CipResponseError
from ratools.cip.messaging import cipmessage
from ratools.cip.constants import Driver
import ratools.cip.drivers.ethernet_ip as ethernet_ip_driver
from ratools.cip.channel._channelcfg import ChannelCfg
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.classes.connection_manager import ForwardOpenTransportClassTrigger
import binascii
import ratools.cip.utils as utils
import ratools.cip.messaging as messaging
from time import sleep

from pprint import pprint


@attr('edlt', 'adc_cfg_sig')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):

        print("SAVE MACHINE CONFIGURATION SIGNATURE")

        self.original_configuration_signature = self.get_configuration_signature(0)
        print(self.array_to_string(self.original_configuration_signature))

        pass

    # teardown() after each test method"
    def teardown(self):
        print("RECOVER MACHINE CONFIGURATION SIGNATURE")
        self.set_configuration_signature(0,self.original_configuration_signature)
        temp = self.get_configuration_signature(0)
        print("RECOVERED SIGNATURE")
        print(self.array_to_string(temp))

        pass

    def array_to_string(self, conf_array):
        out = ''
        for a in conf_array:
            c = str(hex(a & 0xFF)).replace('0x', '').upper()
            if len(c) == 1:
                out = out + '0' + c
            else:
                out = out + c
        return out

    def get_configuration_signature(self, port):
        port_ov = port
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_get_single = 0x0E  # GET ATTR SINGLE

        reply = cipmessage(utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))
        unpacked = utils.unpackData(reply, ['SINT'] * 16)

        return unpacked

    def set_configuration_signature(self, port_ov, data_arr):
        attribute = 0x26  # Attribute 38
        class_id = 0x92  # USER MEMORY Object
        instance = (int(port_ov) * 0x1000)
        service_set_single = 0x10  # SET ATTR SINGLE

        # SET CONFIG_SIGNATURE
        format_arr = []
        for i in range(16):
            format_arr.append('SINT')

        formated_data = utils.packdata(data_arr, format_arr)
        reply = messaging.cipmessage(
            utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)
        return reply

    def set_parameter(self, param, value, type):
        attribute = 0x9  # Attribute 9
        class_id = 0x93  # USER MEMORY Object
        instance = param # 38 # Prchrg Delay
        service_set_single = 0x10  # SET ATTR SINGLE

        # type: 'UINT16', 'FLOAT', etc.
        format_arr = [type]
        data_arr = [value]
        formated_data = utils.packdata(data_arr, format_arr)
        reply = messaging.cipmessage(
            utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)
        return reply

    def get_parameter(self, param, type):
        attribute = 0x9  # Attribute 9
        class_id = 0x93  # USER MEMORY Object
        instance = param # 38 # Prchrg Delay
        service_get_single = 0x0E  # GET ATTR SINGLE

        reply = messaging.cipmessage(
            utils.fmtadr(svc=service_get_single, cls=class_id, ins=instance, atr=attribute))

        # type: 'UINT16', 'FLOAT', etc.
        unpacked = utils.unpackData(reply, [type])
        return unpacked[0]

    def test_ADC_configuration_signature(self):

        loop_count = 0
        error = False
        max_loop_count = 1

        while (error == False) and (loop_count < max_loop_count):
            # increment the loop count
            loop_count += 1

            print "\n- loop count = ", str(loop_count)
            print("\n- Configuration Signature before clearing")
            print(self.array_to_string(self.get_configuration_signature(0)))

            # write to a parameter to clear the configuration signature
            # the default value of parameter 38 is written to it
            # self.set_float_parameter(38, 0.5)
            self.set_parameter(38, 0.5, 'FLOAT')

            print("- Configuration Signature after setting parameter 38")
            print(self.array_to_string(self.get_configuration_signature(0)))

            print("- set the config sig to 0x55555555555555555555555555555555 and reset the drive")

            # write predefined data into the configuration signature
            self.set_configuration_signature(0,([0x55] * 16))

            # issue a reset command
            attribute = 0x3  # Attribute 3
            class_id = 0x97
            instance = 0
            service_set_single = 0x10  # SET ATTR SINGLE

            format_arr = ['SINT']
            data_arr = [3]
            formated_data = utils.packdata(data_arr, format_arr)
            reply = messaging.cipmessage(
                utils.fmtadr(svc=service_set_single, cls=class_id, ins=instance, atr=attribute) + formated_data)

            # wait for the drive to get back up running
            # Note: this wait time make need adjustments based
            # wait time after reset in seconds
            wait_time = 100
            print("- Wait %s Seconds for drive to reset ... \n" % wait_time)
            i = 0            
            while i < (wait_time / 10):
                i += 1
                sleep(10)
                print("."),
            print(" Drive running!\n")

            # get the configuration signature
            temp = self.get_configuration_signature(0)

            print("---- Configuration Signature after reset")
            print(self.array_to_string(self.get_configuration_signature(0)))

            # compare the read value with the written value at the beginning of the loop
            if temp != ([0x55] * 16):
                # register the error
                error = True

        print("\n*********************************************** ")

        if error == True:
            print "          Error on loop count = ", str(loop_count)
        else:
            print("*********** No Error!")

        print("*********************************************** \n")

