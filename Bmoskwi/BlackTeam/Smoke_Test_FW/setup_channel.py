import os
from lvd.test.testers.nose.init import *


class TestBase:
    def __init__(self):
        self.try_to_setup_channel()

    def try_to_setup_channel(self, conn = False):
        self.channel = setup_comm_cip(connected=conn)
        return
