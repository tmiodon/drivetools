from lvd.test.testers.nose.init import *
from ratools.cip.objects.common.port import Port
from setup_channel import TestBase


@attr('edlt', 'port_object')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass

    def test_port_object(self):
        port = Port(self.channel)

        assert int(port[0].atr.revision) == 2
        assert int(port[0].atr.max_instance) == 2
        j = int(port[0].atr.num_instances)

        for i in range(1,j+1):
            assert 'Port' in str(port[i].atr.port_name)

        pass