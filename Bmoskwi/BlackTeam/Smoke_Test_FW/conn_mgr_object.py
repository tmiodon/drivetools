from lvd.test.testers.nose.init import *
from ratools.cip.objects.common.identity import Identity
from ratools.cip.objects.common.port import Port
from ratools.cip.objects.common.message_router import MessageRouter
import ratools.cip.utils as ciputils
import time
from ratools.cip.objects.constants import ZzStatusCode
from setup_channel import TestBase
from eiptoolkit_pytest_config import *


from ratools.cip.classes.connection_manager import (
    ForwardOpenRequest,
    ForwardCloseRequest,
    ForwardOpenTransportClassTrigger,
    ForwardOpenConnectionParameters)
import ratools.cip.utils
import ratools.cip.sim.connUtils as conn_utils


def send_forward_open_request(channel, forward_open_request):
    '''
    Helper metod for sending forward open
    '''
    error_response = None

    try:
        forward_open_response = channel.send(str(forward_open_request.pack()))
    except ratools.cip.utils.CipResponseError as cipresponseerrorobj:
        error_response = [cipresponseerrorobj.generalStatus, cipresponseerrorobj.extendedStatus]

    return error_response

def send_forward_close_request(channel, forward_close_request):
    '''
    Helper metod for sending forward close
    '''
    error_response = None

    try:
        forward_close_response = channel.send(str(forward_close_request.pack()))
    except ratools.cip.utils.CipResponseError as cipresponseerrorobj:
        error_response = [cipresponseerrorobj.generalStatus, cipresponseerrorobj.extendedStatus]

    return error_response

@attr('edlt', 'conn_mgr_object')
class TestUM(TestBase):

    # setup() before each test method
    def setup(self):
        pass

    # teardown() after each test method"
    def teardown(self):
        pass

    # setup_class() before any methods in this class
    @classmethod
    def setup_class(cls):
        pass

    # teardown_class() after any methods in this class
    @classmethod
    def teardown_class(cls):
        pass


    def test_class3_connections(self):


        print "TEST setup"
        self.try_to_setup_channel(False)
        unconnected_channel = self.channel
        messageRouter = MessageRouter(unconnected_channel)
        identityObj = Identity(unconnected_channel)

        module_major_rev = identityObj[1].atr.revision[0]
        module_minor_rev = identityObj[1].atr.revision[1]
        vendor_id = identityObj[1].atr.vendor_id
        device_type = identityObj[1].atr.device_type
        product_code = identityObj[1].atr.product_code

        print '''
***  TEST 1 - check maximum number of class 3, class 1 connections
'''

        assert int(messageRouter[1].atr.number_available) == TOTAL_NUMBER_OF_CONNECTIONS


        print'''
**** TEST 2 - check if it is possible to open more class 3 connections than EDLT is configured
'''

        messageRouter = MessageRouter(self.channel)

        assert messageRouter[1].atr.number_active == 0

        connection_channels = []
        for i in range(MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS) :
            self.try_to_setup_channel(True)
            connection_channels.append(self.channel)

        assert messageRouter[1].atr.number_active == MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS

        try:
            self.try_to_setup_channel(True)
        except ciputils.CipResponseError as err:
            assert err.generalStatus == 0x1
            assert err.extendedStatus == 0x0113

        #close all connections
        for i in range(MAX_NUMBER_OF_CLASS_3_REGULAR_CONNECTIONS) :
            connection_channels[i].close()

        assert messageRouter[1].atr.number_active == 0

        print '''
**** TEST 3 - check if it is possible to open more class 3 large connections than EDLT is configured
'''
        connParameters= {}
        connParameters['ot_connection_size'] = LARGE_FORWARD_OPEN_MAX_CONNECTION_SIZE
        connParameters['to_connection_size'] = LARGE_FORWARD_OPEN_MAX_CONNECTION_SIZE

        for i in range(MAX_NUMBER_OF_CLASS_3_LARGE_CONNECTIONS + 1) :
            connParameters['to_connection_id'] = 0xabababab+i
            connParameters['connection_serial_number'] = i
            forward_open_request = ForwardOpenRequest(**connParameters)
            forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)] #path of the Message router

            error_response = send_forward_open_request(unconnected_channel, forward_open_request)

            if i < MAX_NUMBER_OF_CLASS_3_LARGE_CONNECTIONS:
                assert error_response is None
            else:
                #out of connections error
                assert error_response[0] == 1
                assert error_response[1] == 0x0113

        # clean up the connections
        for i in range(MAX_NUMBER_OF_CLASS_3_LARGE_CONNECTIONS) :
            connParameters['to_connection_id'] = 0xabababab+i
            connParameters['connection_serial_number'] = i
            forward_close_request = ForwardCloseRequest(**connParameters)
            forward_close_request.connection_path = [(0x20, 0x02), (0x24, 0x01)] #path of the Message router

            error_response = send_forward_close_request(unconnected_channel, forward_close_request)

            #Response should be successful
            assert error_response is None

        # there should be no active connections
        assert messageRouter[1].atr.number_active == 0

        print '''
**** TEST 4 - check large forward open that has too big connections size
'''

        connParameters= {}
        connParameters['ot_connection_size'] = LARGE_FORWARD_OPEN_MAX_CONNECTION_SIZE+5
        connParameters['to_connection_size'] = LARGE_FORWARD_OPEN_MAX_CONNECTION_SIZE+5
        connParameters['to_connection_id'] = 0xabababab

        forward_open_request = ForwardOpenRequest(**connParameters)

        #path of the Message router
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, INVALID O2T CONN SIZE
        assert error_response[0] == 1
       # assert error_response[1] == 0x0127

        # there should be no active connections
        assert messageRouter[1].atr.number_active == 0


        #@ to do B-206219  - check if request can go to the other object that Message Router,
        #                 if Message Router is the only object class 3 connection can go to
        #                 then add proper handling
        print '''
**** TEST 5 - check forward open to object different that message router
'''
        connParameters= {}
        connParameters['to_connection_id'] = 0xabababab

        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x01), (0x24, 0x01)] #path of the Identity Object

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_SEGMENT_TYPE_VALUE
        assert error_response[0] == 1
        assert  error_response[1] == 0x0315

        assert messageRouter[1].atr.number_active == 0

        print '''
**** TEST 6 -  This test check forbiden RPI.
'''
        '''Scenarios:
               1. check forward open with too low o2t RPI
               2. check forward open with too low t20 RPI
        '''
        connParameters= {}
        connParameters['to_connection_id'] = 0xabababab

        # Scenario 1
        # create out of range o2t RPI
        connParameters['ot_rpi'] = 4900

        forward_open_request = ForwardOpenRequest(**connParameters)
        #path of the Message router
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, RPI NOT SUPPORTED
        assert error_response[0] == 1
        assert error_response[1] == 0x0112

         # there should be no active connections
        assert messageRouter[1].atr.number_active == 0

        # Scenario 2
        # create out of range t20 RPI
        connParameters['to_rpi'] = 4900

        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)] #path of the Message router

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, RPI NOT SUPPORTED
        assert error_response[0] == 1
        assert error_response[1] == 0x0112

         # there should be no active connections
        assert messageRouter[1].atr.number_active == 0

        print'''
**** TEST 7 - check forward open with electronic key.
'''
        ''' Scenarios:
                 1. Force device type mismatch.
                 2. Force vendor id mismatch.
                 3. Force product code mismatch.
                 4. Force major rev mismatch.
                 5. Force minor rev mismatch.
                 6. Force device type 0 and and compatibility bit set
                 7. Force vendor id 0 and and compatibility bit set
                 8. Force product cpde 0 and and compatibility bit set
                 9. Force requested major rev > device major rev and and compatibility bit set
                 10. Force requested minor rev > device minor rev and and compatibility bit set
                 11. Send correct electronic key

        '''

        connParameters= {}
        connParameters['to_connection_id'] = 0xabababab

        forward_open_request = ForwardOpenRequest(**connParameters)

        # Scenario 1
        # Set path to Message Router. Key segment with wrong device type
        forward_open_request.connection_path = [(0x34, (vendor_id,device_type-1, product_code, module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0115

        # Scenario 2
        # Set path to Message Router. Key segment with wrong vendor id 2
        forward_open_request.connection_path = [(0x34, (vendor_id+1,device_type, product_code, module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0114

        # Scenario 3
        # Set path to Message Router. Key segment with wrong product code 10129
        forward_open_request.connection_path = [(0x34, (vendor_id,device_type, product_code+1, module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0114


        # Scenario 4
        # Set path to Message Router. Key segment with wrong major revision
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, product_code, module_major_rev+1, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0116
        assert messageRouter[1].atr.number_active == 0

        # Scenario 5
        # Set path to Message Router. Key segment with wrong minor revision
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, product_code, module_major_rev, module_minor_rev+1)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0116
        assert messageRouter[1].atr.number_active == 0


        # Scenario 6
        # Set path to Message Router. Key segment with wrong device type 0 and compatibility bit set
        forward_open_request.connection_path = [(0x34, (vendor_id, 0, product_code, 0x80+module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0115
        assert messageRouter[1].atr.number_active == 0

        # Scenario 7
        # Set path to Message Router. Key segment with vendor id 0 and compatibility bit set
        forward_open_request.connection_path = [(0x34, (0, device_type, product_code, 0x80+module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0114
        assert messageRouter[1].atr.number_active == 0

        # Scenario 8
        # Set path to Message Router. Key segment with product type 0 and compatibility bit set
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, 0, 0x80+module_major_rev, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0114
        assert messageRouter[1].atr.number_active == 0

        # Scenario 9
        # Set path to Message Router. Key segment with requested major rev > device major rev and and compatibility bit set
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, product_code,0x80+module_major_rev+1, module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0116
        assert messageRouter[1].atr.number_active == 0

        # Scenario 10
        # Set path to Message Router. Key segment with requested minor rev > device minor rev and and compatibility bit set
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, product_code, 0x80+module_major_rev, module_minor_rev+1)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        assert error_response[0] == 1
        assert error_response[1] == 0x0116
        assert messageRouter[1].atr.number_active == 0

        # Scenario 10
        # Set path to Message Router. Key segment is correct. Connection should be opened.
        forward_open_request.connection_path = [(0x34, (vendor_id, device_type, product_code,module_major_rev,module_minor_rev)), (0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # Response should be successful
        assert error_response is None
        # One connection should be active
        assert messageRouter[1].atr.number_active == 1

        forward_close_request = ForwardCloseRequest(**connParameters)
        forward_close_request.connection_path = [(0x20, 0x02), (0x24, 0x01)] #path of the Message router


        error_response = send_forward_close_request(unconnected_channel, forward_close_request)

        #Response should be successful
        assert error_response is None

        # No connections should be active
        assert messageRouter[1].atr.number_active == 0

        print '''
**** TEST 8 - Open class 3 connection. Send request to CIP object in this connection
'''
        # Create connected channel
        self.try_to_setup_channel(True)

        # 1 connections should be active
        assert messageRouter[1].atr.number_active == 1

        revision = Identity(self.channel)[1].atr.revision

        assert revision[0] == module_major_rev

        self.channel.close()

        # No connections should be active
        assert messageRouter[1].atr.number_active == 0

        print '''
**** TEST 9 - Class 1 fwd open with too big multiplier
'''
        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['connection_timeout_multiplier'] = 8
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, TIMEOUT MULTIPLIER NOT SUPPORTED
        assert error_response[0] == 1
        assert error_response[1] == 0x0133

        print '''
**** TEST 10 - Class 1 fwd open - check transport type
'''
        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['to_connection_id'] = 0xabababac
        connParameters['trigger'] = ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_SERVER
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_CLASS_TRIGGER_INVALID
        assert error_response[0] == 1
        assert error_response[1] == 0x0103

        print '''
**** TEST 11 - Class 1 null fwd open - check transport type
'''
        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['trigger'] = ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        connParameters['to_connection_type'] = ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        connParameters['ot_connection_type'] = ForwardOpenConnectionParameters.CONNECTION_TYPE_NULL
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x01), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_CLASS_TRIGGER_INVALID
        assert error_response[0] == 1
        assert error_response[1] == 0x0103

        print '''
**** TEST 12 - Class 1 fwd open - check OT redundant owner flag
'''
        ot_parameters = ForwardOpenConnectionParameters( connection_type = ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
                                                         redundant_owner = True)
        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['trigger'] = ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        connParameters['ot_connection_parameters'] = ot_parameters

        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_REDUNDANT_OWNER
        assert error_response[0] == 1
        assert error_response[1] == 0x0125

        print '''
**** TEST 13 - Class 1 fwd open - check OT priority
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['ot_priority'] = ForwardOpenConnectionParameters.PRIORITY_LOW
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_PRIORITY
        assert error_response[0] == 1
        assert error_response[1] == 0x0121

        print '''
**** TEST 14 - Class 1 fwd open - check OT size type
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['ot_fixed'] = False
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x011f

        print '''
**** TEST 15- Class 1 fwd open - check OT connection type
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['ot_connection_type'] = ForwardOpenConnectionParameters.CONNECTION_TYPE_MULTICAST
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_TYPE
        assert error_response[0] == 1
        assert error_response[1] == 0x0123

        print '''
**** TEST 16 - Class 1 fwd open - check TO priority
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['to_priority'] = ForwardOpenConnectionParameters.PRIORITY_LOW
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_PRIORITY
        assert error_response[0] == 1
        assert error_response[1] == 0x0122

        print '''
**** TEST 17 - Class 1 fwd open - check TO size type
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['to_fixed'] = False
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x0120

        print '''
**** TEST 18 - Class 1 fwd open - check OT rpi
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['ot_rpi'] = 1000
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x0112

        print '''
**** TEST 19 - Class 1 fwd open - check TO rpi
'''

        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['to_rpi'] = 1000
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x0112

        print '''
**** TEST 20 - Class 3 fwd  - open with too big multiplier
'''
        connParameters= {}
        connParameters['connection_timeout_multiplier'] = 8
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, TIMEOUT MULTIPLIER NOT SUPPORTED
        assert error_response[0] == 1
        assert error_response[1] == 0x0133

        print '''
**** TEST 21 - Class 3 fwd - check transport direction
'''
        connParameters= {}
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_DIRECTION_NOT_SUPPORTED
        assert error_response[0] == 1
        assert error_response[1] == 0x011e

        print '''
**** TEST 22 - Class 3 fwd open - check OT redundant owner flag
'''
        ot_parameters = ForwardOpenConnectionParameters( connection_type = ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
                                                         redundant_owner = True)
        connParameters= {}
        connParameters['ot_connection_parameters'] = ot_parameters
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_REDUNDANT_OWNER
        assert error_response[0] == 1
        assert error_response[1] == 0x0125

        print '''
**** TEST 23 - Class 3 fwd open - check OT connection type
'''

        connParameters= {}

        connParameters['ot_connection_type'] = ForwardOpenConnectionParameters.CONNECTION_TYPE_MULTICAST
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_TYPE
        assert error_response[0] == 1
        assert error_response[1] == 0x0123

        print '''
**** TEST 24 - Class 3 fwd open - check OT priority
'''

        connParameters= {}
        connParameters['ot_priority'] = ForwardOpenConnectionParameters.PRIORITY_SCHEDULED
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_PRIORITY
        assert error_response[0] == 1
        assert error_response[1] == 0x0121


        print '''
**** TEST 25 - Class 3 fwd open - check OT size type
'''

        connParameters= {}
        ot_parameters = ForwardOpenConnectionParameters( connection_type = ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
                                                         fixed = True)
        connParameters['ot_connection_parameters'] = ot_parameters
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_O2T_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x011f

        print '''
**** TEST 26 - Class 3 fwd open - check TO connection type
'''

        connParameters= {}
        connParameters['to_connection_type'] = ForwardOpenConnectionParameters.CONNECTION_TYPE_MULTICAST
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_TYPE
        assert error_response[0] == 1
        assert error_response[1] == 0x0124

        print '''
**** TEST 27 - Class 3 fwd open - check TO priority
'''

        connParameters= {}
        connParameters['to_priority'] = ForwardOpenConnectionParameters.PRIORITY_SCHEDULED

        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path =  [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_PRIORITY
        assert error_response[0] == 1
        assert error_response[1] == 0x0122

        print '''
**** TEST 28 - Class 3 fwd open - check TO size type
'''

        connParameters= {}
        to_parameters = ForwardOpenConnectionParameters( connection_type = ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
                                                         fixed = True)
        connParameters['to_connection_parameters'] = to_parameters
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request = ForwardOpenRequest(**connParameters)
        forward_open_request.connection_path = [(0x20, 0x02), (0x24, 0x01)]

        error_response = send_forward_open_request(unconnected_channel, forward_open_request)

        # error response should be returned, CIP_EXT_STATUS_INVALID_T2O_CONN_FIXVAR
        assert error_response[0] == 1
        assert error_response[1] == 0x0120

        print '''
**** TEST 30 - Class 1 connection - accept only one
'''
        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['trigger'] = ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        connParameters['to_connection_id'] = 0xabababab
        connParameters['ot_connection_size'] = 14
        connParameters['to_connection_size'] = 14
        forward_open_request1 = ForwardOpenRequest(**connParameters)
        forward_open_request1.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]


        connParameters= {}
        connParameters['transport_class'] = 1
        connParameters['to_connection_id'] = 0xabababac
        connParameters['trigger'] = ForwardOpenTransportClassTrigger.TRIGGER_CYCLIC
        connParameters['direction'] = ForwardOpenTransportClassTrigger.DIRECTION_CLIENT
        forward_open_request2 = ForwardOpenRequest(**connParameters)
        forward_open_request2.connection_path = [ (0x20, 0x04), (0x24, 0x06), (0x2c, 0x02), (0x2c, 0x01)]

        send_forward_open_request(unconnected_channel, forward_open_request1)
        time.sleep(1)
        error_response = send_forward_open_request(unconnected_channel, forward_open_request2)

        # error response should be returned, CIP_EXT_STATUS_CONNECTION_LIMIT_REACHED
        assert error_response[0] == 1
        assert error_response[1] == 0x0113

        forward_close_request = ForwardCloseRequest(**connParameters)
        forward_close_request.connection_path = [(0x20, 0x04), (0x24, 0x06), (0x2c, 0x02),
                                                 (0x2c, 0x01)]  # path of the Message router

        error_response = send_forward_close_request(unconnected_channel, forward_close_request)