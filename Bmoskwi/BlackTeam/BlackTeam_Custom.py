# -------------------------------------------------------------------
#
# FILE NAME:    driveClass.py
# VERSION:      0.1
# CONTRIBUTORS: Tomasz Miodonski
#               Bartosz Moskwik
# START DATE:   01/02/2019
#
# DESCRIPTION:  driveClass library contains definition and implementation
#               of HPC drive family with
#
# -------------------------------------------------------------------
import struct
import ratools.cip.channel as channel
import ratools.cip.messaging as messaging
import time
from datetime import datetime
from threading import Thread
import atexit
import os

from ParameterDatabase.parametersByNameAFE import parametersByNameAFE
from ParameterDatabase.parametersByNumberAFE import parametersByNumberAFE

class driveBase():

    close_conn = False
    READ = 0x68  # type:
    WRITE = 0x67
    TNSW = 0x00
    TNSW2 = 0x00
    c = [0x4b, 0x02, 0x20, 0x67, 0x24, 0x01]  # Header CIP
    head_cip = "".join(chr(i) for i in c)
    head_pccc = ""
    ipAddress = ""

    def __init__(self, ipAddress='192.168.1.101', connection=False):
        self.ipAddress = ipAddress
        try:
            self.close_conn = False
            self.ch = channel.create(keys='PF755T', path=r'AB_ETHIP-1\\' + ipAddress, connected=connection, driver='enet')
        except NameError:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Cannot connect to drive at " + ipAddress + " Test will be closed!"
            time.sleep(1)
            exit(123)
        try:
            self.t1 = Thread(target=self.PCCCLoop)
            self.t1.setDaemon(True)
            self.t1.start()
            atexit.register(self.exit_handler)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Drive connected at: " + ipAddress + " !"
        except Exception:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not start new thread for PCCC communication. Test will be closed!"
            exit(123)
        #Sleep 1 second added for not inerupting PCCC loop initialization
        #time.sleep(0.1)
        return

    def exit_handler(self):
        # at closing we need to set timeout to 0x00 0x00, only this way there will not be idle Net error
        if not self.close_conn:
            #self.ch.is_closed() #Better to use this one
            self.BuildPCCCHeader(self.WRITE)  # Head build
            self.close_conn = True
            b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, 0x00, 0x00]  # body
            body = "".join(chr(i) for i in b)
            messaging.cipmessage(self.head_cip + self.head_pccc + body)
            time.sleep(0.1)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Connection to drive is closing!"

    def WaitToBeOnline(self):
        try:
            response = os.system("ping -n 1 -w 100 " + self.ipAddress + " > nul")
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Resetting drive.""Wait drive to be online. (Ping)"
            while response != 0:
                time.sleep(1)
                response = os.system("ping -n 1 -w 100 " + self.ipAddress+ " > nul")
        except ValueError:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") IP addres not set, initialize drive first."
            exit(124)



    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: connects to drive
    def Connect(self, ipAddress = "192.168.1.101"):
        try:
            self.close_conn = False
            self.ch = channel.create(keys='PF755T', path=r'AB_ETHIP-1\\' + ipAddress, connected=False, driver='enet')

        except NameError:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Cannot connect to drive at " + ipAddress + " Test will be closed!"
            time.sleep(1)
            exit(123)
        try:
            t1 = Thread(target=self.PCCCLoop)
            t1.setDaemon(True)
            t1.start()
            atexit.register(self.exit_handler)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Drive connected at: " + ipAddress + " !"
        except Exception:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not start new thread for PCCC communication. Test will be closed!"
            exit(123)
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: disconnect from drive
    def Disconnect(self):
        print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Connection to drive is closing!"
        self.close_conn = True
        b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, 0x00, 0x00]  # body
        body = "".join(chr(i) for i in b)
        messaging.cipmessage(self.head_cip + self.head_pccc + body)
        return

    ###############################################################
    def CheckIfPortCorrect(self, port):
        if port < 0 or port > 14:
            raise ValueError("Port number out of range !")
        elif isinstance(port, int) != True:
            raise ValueError("Port number not integer type !")

    ###############################################################
    def DetermineParameterInformation(self, parameter, port):
        parameterCoordinates = []
        if isinstance(parameter, basestring) == True:
            parameterCoordinates.append(parametersByNameAFE[parameter]['port'])
            parameterCoordinates.append(parametersByNameAFE[parameter]['number'])
            parameterCoordinates.append(parametersByNameAFE[parameter]['type'])
        if isinstance(port, int) == True:
            parameterCoordinates.append(port)
            parameterCoordinates.append(parameter)
            parameterCoordinates.append(parametersByNumberAFE[port][parameter])
        return parameterCoordinates

    ###############################################################
    # It does function, but what it returns is drive family, like
    # 755TR or 755TM, without differentiating if it's bus supply
    # or inverter. I'm not sure how to recognize PanelMount.
    def GetDriveType(self):
        address = [[0x92, 0]]
        attribute = [1]
        format = {1: 'BDATA'}
        try:
            response = messaging.getatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Drive type is: ", response[1]
            return response[1].replace('PowerFlex ', '').replace(' ', '')
        except:
            return "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can't determine drive type!"

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: gets amount of option cards connected to
    # drive.
    def GetNumberOfPeripherals(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: determines drive firmware major, minor
    # and build version.
    def GetDriveFirmwareVersion(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: gets port firmware version
    def GetPortFirmwareVersion(self):
        return

    ###############################################################
    # Partially implemented. What is missing is closing connection
    # in a way that will not produce later on errors in script.
    def GetPortType(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096*port
        address = [[0x92, _port]]
        attribute = [1]
        format = {1: 'BDATA'}
        try:
            response = messaging.getatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Port name recognized."
            return response[1]
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can't determine port type!"

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: reset drive
    def ResetDrive(self):
        address = [[0x97, 0]]
        attribute = {3: 3}
        format = {3: 'USINT'}
        try:
            self.exit_handler()
            time.sleep(1)
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Resetting drive."
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can't reset drive!"

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: reset port
    def ResetPort(self, port):
        return

    ###############################################################
    def ClearFault(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x97, _port]]
        attribute = {3: 1}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing fault on port: " + str(port)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing fault on port " + str(port) + " failed!"

    ###############################################################
    def ClearFaultQueue(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x97, _port]]
        attribute = {3: 2}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing fault queue on port: " + str(port)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing fault queue on port " + str(port) + " failed!"

    ###############################################################
    def ClearAlarm(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x98, _port]]
        attribute = {3: 1}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address,attribute,format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing alarm on port: " + str(port)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing alarm on port " + str(port) + " failed!"

    ###############################################################
    def ClearAlarmQueue(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x98, _port]]
        attribute = {3: 2}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address,attribute,format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing alarm queue on port: " + str(port)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clearing alarm queue on port " + str(port) + " failed!"

    ###############################################################
    def ReadParameter(self, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)

        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Read Port: " + str(
                parameterCoordinates[0]) + ", parameter: " + parameter + ",value is: " + str(parameterValue[9])
            return parameterValue[9]
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + parameter + " on port " + str(parameterCoordinates[0]) + "!"

    ###############################################################
    def ReadParameterBit(self, bit, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            result = parameterValue[9] & 2**bit != 0
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Read Port: " + str(
                parameterCoordinates[0]) + ", parameter: " + parameter + ",value is: " + str(result)
            return result
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + parameter + " on port " + str(parameterCoordinates[0]) + "!"

    ###############################################################
    def WriteParameter(self, value=None, parameter=None, port=None):
        if isinstance(value, basestring):
            _value = parametersByNameAFE[port]['enums'][parameter]
        else:
            _value = value

        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Write Port: " + str(parameterCoordinates[0]) + ", parameter: " + parameter + ", with value: " + str(_value)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with writing parameter " + str(parameter) + " on port " + parameter + "!"

    ###############################################################
    def SetParameterBit(self, bit, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]

        currentValue = self.ReadParameter(parameterCoordinates[0], parameterCoordinates[1])
        mask = 1 << bit
        _value = currentValue | mask

        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Set bit " + str(bit) + " on parameter " + str(parameterCoordinates[0]) + ":" + parameter
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + parameter + " !"

    ###############################################################
    def ClearParameterBit(self, bit, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]

        currentValue = self.ReadParameter(parameterCoordinates[0], parameterCoordinates[1])
        mask = ~(1 << bit)
        _value = currentValue & mask

        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clear bit " + str(bit) + " on parameter " + str(parameterCoordinates[0]) + ":" + parameter
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + parameter + " !"

    ###############################################################
    def VerifyParameterValue(self, value, parameter=None, port=None, tolerance = 0):
        if isinstance(value, basestring):
            _value = parametersByNameAFE[port]['enums'][parameter]
        else:
            _value = value

        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            if parameterValue[9] > (_value - tolerance) and parameterValue[9] < (_value + tolerance):
                print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + parameter + ",value is correct"
            else:
                print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + parameter + ", value is incorrect"
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with verifying parameter " + parameter + " !"

    ###############################################################
    def VerifyParameterBitValue(self, value, bit, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            result = parameterValue[9] & 2**bit != 0
            if result == value:
                print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + parameter + ",value is correct."
            else:
                print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + parameter + ",value is incorrect."
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + parameter + " !"

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: waits for parameter to have steady value
    def WaitTillParameterHasSteadyValue(self, parameter=None, port=None, timeout = 60000):
        startTime = time.time()

        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}

        previousParameterValue = 0
        while (time.time() - startTime < timeout):
            try:
                currentParameterValue = messaging.getatrsingle(address, attribute, format)
                if (currentParameterValue == previousParameterValue):
                    print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Parameter " + parameter + " is steady on value: " + str(currentParameterValue[9])
                    return currentParameterValue[9]
                else:
                    previousParameterValue = currentParameterValue
                    time.sleep(0.5)
            except:
                print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[
                                       :-3] + ") Can not read parameter " + parameter + " !"


    ###############################################################
    #
    # This section contains drive related commands like start or
    # stop drive based on CIP messaging and PCCC protocol.
    #
    ###############################################################


    ###############################################################
    def BuildPCCCHeader(self, act):
        self.TNSW = self.TNSW + 1
        a = [0x07, 0x4d, 0x00, 0x44, 0x4e, 0x97, 0x3c, 0x0f, 0x00, self.TNSW, self.TNSW2, act,
             0x00, 0x00, 0x02, 0x00]  # header PCC
        self.head_pccc = "".join(chr(i) for i in a)
        self.TNSWCalc()

    ###############################################################
    def TNSWCalc(self):
        if self.TNSW == 0xff:
            self.TNSW = 0x00#
            self.TNSW2 = self.TNSW2 + 1
        if self.TNSW2 == 0xff:#
            self.TNSW2 = 0x00

    ###############################################################
    def PCCCLoop (self, pccctimeout=10, msgsepparation=0.1):
        TIMEOUT = pccctimeout #Timeout can be adjusted if needed
        ################################## Setting Timeout ########################################
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, TIMEOUT, 0x00]  # body
        body = "".join(chr(i) for i in b)
        try:
            messaging.cipmessage(self.head_cip + self.head_pccc + body)  # Send msg
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"
        ################################## Setting Ref source ########################################
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x99, 0x09, 0x05, 0x42, 0x10, 0x10, 0x00, 0x00]  # body
        body = "".join(chr(i) for i in b)
        try:
            messaging.cipmessage(self.head_cip + self.head_pccc + body)  # Send msg
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"
        while True:
            # for x in xrange(15): # each 15 PCCC DPI message, sand one Status word read request
            #     if self.close_conn:
            #         break
            #     ################################### PCCC DPI wrapping message ########################################
            #     self.TNSW = self.TNSW + 1
            #     a = [0x07, 0x4d, 0x00, 0x44, 0x4e, 0x97, 0x3c, 0x0f, 0x00, self.TNSW, self.TNSW2, 0x95]  # header PCCC
            #     b = [0x0b, 0x01, 0x00, 0xa1, 0x0a, 0x62, 0x03, 0x00, self.TNSW, self.TNSW2, 0x14, 0x00]  # body
            #     head_pccc = "".join(chr(i) for i in a)
            #     body = "".join(chr(i) for i in b)
            #     self.TNSWCalc()
            #     try:
            #         messaging.cipmessage(self.head_cip + head_pccc + body)  # Send msg
            #     except:
            #         print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"
            #     #time.sleep(msgsepparation) # PCC messages separation, can be adjusted or passed as param.

            ################################### PCCC status word read  ########################################
            if self.close_conn:
                break
            self.StatusWord = self.ReadStatusWord()
            self.ReadParamWithoutComment(1, 10)
            #time.sleep(msgsepparation) # PCC messages separation, can be adjusted or passed as param.

    ###############################################################
    def WriteCommandWord(self, com1, com2): # checked
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x99, 0x09, 0x05, 0x42, com1, com2, 0x00, 0x00] # body
        body = "".join(chr(i) for i in b)
        try:
            messaging.cipmessage(self.head_cip + self.head_pccc + body)  # Send msg
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"

    ###############################################################
    def ReadStatusWord(self):
        self.BuildPCCCHeader(self.READ)# Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x02, 0x00]  # body build
        body = "".join(chr(i) for i in b)
        try:
            receive = messaging.cipmessage(self.head_cip + self.head_pccc + body)  # Send msg
            return bytearray(receive[15:19])  # return msg response
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"

    ###############################################################
    def WriteRefference(self, refference):
        ref = bytearray(struct.pack("f", refference))# Conversion from float to structured bytearray
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x32, 0x00, 0x99, 0x09, 0x05, 0x42]  # body build
        body = "".join(chr(i) for i in b)
        refer = "".join(chr(i) for i in ref)
        try:
            messaging.cipmessage(self.head_cip + self.head_pccc + body + refer)  # Send msg
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Write refference value: " + str(refference)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"

    ###############################################################
    def ReadFeedback(self):
        self.BuildPCCCHeader(self.READ)
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x32, 0x00, 0x02, 0x00]  # body
        body = "".join(chr(i) for i in b)
        try:
            receive = messaging.cipmessage(self.head_cip + self.head_pccc + body)  # Send msg
            flo = struct.unpack('f', bytearray(receive[15:19]))
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Feedback value is: " + str(round(flo[0], 2))
            return round(flo[0], 2)
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Problem with sending PCCC message !"

    #########################################STATUS FIRST BYTE############################
    def isReady(self):
        status = self.ReadStatusWord()
        return status[0]& 0x01 == 0x01

    def isActive(self):
        status = self.ReadStatusWord()
        return status[0] & 0x02 == 0x02

    def isCommandedForward(self):
        status = self.ReadStatusWord()
        return status[0]& 0x04 == 0x04

    def isActualForward(self):
        status = self.ReadStatusWord()
        return status[0] & 0x08 == 0x08

    def isAccelerating(self):
        status = self.ReadStatusWord()
        return status[0] & 0x10 == 0x10

    def isDecelerating(self):
        status = self.ReadStatusWord()
        return status[0] & 0x20 == 0x20

    def isAlarm(self):
        status = self.ReadStatusWord()
        return status[0] & 0x40 == 0x40

    def isFault(self):
        status = self.ReadStatusWord()
        return status[0] & 0x80 == 0x80

    #########################################STATUS SECOND BYTE############################
    def isAtSpeed(self):
        status = self.ReadStatusWord()
        return status[1] & 0x01 == 0x01

    def isManual(self):
        status = self.ReadStatusWord()
        return status[1] & 0x02 == 0x02

    def SpdRefID0(self):
        status = self.ReadStatusWord()
        return status[1]& 0x04 == 0x04

    def SpdRefID1(self):
        status = self.ReadStatusWord()
        return status[1] & 0x08 == 0x08

    def SpdRefID2(self):
        status = self.ReadStatusWord()
        return status[1] & 0x10 == 0x10

    def SpdRefID3(self):
        status = self.ReadStatusWord()
        return status[1] & 0x20 == 0x20

    def SpdRefID4(self):
        status = self.ReadStatusWord()
        return status[1] & 0x40 == 0x40

    #########################################STATUS THIRD BYTE############################
    def isRunning(self):
        status = self.ReadStatusWord()
        return status[2] & 0x01 == 0x01

    def isJogging(self):
        status = self.ReadStatusWord()
        return status[2] & 0x02 == 0x02

    def isStopping(self):
        status = self.ReadStatusWord()
        return status[2] & 0x04 == 0x04

    def isDCBrake(self):
        status = self.ReadStatusWord()
        return status[2] & 0x08 == 0x08

    def isDynamicBrakeActive(self):
        status = self.ReadStatusWord()
        return status[2] & 0x10 == 0x10

    def isSpeedMode(self):
        status = self.ReadStatusWord()
        return status[2] & 0x20 == 0x20

    def isPositionMode(self):
        status = self.ReadStatusWord()
        return status[2] & 0x40 == 0x40

    def isTorqueMode(self):
        status = self.ReadStatusWord()
        return status[2] & 0x80 == 0x80

    #########################################STATUS FOURTH BYTE############################
    def isAtZeroSpeed(self):
        status = self.ReadStatusWord()
        return status[3] & 0x01 == 0x01

    def isAtHome(self):
        status = self.ReadStatusWord()
        return status[3] & 0x02 == 0x02

    def isAtLimit(self):
        status = self.ReadStatusWord()
        return status[3] & 0x04 == 0x04

    def isAtCurrentLimit(self):
        status = self.ReadStatusWord()
        return status[3] & 0x08 == 0x08

    def isBusFreqReg(self):
        status = self.ReadStatusWord()
        return status[3] & 0x10 == 0x10

    def isEnableOn(self):
        status = self.ReadStatusWord()
        return status[3] & 0x20 == 0x20

    def isMotorOverload(self):
        status = self.ReadStatusWord()
        return status[3] & 0x40 == 0x40

    def isRegen(self):
        status = self.ReadStatusWord()
        return status[3] & 0x80 == 0x80

    ################################### COMMANDS ###################################################################
    def Start(self):
        try:
            self.WriteCommandWord(0x12,0x00)
            self.WriteCommandWord(0x00,0x00)
            print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Start drive"
            return True
        except:
            return False

    def Stop(self):
        self.WriteCommandWord(0x01,0x00)
        self.WriteCommandWord(0x00,0x00)
        print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Stop drive"

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues run command.
    def RunBit(self,set=True):
        CommandParser = self.ReadParamWithoutComment("P0 MS Logic Rslt")
        CommandByte2 = CommandParser>>8
        CommandByte1 = CommandParser&0x00ff
        if set:
            self.WriteCommandWord(CommandByte1, CommandByte2|0x40)
        else:
            self.WriteCommandWord(CommandByte1, CommandByte2&(0xff-0x40))
        return
    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues coast to stop command.
    def CoastStop(self):
        self.WriteCommandWord(0x00, 0x00)
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues current limit stop command.
    def CurrentLimitStop(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues jog 1 command.
    def Jog1(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues jog 2 command.
    def Jog2(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues drive to use AccelTime1 value.
    def UseAccelTime1(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues drive to use AccelTime2 value.
    def UseAccelTime2(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues drive to use DecelTime1 value.
    def UseDecelTime1(self):
        return

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: issues drive to use DecelTime2 value.
    def UseDecelTime2(self):
        return

    def ClearFaults(self):
        self.WriteCommandWord(0x18,0x10)
        print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Clear drive faults (CommandWord)"

    ######################################################################################################

    def WaitToBeAtRefference(self, timeoutS = 0.1):
        print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Wait drive to be at refference!"
        while not self.isAtSpeed():
            time.sleep(timeoutS)
        return

    def WaitToBeAtZeroSpeed(self, timeoutS = 0.1):
        print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Wait drive to be at zero speed!"
        while not self.isAtZeroSpeed():
            time.sleep(timeoutS)
        return

    def ReadParamWithoutComment(self, parameter=None, port=None):
        parameterCoordinates = self.DetermineParameterInformation(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            return parameterValue[9]
        except:
            print "[ERRO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Can not read parameter " + str(parameter) + " on port " + str(parameterCoordinates[0]) + "!"