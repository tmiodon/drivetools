import numpy as np
import time
import math
import random
from matplotlib import pyplot as plt
import cv2
import glob

WIDTH = 1920
HIGHT = 1080

CHECK_LONG = HIGHT / 2
POINT_OFFSET = 100

RADIUS = 0
X_CENTER = 0
Y_CENTER = 0
list = []
Hits = []
ScorePoints = [0,0,0,0,0,0,0,0,0,0]
z = False
def CameraCorrection():
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    i = 0
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((6 * 8, 3), np.float32)
    objp[:, :2] = np.mgrid[0:8, 0:6].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    img = cv2.imread('7camera.jpg')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, (8, 6), None)
    objpoints.append(objp)
    corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
    imgpoints.append(corners2)
    # Draw and display the corners
    # img = cv2.drawChessboardCorners(img, (8,6), corners2,ret)
    # cv2.imshow('img',img)
    # cv2.waitKey(500)
    # cv2.imwrite(fname + 'img.jpg', img)
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    # img = cv2.imread(fname)
    h, w = img.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)  # crop the image
    x, y, w, h = roi
    dst = dst[y:y + h, x:x + w]
    # cv2.imwrite(fname + 'calibresult.jpg', dst)

def PointCatcher(Xaxis=1,Yaxis=1):
    global CHECK_LONG
    global POINT_OFFSET

    offset = 0
    for x in xrange(CHECK_LONG):
        pixel = threshold2[(HIGHT / 2) + int(x*Xaxis), (WIDTH / 2) + int(x*Yaxis)]
        if pixel != 255:
            cv2.circle(frame, ((WIDTH / 2) + int(x*Yaxis), (HIGHT / 2) + int(x*Xaxis)), 0, (0, 0, 255), 1)
            if offset <10:
                offset=0
        else:
            offset += 1
        if offset >= POINT_OFFSET:
            cv2.circle(frame, ((WIDTH / 2) + int((x - POINT_OFFSET)*Yaxis), (HIGHT / 2) + int((x - POINT_OFFSET)*Xaxis)), 2, (0, 255, 0), -1)
            return [(WIDTH / 2) + int((x - POINT_OFFSET)*Yaxis)+Yaxis, (HIGHT / 2) + int((x - POINT_OFFSET)*Xaxis)+Xaxis]

def Center_calculation(Point):
      # for i in range(3):
    #    r = random.randint(0, 3)
    #     if r not in list: list.append(r)
    if Point[0] and Point[1] and Point[2] and Point[3] is not None:
        P1x = Point[0][0]
        P1y = Point[0][1]
        P2x = Point[1][0]
        P2y = Point[1][1]
        P3x = Point[2][0]
        P3y = Point[2][1]
        P4x = Point[3][0]
        P4y = Point[3][1]
        Center_Points = [0,0,0]
        if (P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x) <> 0:
            X = (((P2x ** 2 * P3y + P2y ** 2 * P3y - P1x ** 2 * P3y - P1y ** 2 * P3y) / (
                    P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x)) + (
                                (P1x ** 2 * P2y + P1y ** 2 * P2y - P3x ** 2 * P2y - P3y ** 2 * P2y) / (
                                P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x)) + (
                                (P3x ** 2 * P1y + P3y ** 2 * P1y - P2x ** 2 * P1y - P2y ** 2 * P1y) / (
                                P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x))) / 2
            Y = (((P2x ** 2 * P1x + P2y ** 2 * P1x - P3x ** 2 * P1x - P3y ** 2 * P1x) / (
                    P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x)) + (
                                (P1x ** 2 * P3x + P1y ** 2 * P3x - P2x ** 2 * P3x - P2y ** 2 * P3x) / (
                                P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x)) + (
                                (P3x ** 2 * P2x + P3y ** 2 * P2x - P1x ** 2 * P2x - P1y ** 2 * P2x) / (
                                P1y * P3x - P1y * P2x + P2y * P1x - P2y * P3x + P3y * P2x - P3y * P1x))) / 2
            R = int(round(math.sqrt((P4x - X) ** 2 + (P4y - Y) ** 2)))
            # cv2.circle(frame, (int(X_center), int(Y_center)), Radius, (255, 0, 0), 1)
            Center_Points = [X, Y, R]
        return Center_Points

def nothing(x):
    # any operation
    pass

cap = cv2.VideoCapture(0)
cap.set(3,WIDTH)
cap.set(4,HIGHT)
fgbg = cv2.createBackgroundSubtractorMOG2(5,150,False)

cv2.namedWindow("Trackbars",cv2.WINDOW_AUTOSIZE)
cv2.createTrackbar("1", "Trackbars", 10, 50, nothing)
cv2.createTrackbar("2", "Trackbars", 10, 50, nothing)
cv2.createTrackbar("3", "Trackbars", 10, 50, nothing)
cv2.createTrackbar("4", "Trackbars", 130, 255, nothing)

while(True):
    LVL_1 = cv2.getTrackbarPos("1", "Trackbars")
    LVL_2 = cv2.getTrackbarPos("2", "Trackbars")
    LVL_3 = cv2.getTrackbarPos("3", "Trackbars")
    LVL_4 = cv2.getTrackbarPos("4", "Trackbars")



    ret, frame = cap.read()

    grayscaled = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    medianBlur = cv2.GaussianBlur(grayscaled, (0,0), cv2.BORDER_DEFAULT)
    ret2, threshold2 = cv2.threshold(medianBlur, LVL_4, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.circle(frame, (WIDTH/2, HIGHT/2), 2, (0, 0, 255), 1)

    fgmask = fgbg.apply(grayscaled)
    if list.__len__() == 100 and z:
        contours, hierarchy = cv2.findContours(fgmask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            area = cv2.contourArea(contour)
            if area > 15:
                print "AREA:  " + str(area)
                cv2.imshow("AREA>15",fgmask)
            if area > 50 and area < 600:
                M = cv2.moments(contour)
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                R_calc = int(round(math.sqrt(area/3.14159265)))
                Hits.append((cx,cy,R_calc))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    if cv2.waitKey(1) & 0xFF == ord('z'):
        z= True
    if z:
        if cv2.waitKey(0) & 0xFF == ord('z'):
            pass
    #if cv2.waitKey(0) & 0xFF == ord('z'):
    #    pass

    Point = [[0,0],[0,0],[0,0],[0,0]]
    Point[0] = PointCatcher(1, 1)
    Point[1] = PointCatcher(-1, 1)
    Point[2] = PointCatcher(1, -1)
    Point[3] = PointCatcher(-1, -1)
    CENTER = Center_calculation(Point)
    cv2.putText(frame, str(list.__len__()), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
    if CENTER is not None:
        if list.__len__() >=100:
            cv2.putText(frame,"CENTERED !",(50,70),cv2.FONT_HERSHEY_SIMPLEX, 0.5 , (255,0,0))
            list.pop(0)
        list.append(CENTER)
        for x in xrange(list.__len__()-1):
            X_CENTER += (list[x-1][0] + LVL_1-10)
            Y_CENTER += (list[x-1][1] + LVL_2-10)
            RADIUS += (list[x-1][2] + LVL_3-10)

    X_CENTER = X_CENTER / list.__len__()
    Y_CENTER = Y_CENTER / list.__len__()
    RADIUS = RADIUS / list.__len__()

    # ========================
    # DRAWING CIRCLES
    # ========================
    cv2.circle(frame, (int(X_CENTER), int(Y_CENTER)), RADIUS, (0, 0, 255), 1)
    for x in xrange(10):
        Score = int(((RADIUS) * 50*(x+1)) / 200)
        cv2.circle(frame, (int(X_CENTER), int(Y_CENTER)),Score , (0, 0, 255), 1)
        ScorePoints[x] = Score
    i = 0
    for P in Hits:
        cv2.circle(frame, (P[0], P[1]), P[2], (200, 67, 0), -1)
        Distance_from_center = int(round(math.sqrt((X_CENTER - P[0])**2+(Y_CENTER - P[1])**2)))
        for eachscore in xrange(ScorePoints.__len__()):
            if Distance_from_center > ScorePoints[eachscore]:
                pass
            else:
                cv2.putText(frame,"HIT!  " + str(10 - eachscore),(50,100 + i*20),cv2.FONT_HERSHEY_SIMPLEX, 0.7 , (255,60,0))
                i +=1
                break
        if Distance_from_center > ScorePoints[9]:
            cv2.putText(frame,"MISS!  0",(50,100 + i*20),cv2.FONT_HERSHEY_SIMPLEX, 0.7 , (255,60,0))
            i += 1


    cv2.ellipse(frame, (X_CENTER,Y_CENTER), (WIDTH/2,HIGHT/2), 30,0, 360, 255, 1)
    cv2.imshow('Obraz',frame)
    #cv2.imshow('frame', fgmask)
    #cv2.imshow('threshold2', threshold2)
    #cv2.imshow('grayscaled', grayscaled)
    #cv2.imshow('medianBlur', medianBlur)
cap.release()
cv2.destroyAllWindows()

