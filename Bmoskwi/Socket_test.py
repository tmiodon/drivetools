import sys, socket
from threading import Thread
def main():
   start_server()
def start_server():
   HOST = "192.168.1.11"
   PORT = 44818 # arbitrary non-privileged port
  # soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   soc2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
   #soc.connect((HOST, PORT))
   #soc.sendall(b'Hello, world')

   RAW_MSG = b"asdasdasdasd"

   soc2.sendto(RAW_MSG,(HOST,PORT))


def clientThread(connection, ip, port, max_buffer_size = 5120):
   is_active = True
   while is_active:
      client_input = receive_input(connection, max_buffer_size)
      if "--QUIT--" in client_input:
         print("Client is requesting to quit")
         connection.close()
         print("Connection " + ip + ":" + port + " closed")
         is_active = False
      else:
         print("Processed result: {}".format(client_input))
         connection.sendall("-".encode("utf8"))
def receive_input(connection, max_buffer_size):
   client_input = connection.recv(max_buffer_size)
   client_input_size = sys.getsizeof(client_input)
   if client_input_size > max_buffer_size:
      print("The input size is greater than expected {}".format(client_input_size))
   decoded_input = client_input.decode("utf8").rstrip()
   result = process_input(decoded_input)
   return result
def process_input(input_str):
   print("Processing the input received from client")
   return "Hello " + str(input_str).upper()
if __name__ == "__main__":
   main()