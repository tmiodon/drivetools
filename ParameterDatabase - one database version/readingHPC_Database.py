from openpyxl import load_workbook
import os, warnings
import CreatingDatabaseByName, CreatingDatabaseByNumber
import time

import CONFIG_FILE

def DeleteFiles(device):
    try:
        os.remove(device + '.py')
        print "Files for " + str(device) +  " deleted!"
    except:
        print "No files for " + str(device) + "!"
    return


#####################################
# Choosing operation options
# 0 - only delete files
# 1 - only create files
# 2 - delete old files and create new
mode = 2
#####################################

list_sheets_only = 0
CONFIG_FILE.skip_mv_parameters = 1
CONFIG_FILE.print_debug_data = 0

#####################################
# Determining for which configuration
# database will be created
createFor755 = True
createFor6000 = True
#####################################

start = time.time()

if mode == 0:
    print "We are now in DELETE mode!"
elif mode == 1:
    print "We are now in CREATE mode."
elif mode == 2:
    print "We are now in COMBINED mode."
else:
    print "WTF?!"


# Removing old files to make sure that new are created
if mode == 0 or mode == 2 and list_sheets_only == 0:
    DeleteFiles(device = 'parametersByName')
    DeleteFiles(device='parametersByNumber')

# Creating new database
if mode == 1 or mode == 2:
    print "Opening excel file."
    warnings.simplefilter("ignore")
    wb = load_workbook('HPC Database - Draft.xlsm')
    warnings.simplefilter("default")
    list_of_sheets = wb.sheetnames
    print "File is opened."

    if list_sheets_only == 0:
        if createFor755:
            CreatingDatabaseByName.CreateDatabase(workbook=wb, listOfSheets=list_of_sheets)
            CreatingDatabaseByNumber.CreateDatabase(workbook=wb, listOfSheets=list_of_sheets)

        # if createFor6000:
        #     CreatingDatabaseByName.CreateDatabase(workbook=wb, listOfSheets=listOfSheets)
        #     CreatingDatabaseByNumber.CreateDatabase(workbook=wb, listOfSheets=listOfSheets)
    if list_sheets_only == 1:
        iterator = 0
        for sheet in list_of_sheets:
            print str(iterator) + ' - ' + sheet
            iterator = iterator + 1
    stop = time.time()

    print (stop - start)
