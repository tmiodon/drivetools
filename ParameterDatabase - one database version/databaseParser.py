# -------------------------------------------------------------------
#
# FILE NAME:    databaseParser.py
# VERSION:      0.4
# CONTRIBUTORS: Tomasz Miodonski
# START DATE:   12/19/2018
#
# DESCRIPTION:  Methods to extract parameter data and save them to Python
#               friendly form.
#
# NOTES:        Yet another version of this tool. This time I try to design class
#               that will create file for single drive, single port.
#               Creating multiple instances of this will create whole database.
#
# -------------------------------------------------------------------
import CONFIG_FILE
import skipEnumsFor

class ParameterSet():



    def __init__(self, workbook = None):
        # Define properties related to parameters
        self.workbook = workbook
        self.port = []
        self.number = []
        self.name = []
        self.format = []
        self.isWritable = []
        self.isWritableWhenRun = []
        self.isEnum = []
        self.enumsInput = []
        self.enumsOutput = []

        self.CommercialRelease = 'A'
        self.available_releases = [
            "CR1",
            "CR1 R2",
            "CR1 R3",
            "CR2",
            "CR2 R4",
            "CR2 R5",
            "CR2 R6",
            "CR3",
        ]
        return

    ###############################################################
    # This method fills parameter names, numbers, formats, info
    # about writability, if parameters are enum.
    ###############################################################
    def AppendCoreInfo(self, parameterSheet, port):
        sheet = self.workbook[parameterSheet]

        parameterNumber = 'C2'
        i = 2
        j = i - 2
        while sheet[parameterNumber].value != None:
            parameterName = 'D' + str(i)
            parameterFormat = 'H' + str(i)
            parameterIsWritable = 'AE' + str(i)
            parameterIsWritableWhenRun = 'AF' + str(i)
            parameterIsEnum = 'AD' + str(i)
            if sheet[self.CommercialRelease + str(i)].value in self.available_releases:
                if sheet[parameterNumber].value in self.number:
                    self.port[j-1] = port
                    self.number[j-1] = sheet[parameterNumber].value
                    self.name[j-1] = sheet[parameterName].value.strip()
                    self.format[j-1] = sheet[parameterFormat].value
                    self.isWritable[j-1] = sheet[parameterIsWritable].value
                    self.isWritableWhenRun[j-1] = sheet[parameterIsWritableWhenRun].value
                    self.isEnum[j-1] = sheet[parameterIsEnum].value
                else:
                    self.port.append(port)
                    self.number.append(sheet[parameterNumber].value)
                    self.name.append(sheet[parameterName].value.strip())
                    self.format.append(sheet[parameterFormat].value)
                    self.isWritable.append(sheet[parameterIsWritable].value)
                    self.isWritableWhenRun.append(sheet[parameterIsWritableWhenRun].value)
                    self.isEnum.append(sheet[parameterIsEnum].value)
                    j = j + 1
            i = i + 1
            parameterNumber = 'C' + str(i)
        return

    ###############################################################
    # This method appends enumerable values and names
    # to parameters that are enumerable.
    ###############################################################
    def AppendEnums(self, enumSheet):
        sheet = self.workbook[enumSheet]
        if enumSheet == 'Port 0 ICB Parameter Enums':
            number_column = 'B'
            name_column = 'C'
        else:
            number_column = 'A'
            name_column = 'B'
        enumParameterNumber = number_column + '3'
        enumParameterName = name_column + '3'
        for k in range(len(self.number)):
            if self.isEnum[k] == 1:
                if self.name[k] not in skipEnumsFor.skip_this_parameter:
                    i = 3
                    if CONFIG_FILE.print_debug_data == 1:
                        print self.number[k], self.name[k]
                    while sheet[enumParameterName].value != self.name[k]:
                        i = i + 1
                        enumParameterName = name_column + str(i)
                        enumParameterNumber = number_column + str(i)
                    if CONFIG_FILE.print_debug_data == 1:
                        print i
                    inputDict = {}
                    outputDict = {}
                    j = i + 1
                    if enumSheet == 'Port 0 ICB Parameter Enums':
                        enum_value_column = 'D'
                        enum_name_column = 'E'
                    else:
                        enum_value_column = 'C'
                        enum_name_column = 'D'
                    enumValue = enum_value_column + str(j)
                    enumName = enum_name_column + str(j)
                    while sheet[enumValue].value != None:
                        if sheet[enumName].value != 'Reserved':
                            inputDict[sheet[enumName].value] = sheet[enumValue].value
                            outputDict[sheet[enumValue].value] = sheet[enumName].value
                        j = j + 1
                        enumValue = enum_value_column + str(j)
                        enumName = enum_name_column + str(j)
                    if sheet[enumParameterName].value in self.name:
                        self.enumsInput.append([sheet[enumParameterNumber].value, inputDict])
                        self.enumsOutput.append([sheet[enumParameterNumber].value, outputDict])
        return
    ###############################################################
    # This method
    ###############################################################
    def CreateParametersByName(self, fileName):
        file = open(fileName + '.py', 'a')
        for i in range(len(self.number)):
            file.write('\t' + '\'' + str(self.name[i]) + '\'' + ': ' + '{\'port\': ' + str(self.port[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'number\': ' + str(self.number[i]) + ',\n')
            if self.format[i] == 'BOOL[16]':
                file.write('\t\t\t\t\t' + '\'type\': \'WORD\',\n')
            elif self.format[i] == 'BOOL[32]':
                file.write('\t\t\t\t\t' + '\'type\': \'DWORD\',\n')
            else:
                file.write('\t\t\t\t\t' + '\'type\': ' + '\'' + str(self.format[i]) + '\'' + ',\n')
            file.write('\t\t\t\t\t' + '\'isWritable\': ' + str(self.isWritable[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'isWritableWhenRun\': ' + str(self.isWritableWhenRun[i]) + ',\n')
            file.write('\t\t\t\t\t' + '\'isEnum\': ' + str(self.isEnum[i]) + ',\n')
            for j in range(len(self.enumsInput)):
                if self.number[i] == self.enumsInput[j][0]:
                    file.write('\t\t\t\t\t' + '\'enumsInput\': ' + str(self.enumsInput[j][1]).replace(' ', '') + ',\n')
                    file.write('\t\t\t\t\t' + '\'enumsOutput\': ' + str(self.enumsOutput[j][1]).replace(' ', '') + '\n')
            file.write('\t\t\t\t\t},\n')
        file.close()
        return

        ###############################################################
        # This method
        ###############################################################
    def CreateParametersByNumber(self, fileName):
        file = open(fileName + '.py', 'a')
        file.write('\t {\n')
        for i in range(len(self.number)):
            if self.format[i] == 'BOOL[16]' or self.format[i] == 'BOOL[32]':
                dataFormat = str(self.format[i])
                file.write('\t\t' + str(self.number[i]) + ': \'' + 'DWORD' + '\',\n')
            else:
                file.write('\t\t' + str(self.number[i]) + ': \'' + str(self.format[i]) + '\',\n')
        file.write('\t },\n')
        file.close()
        return

