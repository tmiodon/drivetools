from databaseParser import *
import CONFIG_FILE

def CreateDatabase(workbook = None, listOfSheets = None):
    file = open('parametersByName' + '.py', 'w')
    file.write(
        '# This is stored as dictionary\n# In future maybe new method will be introduced to keep parameter database\n\n')
    file.write('parametersByName' + '= {\n')
    file.close()

    port0 = ParameterSet(workbook=workbook)
    port0.AppendCoreInfo(listOfSheets[2], port = 0)
    port0.AppendEnums(listOfSheets[3])
    port0.CreateParametersByName('parametersByName')
    print "Port 0 ready"

    port7 = ParameterSet(workbook=workbook)
    port7.AppendCoreInfo(listOfSheets[4], port=7)
    port7.AppendEnums(listOfSheets[5])
    port7.CreateParametersByName('parametersByName')
    print "Port 7 ready"

    port9 = ParameterSet(workbook=workbook)
    port9.AppendCoreInfo(listOfSheets[6], port = 9)
    port9.AppendEnums(listOfSheets[7])
    port9.CreateParametersByName('parametersByName')
    print "Port 9 ready"

    port10 = ParameterSet(workbook=workbook)
    port10.AppendCoreInfo(listOfSheets[8], port = 10)
    # port10.AppendEnums(listOfSheets[10])
    port10.CreateParametersByName('parametersByName')
    print "Port 10 ready"

    port12 = ParameterSet(workbook=workbook)
    # PLI related parameters
    port12.AppendCoreInfo(listOfSheets[12], port = 12)
    port12.AppendEnums(listOfSheets[13])
    port12.CreateParametersByName('parametersByName')
    # PCCB related parameters
    if CONFIG_FILE.skip_mv_parameters == 0:
        port12_MV = ParameterSet(workbook=workbook)
        port12_MV.AppendCoreInfo(listOfSheets[14], port=12)
        port12_MV.AppendEnums(listOfSheets[15])
        port12_MV.CreateParametersByName('parametersByName')
    print "Port 12 ready"

    port13 = ParameterSet(workbook=workbook)
    port13.AppendCoreInfo(listOfSheets[10], port = 13)
    port13.AppendEnums(listOfSheets[11])
    port13.CreateParametersByName('parametersByName')
    print "Port 13 ready"

    port14 = ParameterSet(workbook=workbook)
    # PLI related parameters
    port14.AppendCoreInfo(listOfSheets[16], port = 14)
    port14.AppendEnums(listOfSheets[17])
    port14.CreateParametersByName('parametersByName')
    # PIOB related parameters
    if CONFIG_FILE.skip_mv_parameters == 0:
        port14_MV = ParameterSet(workbook=workbook)
        port14_MV.AppendCoreInfo(listOfSheets[18], port=14)
        port14_MV.AppendEnums(listOfSheets[19])
        port14_MV.CreateParametersByName('parametersByName')
    print "Port 14 ready"

    port0_diag = ParameterSet(workbook=workbook)
    port0_diag.AppendCoreInfo(listOfSheets[20], port=0)
    port0_diag.CreateParametersByName('parametersByName')
    print "Port 0 diagnostic ready"

    port10_diag_LV = ParameterSet(workbook=workbook)
    port10_diag_LV.AppendCoreInfo(listOfSheets[21], port=10)
    port10_diag_LV.CreateParametersByName('parametersByName')
    print "Port 10 diagnostic LV ready"

    port10_diag_MV = ParameterSet(workbook=workbook)
    port10_diag_MV.AppendCoreInfo(listOfSheets[22], port=10)
    port10_diag_MV.CreateParametersByName('parametersByName')
    print "Port 10 diagnostic MV ready"

    port12_diag = ParameterSet(workbook=workbook)
    port12_diag.AppendCoreInfo(listOfSheets[23], port=12)
    port12_diag.CreateParametersByName('parametersByName')
    print "Port 12 diagnostic ready"

    port13_diag = ParameterSet(workbook=workbook)
    port13_diag.AppendCoreInfo(listOfSheets[24], port=13)
    port13_diag.CreateParametersByName('parametersByName')
    print "Port 13 diagnostic ready"

    port14_diag = ParameterSet(workbook=workbook)
    port14_diag.AppendCoreInfo(listOfSheets[25], port=14)
    port14_diag.CreateParametersByName('parametersByName')
    print "Port 14 diagnostic ready"

    port14_diag_PIOB = ParameterSet(workbook=workbook)
    port14_diag_PIOB.AppendCoreInfo(listOfSheets[26], port=14)
    port14_diag_PIOB.CreateParametersByName('parametersByName')
    print "Port 14 diagnostic PIOB ready"

    file = open('parametersByName' + '.py', 'a')
    file.write('\t}\n')
    print "File for " + " created."
    file.close()
    return