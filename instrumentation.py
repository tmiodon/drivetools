import instrumentationLibrary.scope_base as scope
import Configuration.TestEnvironment as TestEnviornment
import utilsLibrary.Utils as Utils

TestEnviornment.SetTestEnviornment()

my_tek_scope = scope.Scope()

my_tek_scope.Connect('192.168.1.122')
my_tek_scope.TimeBase('10ms')
Utils.Wait(1)
my_tek_scope.TimeBase('1ms')
Utils.Wait(1)
my_tek_scope.TimeBase('20us')
Utils.Wait(1)
my_tek_scope.TimeBase('100ns')
Utils.Wait(1)
my_tek_scope.TimeBase('100ms')
Utils.Wait(1)
my_tek_scope.TimeBase('1us')
Utils.Wait(1)
my_tek_scope.TimeBase('1s')
my_tek_scope.Disconnect()

TestEnviornment.StopTest()