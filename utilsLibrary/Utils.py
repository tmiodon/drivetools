# -------------------------------------------------------------------
#
# FILE NAME:    Utils.py
# VERSION:      0.1
# CONTRIBUTORS: Tomasz Miodonski
#               Bartosz Moskwik
# START DATE:   03/04/2019
#
# DESCRIPTION:  this class contains miscellaneous methods not directly
#               related to drive functionality, but neccesary from
#               test development standpoint.
#
# -------------------------------------------------------------------

import time
from datetime import datetime
import sys
import struct
import loggingLibrary.loggingdt as logging
import Configuration.TestEnvironment as TestEnviornment

def AbortTestExecution(message='Unspecified'):
    print "[TEST ABORTED]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Reason:" + message
    sys.exit()

def Wait(sec):
    print "[INFO]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Wait " + str(sec) + " seconds"
    time.sleep(sec)

def Comment(comment):
    print "[INFO]" + "(" + str(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]) + ") COMMENT: ", str(comment)

def SectionStart(sectionName):
    print "=================== " + sectionName + " ==================="

def SectionEnd(sectionName):
    print "=================== " + sectionName + " ==================="

def Time():
    return datetime.utcnow()

def VerifyValue(variableToVerify, valueToVerifyAgainst=None, tolerance=None):
    if isinstance(variableToVerify, basestring):
        if variableToVerify == valueToVerifyAgainst:
            print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is the same."
            return True
        else:
            print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is not the same."
            return False
    elif isinstance(variableToVerify, bool):
        if variableToVerify == valueToVerifyAgainst:
            print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is " + str(valueToVerifyAgainst) + "."
            return True
        else:
            print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is not " + str(valueToVerifyAgainst) + "."
            return False
    else:
        if variableToVerify >= valueToVerifyAgainst - tolerance and variableToVerify <= valueToVerifyAgainst + tolerance:
            print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is the same."
            return True
        else:
            print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") Value is not the same."
            return False

def LogPass(message="Unspecified"):
    print "[PASS]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") " + message

def LogFail(message="Unspecified"):
    print "[FAIL]" + "(" + datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + ") " + message

def to_DWORD(float_):
    return struct.unpack('l', struct.pack('f', float_))[0]

def to_FLOAT(int_):
    return struct.unpack('f', struct.pack('l', int_))[0]

def GetVariableExists(variable):
    try:
        if variable in locals() or variable in globals():
            logging.Info("Variable exists.")
            return True
        else:
            logging.Info("Variable doesn't exists.")
            return False
    except:
        logging.Error("Incorrect input.")
        if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
            TestEnviornment.ExitTest()
