from drivesLibrary.driveClass import driveBase
from utilsLibrary import Utils
import SharedWorkflows_SPT

# Test scripts imported
import SYST_1101, SYST_1102, SYST_1103

drive1 = driveBase("192.168.1.100")

Utils.SectionStart("SYST_1101")
SYST_1101.SYST_1101(drive1)
Utils.SectionStart("SYST_1102")
SYST_1102.SYST_1102(drive1)
Utils.SectionStart("SYST_1103")
SYST_1103.SYST_1103(drive1)