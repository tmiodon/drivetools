from utilsLibrary import Utils


def SYST_1103(drive1):
# Clearing faults
    drive1.ClearFault(0)
    drive1.ClearFaultQueue(0)

    drive1.ClearAlarm(0)
    drive1.ClearAlarmQueue(0)

    drive1.WriteParameter(1, "P10 VRef Accel Time1")
    drive1.WriteParameter(1, "P10 VRef Decel Time1")
    drive1.WriteParameter(130, "P10 Maximum Freq")
    drive1.WriteParameter(20, "P10 Vel Limit Pos")
    drive1.WriteParameter(-30, "P10 Vel Limit Neg")
    drive1.WriteParameter(5, "P10 Overspeed Limit")
    drive1.WriteParameter(10, "P10 VRef A Stpt")

    drive1.Start()
    Utils.Wait(20)
    drive1.VerifyParameterValue(10, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(30, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(20, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(-40, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-30, "P10 Output Frequency", tolerance=0.5)

    drive1.Stop()
    Utils.Wait(20)

    drive1.WriteParameter(60, "P10 Vel Limit Pos")
    drive1.WriteParameter(-60, "P10 Vel Limit Neg")
    drive1.WriteParameter(50, "P10 VRef A Stpt")

    drive1.Start()
    Utils.Wait(20)
    drive1.VerifyParameterValue(50, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(65, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(60, "P10 Output Frequency", tolerance=0.5)

    drive1.WriteParameter(-65, "P10 VRef A Stpt")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-60, "P10 Output Frequency", tolerance=0.5)

    drive1.WaitTillParameterHasSteadyValue("P10 VRef Commanded")
    Utils.Wait(20)
    drive1.VerifyParameterValue(-60, "P10 Output Frequency", tolerance = 0.5)

    drive1.Stop()
    Utils.Wait(20)