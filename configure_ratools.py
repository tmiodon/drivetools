pip ###############################################################################
#          Copyright (c) 2018 Rockwell Automation Technologies, Inc.          #
#                            All rights reserved.                             #
###############################################################################
"""Generate pip configuration for RATools installation."""

# Standard Library
import sys
from argparse import ArgumentParser
from os import environ, makedirs, remove, rename
from os.path import isdir, isfile
from platform import architecture
from warnings import warn

# Third Party Packages
# none

# RATools Package
# none

# Relative Imports
# none

# Check Python architecture. We only support 32-bit.
PLATFORM = architecture()[0]
if PLATFORM != '32bit':
    raw_input('Detected {}. Please install 32-bit Python.\n'
              'Press any key to exit'.format(PLATFORM))
    sys.exit(1)

# pylint: disable=pointless-string-statement
'''
## Generate PEM Certificates for pip

1. Navigate to the repository location using Chrome
2. Open the developer tools (Ctrl + Shift + J)
3. `Security` -> `View certificate`
4. `Certification Path`
5. Click on the topmost root certificate in the certification path
6. `View certificate`
7. `Details` -> `Copy to File...`
8. Export the certificate to this folder as DER encoded binary X.509 format
(`certificate.cer` in this example)
9. `openssl x509 -inform DER -outform PEM > certificate.crt < certificate.cer`
10. The contents of `certificate.crt` will now contain the pip certificate to be
used in the `configure_ratools.py` to generate the certificate authority bundle
11. The `certificate.cer` file can now be removed using `rm certificate.cer`
'''

REPOSITORY_INFO = {
    'rockwell': {
        'name': 'Rockwell Enterprise',
        'url': ('https://cvbartifactory.cle.ra.rockwell.com/'
                'artifactory/api/pypi/pypi-dev/simple/'),
        'certificate': u'''-----BEGIN CERTIFICATE-----
MIIF5zCCA8+gAwIBAgIQEOK9EJ3e75dDo5datj5AUTANBgkqhkiG9w0BAQsFADB5
MQswCQYDVQQGEwJVUzEiMCAGA1UEChMZUm9ja3dlbGwgQXV0b21hdGlvbiwgSW5j
LjFGMEQGA1UEAxM9Um9ja3dlbGwgQXV0b21hdGlvbiAtIEVudGVycHJpc2UgUm9v
dCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAgFw0xNTA4MjUxODM1MTlaGA8yMDUw
MDgyNTE4NDUxN1oweTELMAkGA1UEBhMCVVMxIjAgBgNVBAoTGVJvY2t3ZWxsIEF1
dG9tYXRpb24sIEluYy4xRjBEBgNVBAMTPVJvY2t3ZWxsIEF1dG9tYXRpb24gLSBF
bnRlcnByaXNlIFJvb3QgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggIiMA0GCSqG
SIb3DQEBAQUAA4ICDwAwggIKAoICAQC6URXAGX5kyV4dEoMgdTi1cIBnWmpl9JB5
V6hdh325/SGogD9V5s/AaXKR/0XwBFnwtEZhxvwL23zLTxa0i4tazlE2N5s6VMNG
YjysVj/Qr2oNxSKVXw5OzgcBgvGgfH7zMQkJxTcOGAtp9Tr2SLBqJOQOBQ50sO+I
L1fqUbRSHVEC73a0IF1tzdpZm2qLN+xgBowllR2uoIUUzRWESs6v/IOr6e1iimHi
YFBIHV0xujtplVuOewR5f/LZoVGLZepXZge0x6f3uO/xsjkEFtLDjITItfQFEnxU
ijv1qV8wTFcDNtRcBVsfqDjye1jPHFcDS4v1sYhZA84KRSQe3OHX7MF1kuimabI9
CaDz0cd/B7Yriawnoq3OriM+6zXgnu8B8dtOeChGEizh0y3+1wIG9O0qarabCDdz
T3xswkca3j32eZN2+Jwk7SWsR6Pts0lRMoYsryRs+GHzRVBVx2S8FBBqqPbPqVVj
CLWWu0UwEX9KSfgNdaXRAh6bKjm4sLC7cD+9v8zWXc9wv7RLgAk12P/UQCVuXlpi
d9Dl6cim40Clzg8aT+ZMAQ/QtZfkxEXS1yKeNKawJ68wCd1bUx5RZbjRuzURMM0s
ZZa82B0B/Iu6Lwyt0m0+v4wBdywWv9TmDiUUesLNJ+oQjvBLTESS3LpaI1dDSRS5
uH0NqNHhbQIDAQABo2kwZzATBgkrBgEEAYI3FAIEBh4EAEMAQTAOBgNVHQ8BAf8E
BAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUIcyaHbZLvh1g7VN+XNpB
RI6hOsMwEAYJKwYBBAGCNxUBBAMCAQAwDQYJKoZIhvcNAQELBQADggIBADC5ZyN4
12uKyuPPnu+DbZkPO595coXyZWWmze8HUaX1PE5M3Si1LGs2NxJ0ivYt6V+hsZNs
g/DZy5ovjCkDN2x2lfnH6Pau8BAebpfHCQjRLsqPRMg7od7yP8tT8GBlqFjjY6SB
+DxDnfsl+2drZomHlQUzGX7QwTp7cbQf+v2vawkFWF6p7qLZ5CgtscS9y14NsBHO
6ZTLImFQVvRhziH8yRkvFHdPVsNkPPrx4KOHPc20ueJ229tB0Wy8if+nvXw0cxKD
ERb2aHIKqEW7QhY0FIfDHC7DUBNnBWcbrbQgtfiUwPnCARtGGWCvl/znt6bTZc6p
6hj7hDmzihW7d5V8SshwRnBnUcjFGsGJZ2Tqgx3ecYUuYwpnk1n51N1tuA97ntyA
xaCmzFTj1O65QEaUZ5S33L/UfHU/bkScsiUl9ETuvtSYpv+LM+kqICMUEPQM/S9T
bKCRAWqWOIC8sbAvNu26E2ufol9DBWrqZ2RfLhU12LQaCMjnP+WJORIoUiISPpKe
kwzMe3enm5QLX1h+D7uUJAPB2MGIsmX3LRokTcmbn2LrhHYQGMdXnS69do1H7Nau
CHPr/gPvVKzPZH1IYpstvAajEG9CnTADJF9t2KO524dD85p5pZJmKpqXvP34m3FI
SFaC9njssgVLT5j5/Qx9UsThEwMRcj+Wml6/
-----END CERTIFICATE-----'''
    },
    'pypi': {
        'name': 'PyPI',
        'url': 'https://pypi.python.org/simple/',
        'certificate': u'''-----BEGIN CERTIFICATE-----
MIIDxTCCAq2gAwIBAgIQAqxcJmoLQJuPC3nyrkYldzANBgkqhkiG9w0BAQUFADBs
MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
d3cuZGlnaWNlcnQuY29tMSswKQYDVQQDEyJEaWdpQ2VydCBIaWdoIEFzc3VyYW5j
ZSBFViBSb290IENBMB4XDTA2MTExMDAwMDAwMFoXDTMxMTExMDAwMDAwMFowbDEL
MAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3
LmRpZ2ljZXJ0LmNvbTErMCkGA1UEAxMiRGlnaUNlcnQgSGlnaCBBc3N1cmFuY2Ug
RVYgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMbM5XPm
+9S75S0tMqbf5YE/yc0lSbZxKsPVlDRnogocsF9ppkCxxLeyj9CYpKlBWTrT3JTW
PNt0OKRKzE0lgvdKpVMSOO7zSW1xkX5jtqumX8OkhPhPYlG++MXs2ziS4wblCJEM
xChBVfvLWokVfnHoNb9Ncgk9vjo4UFt3MRuNs8ckRZqnrG0AFFoEt7oT61EKmEFB
Ik5lYYeBQVCmeVyJ3hlKV9Uu5l0cUyx+mM0aBhakaHPQNAQTXKFx01p8VdteZOE3
hzBWBOURtCmAEvF5OYiiAhF8J2a3iLd48soKqDirCmTCv2ZdlYTBoSUeh10aUAsg
EsxBu24LUTi4S8sCAwEAAaNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQF
MAMBAf8wHQYDVR0OBBYEFLE+w2kD+L9HAdSYJhoIAu9jZCvDMB8GA1UdIwQYMBaA
FLE+w2kD+L9HAdSYJhoIAu9jZCvDMA0GCSqGSIb3DQEBBQUAA4IBAQAcGgaX3Nec
nzyIZgYIVyHbIUf4KmeqvxgydkAQV8GK83rZEWWONfqe/EW1ntlMMUu4kehDLI6z
eM7b41N5cdblIZQB2lWHmiRk9opmzN6cN82oNLFpmyPInngiK3BD41VHMWEZ71jF
hS9OMPagMRYjyOfiZRYzy78aG6A9+MpeizGLYAiJLQwGXFK3xPkKmNEVX58Svnw2
Yzi9RKR/5CYrCsSXaQ3pjOLAEFe4yHYSkVXySGnYvCoCWw9E1CAx2/S6cCZdkGCe
vEsXCS+0yx5DaMkHJ8HSXPfqIbloEpw8nL+e/IBcm2PN7EeqJSdnoDfzAIJ9VNep
+OkuE6N36B9K
-----END CERTIFICATE-----'''
    },
    'pypi-hosted': {
        'name': 'PyPI Hosted',
        'url': 'https://files.pythonhosted.org/',
        'certificate': u'''-----BEGIN CERTIFICATE-----
MIIDdTCCAl2gAwIBAgILBAAAAAABFUtaw5QwDQYJKoZIhvcNAQEFBQAwVzELMAkG
A1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv
b3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw05ODA5MDExMjAw
MDBaFw0yODAxMjgxMjAwMDBaMFcxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i
YWxTaWduIG52LXNhMRAwDgYDVQQLEwdSb290IENBMRswGQYDVQQDExJHbG9iYWxT
aWduIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDaDuaZ
jc6j40+Kfvvxi4Mla+pIH/EqsLmVEQS98GPR4mdmzxzdzxtIK+6NiY6arymAZavp
xy0Sy6scTHAHoT0KMM0VjU/43dSMUBUc71DuxC73/OlS8pF94G3VNTCOXkNz8kHp
1Wrjsok6Vjk4bwY8iGlbKk3Fp1S4bInMm/k8yuX9ifUSPJJ4ltbcdG6TRGHRjcdG
snUOhugZitVtbNV4FpWi6cgKOOvyJBNPc1STE4U6G7weNLWLBYy5d4ux2x8gkasJ
U26Qzns3dLlwR5EiUWMWea6xrkEmCMgZK9FGqkjWZCrXgzT/LCrBbBlDSgeF59N8
9iFo7+ryUp9/k5DPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8E
BTADAQH/MB0GA1UdDgQWBBRge2YaRQ2XyolQL30EzTSo//z9SzANBgkqhkiG9w0B
AQUFAAOCAQEA1nPnfE920I2/7LqivjTFKDK1fPxsnCwrvQmeU79rXqoRSLblCKOz
yj1hTdNGCbM+w6DjY1Ub8rrvrTnhQ7k4o+YviiY776BQVvnGCv04zcQLcFGUl5gE
38NflNUVyRRBnMRddWQVDf9VMOyGj/8N7yy5Y0b2qvzfvGn9LhJIZJrglfCm7ymP
AbEVtQwdpf5pLGkkeB6zpxxxYu7KyJesF12KwvhHhm4qxFYxldBniYUr+WymXUad
DKqC5JlR3XC321Y9YeRq4VzW9v493kHMB65jUr9TU/Qr6cf9tveCX4XSQRjbgbME
HMUfpIBvFSDJ3gyICh3WZlXi/EjJKSZp4A==
-----END CERTIFICATE-----
'''
    }
}


# use system level for config generation
DIRNAME = 'C:\\ProgramData\\pip\\'

CONF_FILEPATH = DIRNAME + 'pip.ini'
CA_FILEPATH = DIRNAME + 'ca-bundle.crt'


CONFIG = {
    'global': {
        'index-url': REPOSITORY_INFO['rockwell']['url'],
        'extra-index-url': [],
        'cert': CA_FILEPATH
    },
    'install': {
        'no-binary': 'pywin32-postinstall'
    }
}


def check_pip_configs():
    """Check for other pip configuration files and create backups."""
    conf_files = []

    try:
        virtual_env = environ['VIRTUAL_ENV']
    except KeyError:
        pass
    else:
        # virtual_env
        conf_files += [r'{}\pip.ini'.format(virtual_env),
                       r'{}\home\pip\pip.ini'.format(virtual_env)]

    try:
        appdata = environ['APPDATA']
    except KeyError:
        pass
    else:
        # user
        conf_files += [r'{}\pip\pip.ini'.format(appdata)]

    try:
        home = environ['USERPROFILE']
    except KeyError:
        pass
    else:
        # user (legacy)
        conf_files += [r'{}\pip\pip.ini'.format(home)]

    conf_files = [filename.replace('/', '\\')
                  for filename in conf_files if isfile(filename)]

    desc = 'The old pip config file'
    for filename in conf_files:
        temp_filename = '{}.temp'.format(filename)
        rename(filename, temp_filename)
        msg = '\n{} at `{}` is now backed up at `{}`\n'
        print msg.format(desc, filename, temp_filename)


def generate_config():
    """Generate configuration file string.

    :returns:
        generated configuration file string
    :rtype: str

    """
    configuration = ''
    for section_name, section in CONFIG.iteritems():
        configuration += '[{}]\n'.format(section_name)
        for key, value in section.iteritems():
            try:
                # iterable
                value.__iter__
            except AttributeError:
                configuration += '{}={}\n'.format(key, value)
            else:
                indentation = ' ' * (len(key) + 1)
                for i, item in enumerate(value):
                    if i == 0:
                        configuration += '{}={}\n'.format(key, item)
                    else:
                        configuration += '{}{}\n'.format(indentation, item)
        configuration += '\n'
    return configuration


def generate_certificate(repo):
    """Generate certificate from a repository listing.

    :param repo:
        repository information listing
    :type repo: dict

    :returns:
        generated certificate string
    :rtype: str

    """
    return '{} Root Certificate ({})\n{}\n'.format(repo['name'],
                                                   repo['url'],
                                                   repo['certificate'])


def generate_ca_bundle():
    """Generate certificate authority bundle file string.

    :returns:
        generated certificate authority bundle file string
    :rtype: str

    """
    # include all repositories in CA bundle
    return '\n'.join(generate_certificate(repo) for repo in REPOSITORY_INFO.values())


def generate_file(filename, content, desc):
    """Generate file and temp file if changes exist.

    :param filename:
        filename where content will be written
    :type filename: str

    :param content:
        content that will be written
    :type content: str

    :param desc:
        description of the file that is being written
    :type desc: str

    """
    try:
        with open(filename) as open_file:
            old_content = open_file.read()
    except IOError:
        old_content = None

    if content == old_content:

        msg = '\n{} at `{}` is already up to date\n'
        print msg.format(desc, filename)

    else:
        if old_content is not None:

            temp_filename = '{}.temp'.format(filename)
            if isfile(temp_filename):
                remove(temp_filename)

            rename(filename, temp_filename)

            msg = '\n{} at `{}` is now backed up at `{}`\n'
            print msg.format(desc, filename, temp_filename)

            file_mismatch = (
                '\nMove any custom changes in `{}` to `{}`, '
                'because the temp file will be overwritten the next time '
                'this script is executed.\n'
            ).format(temp_filename, filename)

            warn(file_mismatch)

        with open(filename, 'wb') as open_file:
            open_file.write(content)

        print '\n{} written to `{}`\n'.format(desc, filename)


def main():
    """Generate pip configuration for RATools installation."""
    parser = ArgumentParser(
        description='Generate pip configuration for RATools installation.')
    parser.add_argument(
        '--include-pypi', action='store_true', help='Include the '
        'Python Package Index (PyPI) repository server.')
    parser.add_argument(
        '--longterm', action='store_true', help='Point to the long term retention '
        'repositories only.')
    args = parser.parse_args()

    if args.include_pypi:
        CONFIG['global']['extra-index-url'].append(REPOSITORY_INFO['pypi']['url'])

    if args.longterm:
        longterm_url = CONFIG['global']['index-url'].replace('pypi-dev', 'pypi-release')
        REPOSITORY_INFO['rockwell']['url'] = longterm_url
        CONFIG['global']['index-url'] = longterm_url

    # check for other pip config file locations
    check_pip_configs()

    # create folder if it does not exist
    if not isdir(DIRNAME):
        makedirs(DIRNAME)

    # create the pip config file
    generate_file(CONF_FILEPATH,
                  generate_config(),
                  'System level pip configuration')

    # create the certificate authority file
    generate_file(CA_FILEPATH,
                  generate_ca_bundle(),
                  'System level pip certificate authority bundle')


if __name__ == '__main__':
    main()
