import utilsLibrary.Utils as utils
import SharedWorkflows_SPT as SharedWorkflows

def SetMotorControlMode(drive, motorControlMode):
    motorControlExists = utils.GetVariableExists('motorControl')
    if motorControlExists == False:
        motorControl = 'Primary'
    if motorControl == 'Secondary':
        drive.WriteParameter(1, "Motor Ctrl Sel")
        utils.Wait(2)
        mtrCtrlMode = drive.ReadParameter("Sec MtrCtrl Act")
        drive.WriteParameter(motorControlMode, "Sec MtrCtrl Mode")
        drive.UseParametersFromSecondaryMotorControl()
    else:
        drive.WriteParameter(1, "Motor Ctrl Sel")
        utils.Wait(2)
        mtrCtrlMode = drive.ReadParameter("Pri MtrCtrl Act")
        drive.WriteParameter(motorControlMode, "Pri MtrCtrl Mode")
        drive.UseParametersFromPrimaryMotorControl()
    if mtrCtrlMode != motorControlMode:
        utils.Wait(10)
        SharedWorkflows.Drive.Initialize.ResetDriveAndReconnect()
    return
