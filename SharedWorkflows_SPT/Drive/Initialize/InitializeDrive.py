#
# Shared Workflow: InitializeDrive
# AUTHOR:
# CREATION DATE:
# PURPOSE:



import Configuration.TestEnvironment as TestEnviornemnt
import drivesLibrary.driveClass as drives
import SharedWorkflows_SPT as SharedWorkflows

drive = drives.driveBase()

# def InitializeDrive(drive, speedUnits, motorControlMode):

TestEnviornemnt.AbortExecutionOnException()
TestEnviornemnt.SetTestEnviornment()

drive.encoder_port_number = 5
drive.io_port_number = 4

# Connect to drive
drive.Connect()
if drive.Connected == False:
    SharedWorkflows.Drive.Initialize.ResetDriveAndReconnect()

# Not sure what those Primary, Secondary motor control actors do.
# Check Motor Control Selected and act accordingly
mtr_ctrl_sel_act = drive.ReadParameter("P0 Mtr Ctrl Sel Act")
if mtr_ctrl_sel_act == 1:
    drive.UseParametersFromSecondaryMotorControl()
else:
    drive.UseParametersFromPrimaryMotorControl()

# Check if drive is active, stop if running
active_bit = drive.ReadParameterBit(1, "P10 Motor Side Sts 1")
if active_bit == 1:
    drive.Stop()
    drive.WaitUntilParameterBitHasValue()
    if TestEnviornemnt.RESULT == False:
        TestEnviornemnt.ExitTest('Couldn\'t stop DUT')

# Clear faults and alarms (maybe would be good to create method that combines this)
drive.ClearFault()
drive.ClearFaultQueue()
drive.ClearAlarm()
drive.ClearAlarmQueue()

# We want to determine on which platform test is running, therefore use proper
config_family_rating = drive.ConfigCode.strip() + '_' + drive.FamilyCode.strip() + '_' + drive.RatingID.strip()