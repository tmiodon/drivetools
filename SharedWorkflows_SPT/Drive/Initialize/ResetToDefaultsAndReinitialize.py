import Configuration.TestEnvironment as TestEnviornment
import loggingLibrary.loggingdt as logging
import SharedWorkflows_SPT as SharedWorkflows


def ResetToDefaultsAndReinitialize(drive, speedUnits, motorControlMode):
    defaultParameters = drive.SetDefaultParameters(port=0, mode="SystemDefault")
    if defaultParameters == False:
        TestEnviornment.ExitTest("Set to default parameters failed!")
    else:
        logging.Pass("DUT has been set to default parameters.")

    SharedWorkflows.Drive.Initialize.SetSpeedUnits(speedUnits = speedUnits)
    SharedWorkflows.Drive.Initialize.SetMotorControlMode(motorControlMode = motorControlMode)
    SharedWorkflows.Drive.Initialize.SetHPCDefaultParameters()

    clearFaults = drive.ClearFault()
    driveFaulted = drive.IsFaulted()
    if driveFaulted == True:
        TestEnviornment.ExitTest("DUT faulted.")
    else:
        logging.Pass("DUT is not faulted.")

    faultQueueCleared = drive.ClearFaultQueue()
    if faultQueueCleared == False:
        logging.Error("Fault queue is not cleared.")

    return
