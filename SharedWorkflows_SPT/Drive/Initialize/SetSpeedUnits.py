import SharedWorkflows_SPT as SharedWorkflow


def SetSpeedUnits(drive, speedUnits):
    spdUnits = drive.ReadParameter("Vel Units Act")
    if spdUnits != speedUnits:
        drive.WriteParameter(speedUnits, "Velocity Units")
        SharedWorkflow.Drive.Initialize.ResetDriveAndReconnect()
    return
