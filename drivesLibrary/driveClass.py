# -------------------------------------------------------------------
#
# FILE NAME:    driveClass.py
# VERSION:      0.1
# CONTRIBUTORS: Tomasz Miodonski
#               Bartosz Moskwik
# START DATE:   01/02/2019
#
# DESCRIPTION:  driveClass library contains definition and implementation
#               of HPC drive family with
#
# -------------------------------------------------------------------

# Standard packages
import time
import struct
import os
import atexit
from threading import Thread

# RATOOLS packages
import ratools.cip.channel as channel
import ratools.cip.messaging as messaging
from ratools.cip.classes.connection_manager import ForwardOpenConnectionParameters
from ratools.cip.drivers.ethernet_ip import ChannelCfg
from ratools.cip.drivers.ethernet_ip_secure import SocketType

# LVPY packages
import loggingLibrary.loggingdt as logging
import Configuration.TestEnvironment as TestEnviornment

# LVPY parameter databases
from ParameterDatabase.parametersByNameAFE import parametersByNameAFE
from ParameterDatabase.parametersByNumberAFE import parametersByNumberAFE


class driveBase():
    """*placeholder for short description*

    *palceholder for long description*

    """

    # This part needs to be cleaned up and described better.
    # What are those constants? Is error_counter used?
    READ = 0x68  
    WRITE = 0x67
    HEAD_CIP = "".join(chr(i) for i in [0x4b, 0x02, 0x20, 0x67, 0x24, 0x01])# Header CIP
    error_counter = 0

    def __init__(self, ipAddress=None, connection=False, PCCC=True, ConnectOnCreate = False):
        """*placeholder for short description*

        *palceholder for long description*


        :param ipAddress: IP address of device under test.
        :param connection: Defines if channel is in connected or unconnected mode.
        :param PCCC: Selects if PCCC connection and background thread for constant PCCC status messages.
        :param ConnectOnCreate: Defines if device is connected on object creation or with separate method later.
        """

        # Device related data.
        self.RatingID = None
        self.DeviceSerialNumber = None
        self.CustomizationCode = None
        self.CustomizationRevision = None
        self.BrandCode = None
        self.FamilyCode = None
        self.ConfigCode = None
        self.LanguageCode = None
        self.MajorRevision = None
        self.MinorRevision = None
        self.Build = None
        self.FirmwareRevision = None

        # Device configuration data.
        self.motor_control_mode_selected = 'Primary'
        self.encoder_port_number = 0
        self.io_port_number = 0

        # Connection related data.
        # Probably will need some cleanup.
        self.ipAddress = ipAddress
        self.dut_path = r'AB_ETHIP-1\\' + self.ipAddress
        self.close_conn = False
        self.Connected = False
        self.PCCstarted = False
        self.IOconnectionStarted = False
        self.READ = 0x68  # type:
        self.WRITE = 0x67
        self.TNSW = 0x00
        self.TNSW2 = 0x00
        self.c = [0x4b, 0x02, 0x20, 0x67, 0x24, 0x01]  # Header CIP
        self.head_cip = "".join(chr(i) for i in self.c)
        self.head_pccc = ""
        self.ipAddress = ""
        self.StatusWord = [0x00, 0x00, 0x00, 0x00]
        self.CommandWord = 0x00000000
        self.Refference = 0.0
        self.Feedback = 0.0

        # Because sometimes it's convenient to connect to drive at time of initializing object, hence this part
        # of __init__ method. For other cases there is separate 'connect' method.
        if ConnectOnCreate:
            self.ipAddress = ipAddress
            try:
                self.close_conn = False
                self.ch = channel.create(keys='PF755T', path=self.dut_path, connected=connection, driver='ethernet_ip', authenticate=[])
                print self.ch
                logging.Info("Channel created to : " + ipAddress + " !")
                self.Connected = True
                self.GetDeviceDataOnConnection()
            except Exception, e:
                logging.Error("Cannot connect to drive at " + ipAddress + " !")
                logging.Error("\t" + str(e))
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()

            # It's not always desirable to create PCCC loop, hence this can be disabled.
            if PCCC:
                try:
                    self.t1 = Thread(target=self.PCCCLoop)
                    self.t1.setDaemon(True)
                    self.t1.start() # start separated PCCCLoop that keeps connection to drive. In separated thread.
                    atexit.register(self.exit_handler)
                    while not self.PCCstarted:
                        pass
                except:
                    logging.Error("Can not start new thread for PCCC communication. Test will be closed!")
                    exit(123)

   ###############################################################
    def Connect(self, ipAddress=None, connected=False, PCCC=True):
        """Connects to drive.

        *placeholder for longer description*

        :param ipAddress: IP address of drive.
        :param connected can be either 'unconnected' or 'connected'
        :param PCCC: indicates whether PCCC connection should be created
        :return:
        """

        # This case of 'raise' is justified as if there is no IP address provided for drive, there is no point in
        # executing test further.
        # Still, exit procedure must be provided for this 'raise'.
        if self.ipAddress is None:
            if ipAddress is None:
                raise ValueError(logging.Error("\t" + "IP address can not be None !"))
            self.ipAddress = ipAddress
            self.dut_path = r'AB_ETHIP-1\\' + self.ipAddress
        try:
            self.close_conn = False
            self.ch = channel.create(keys='PF755T', path=self.dut_path, connected=connected, driver='ethernet_ip', authenticate=[])
            logging.Info("Channel created to : " + str(self.ipAddress) + " !")
            self.Connected = True
            self.GetDeviceDataOnConnection()
        except Exception, e:
            logging.Error("Cannot connect to drive at " + str(self.ipAddress) + " !")
            logging.Error("\t" + str(e))
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

        if PCCC:
            try:
                self.t1 = Thread(target=self.PCCCLoop)
                self.t1.setDaemon(True)
                self.t1.start()
                atexit.register(self.exit_handler)
                while not self.PCCstarted:
                    pass
            except:
                logging.Error("Can not start new thread for PCCC communication. Test will be closed!")
                exit(123)

    # After connecting there is a need to collect all device related data, like drive model, type etc.
    # This will be made each time connection is created.
    def GetDeviceDataOnConnection(self):
        if self.Connected:
            address = [[0x92, 0]]
            attribute = [34]
            format = {34: 'BDATA'}
            try:
                response = messaging.getatrsingle(address, attribute, format)
                format = 'LLHHHBBBBBBBBBBBBBBBBBBBBB'
                key_information = struct.unpack(format, response[34])
                self.RatingID = key_information[0]
                self.DeviceSerialNumber = key_information[1]
                self.CustomizationCode = key_information[2]
                self.CustomizationRevision = key_information[3]
                self.BrandCode = key_information[4]
                self.FamilyCode = key_information[5]
                self.ConfigCode = key_information[6]
                self.LanguageCode = key_information[7]
                self.MajorRevision = key_information[8]
                self.MinorRevision = key_information[9]
            except:
                logging.Error("Can't collect data from drive!")
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()

    ###############################################################
    def Disconnect(self, PCCC=True):
        """Disconnect from drive .

        *placeholder for long description*

        :param ipAddress: IP address of drive.
        :param connected can be either 'unconnected' or 'connected'
        :param PCCC: indicates whether PCCC connection should be created
        :return:
        """
        try:
            if PCCC:
                b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, 0x00, 0x00]  # body
                body = "".join(chr(i) for i in b)
                messaging.cipmessage(self.HEAD_CIP + self.BuildPCCCHeader(self.WRITE) + body)
                self.PCCstarted = False
            self.close_conn = True
            self.ch.close_connection()
            logging.Info("Connection to drive is closed")
            self.Connected = False
        except :
            logging.Error("Can not disconnect from drive at: " + self.ipAddress)
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    # In order to close connection gracefully (without drive failing), PCCC timeout needs to be set to 0.
    def exit_handler(self):
        if not self.close_conn:
            self.BuildPCCCHeader(self.WRITE)
            self.close_conn = True
            b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, 0x00, 0x00]  # body
            body = "".join(chr(i) for i in b)
            messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)
            time.sleep(0.1)
            logging.Info("PCCC connection to drive is closing!")

    def ConnectIOConnection(self, ipAddress=None, inData=16, outData=16, pullTime=2, initDataLinks_Out=None):
        """Create Class 1 Conenction to drive.

        Connection allow to create 1 class connection, sending DataLinks,

        :param ipAddress: IP address of drive.
        :param inData: number in range 0 to 16 - amount of DataLinks to be received from drive
        :param outData: number in range 0 to 16 - amount of DataLinks to be send to drive
        :param pullTime: number in range 2 to 3000, miliseconds of pull cycle interval.
        :param initDataLinks_Out: initial values of Datalinks that can be passed during link creation, otherwise
            datalinks starts with zero list.
        :return:
        """
        self.DataLinks_In = [0x00000000] * inData
        if initDataLinks_Out == None:
            self.DataLinks_Out = [0x00000000]*outData
        else:
            if initDataLinks_Out.__len__ > inData:
                raise ValueError(logging.Error("\t" + "DataLinks table size incorrect !"))
            self.DataLinks_Out = initDataLinks_Out

        if self.ipAddress == None:
            self.ipAddress = ipAddress
        if self.ipAddress == None:
            raise ValueError(logging.Error("\t" + "IP address can not be None !"))
        OT_RPI = pullTime*1000
        TO_RPI = pullTime*1000
        multiplier = 4

        OT_CONN_PARAMS = ForwardOpenConnectionParameters(
            connection_size=14+4*inData,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            redundant_owner=False)

        TO_CONN_PARAMS = ForwardOpenConnectionParameters(
            connection_size=14+4*outData,
            connection_type=ForwardOpenConnectionParameters.CONNECTION_TYPE_POINT_TO_POINT,
            priority=ForwardOpenConnectionParameters.PRIORITY_SCHEDULED,
            redundant_owner=False)

        conn_params = {
            'transport_class': 1,
            'trigger': 0,
            'direction': 0,
            'to_rpi': TO_RPI,
            'ot_rpi': OT_RPI,
            'connection_path': [(0x34, [0x0001, 0x0000, 0x000, 0x00, 0x00]),
                                (0x20, 0x04),
                                (0x24, 0x06),
                                (0x2c, 0x02),
                                (0x2c, 0x01), ],
            'connection_timeout_multiplier': multiplier,
            'to_connection_parameters': TO_CONN_PARAMS,
            'ot_connection_parameters': OT_CONN_PARAMS
        }
        try:
            channel_cfg = ChannelCfg(path=self.dut_path, connected=False, socket_type=SocketType.UDP)
            self.IOchannel = channel_cfg.create_channel()
            self.IOconnection = self.IOchannel.create_connection(
                receive_thread=self.Received_DL,
                send_thread=self.Send_DL,
                open_connection=True,
                **conn_params)
        except Exception, e:
            
            logging.Error("Cannot set IO connection to drive at " + self.ipAddress + " !")
            logging.Error("\t" + str(e))
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

        # If you have time try to delete this time delay and add here some reliable condition. I tried
        # with IOconnection.is_open() but did not work as expected. Still send thread is faster and generate exception
        time.sleep(0.005)
        self.IOconnectionStarted = True

    def DisconnectIOconnection(self):
        """Disconnect Class 1 Conenction

         Close class 1 connection, rise an error if failed.

         :return:
         """
        try:
            self.IOconnection.close()
            self.IOchannel.close()
        except Exception, e:
            logging.Error("Cannot stop IO connection !")
            logging.Error("\t" + str(e))
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
        self.IOconnectionStarted = False

    def Received_DL(self, conn, msg):
        """Received_DL - not for test use, 

         Method that is used by ConnectIOConnection to create recaiving thread. It formats incoming data into known 
            variables.
         :param conn: not used
         :param msg: received message, will be parsed into object attribute.
         :return:
         """
        y = bytearray(msg)
        formatted=[]
        if msg != None:
            for x in range(y.__len__()):
                if len(y[x * 4:]) >= struct.calcsize('<L'):
                    formatted.append(struct.unpack_from('<L', y, x * 4)[0])
                else:
                    break           #This is not so preaty,
            self.StatusWord = formatted[1]
            self.Feedback = formatted[2]
            self.DataLinks_In = formatted[3:]

    def Send_DL(self, msg):
        """Send_DL - not for test use, 

         Method that is used by ConnectIOConnection to create sending thread
        :param msg: not used

         """
        if self.IOconnectionStarted:
            data_to_send = self.IOconnection.pack_data([1] + [self.CommandWord] + [self.Refference] + self.DataLinks_Out,['DWORD', 'DWORD', 'REAL'] + ['DWORD'] * len(self.DataLinks_Out))
            self.IOconnection.sent = data_to_send
            self.IOconnection.send(data_to_send)

    def WaitToBeOnline(self):
        """Wait for device to be avaiable at network.

         Use Os commands to send ping and wait for pong response.

         :return:
         """
        try:
            response = os.system("ping -n 1 -w 100 " + self.ipAddress + " > nul")
            logging.Info("Wait drive to be online. (Ping)")
            while response != 0:
                time.sleep(1)
                response = os.system("ping -n 1 -w 100 " + self.ipAddress+ " > nul")
        except ValueError:
            logging.Error("IP addres not set, initialize drive first.")
            exit(124)


    ###############################################################
    # Perhaps instead of checking range of variable, we can check if instance of DPI Parameter class exists.
    def CheckIfPortCorrect(self, port):
        if port < 0 or port > 14:
            raise ValueError("Port number out of range !")
        elif isinstance(port, int) != True:
            raise ValueError("Port number not integer type !")


    ###############################################################
    def DetermineParameterInstance(self, parameter, port):
        parameterCoordinates = []
        if isinstance(parameter, basestring) == True:
            parameterCoordinates.append(parametersByNameAFE[parameter]['port'])
            if self.motor_control_mode_selected == 'Secondary':
                parameterCoordinates[0] += 1
            parameterCoordinates.append(parametersByNameAFE[parameter]['number'])
            parameterCoordinates.append(parametersByNameAFE[parameter]['type'])
        if isinstance(port, int) == True:
            parameterCoordinates.append(port)
            parameterCoordinates.append(parameter)
            parameterCoordinates.append(parametersByNumberAFE[port][parameter])
        return parameterCoordinates

    ###############################################################
    # We need later to improve this with Rhino drives
    #
    # Config text for different devices:
    # - Regenerative Drv (AFE)
    # - Low Harmonic Drv (???)
    # - Regen Bus Supply (RBS)
    # - Common Bus Inv (CBI)
    def GetDriveType(self):
        """Returns type of HPC drive"""

        address = [[0x92, 0]]
        attribute = [1]
        format = {1: 'BDATA'}
        try:
            response = messaging.getatrsingle(address, attribute, format)
            logging.Info("Drive type is: " + str(response[1]))
            return response[1].replace('PowerFlex ', '').replace(' ', '')
        except:
            logging.Error("Can't determine drive type!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    def GetDriveType2(self):
        address = [[0x92, 0]]
        attribute = [8]
        format = {8: 'BDATA'}
        try:
            response = messaging.getatrsingle(address, attribute, format)
            logging.Info("Drive type is: " + str(response[8]))
            #return response[2].replace('PowerFlex ', '').replace(' ', '')
        except:
            logging.Error("Can't determine drive type!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False


    ###############################################################
    # This is a placeholder for method
    def GetNumberOfPeripherals(self):
        return

    ###############################################################
    # This is a placeholder for method
    def GetDriveFirmwareVersion(self):
        return

    ###############################################################
    # This is a placeholder for method
    def GetPortFirmwareVersion(self):
        return

    ###############################################################
    def GetPortType(self, port):
        self.CheckIfPortCorrect(port)
        _port = 4096*port
        address = [[0x92, _port]]
        attribute = [1]
        format = {1: 'BDATA'}
        try:
            response = messaging.getatrsingle(address, attribute, format)
            logging.Info("Port name recognized.: " + str(response[1]))
            return response[1]
        except:
            logging.Error("Can't determine drive type!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def ResetDrive(self):
        address = [[0x97, 0]]
        attribute = {3: 3}
        format = {3: 'USINT'}
        try:
            self.exit_handler()
            time.sleep(1)
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Resetting drive.")
            return True
        except:
            logging.Error("Can't reset drive!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    # This is a placeholder for method
    def ResetPort(self, port):
        return

    ###############################################################
    def ClearFault(self, port=0):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x97, _port]]
        attribute = {3: 1}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Clearing fault on port: " + str(port))
            return True
        except:
            logging.Error("Clearing fault on port " + str(port) + " failed!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def ClearFaultQueue(self, port=0):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x97, _port]]
        attribute = {3: 2}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Clearing fault queue on port: " + str(port))
            return True
        except:
            logging.Error("Clearing fault queueon port " + str(port) + " failed!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def ClearAlarm(self, port=0):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x98, _port]]
        attribute = {3: 1}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address,attribute,format)
            logging.Info("Clearing alarm on port: " + str(port))
            return True
        except:
            logging.Error("Clearing alarm on port " + str(port) + " failed!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def ClearAlarmQueue(self, port=0):
        self.CheckIfPortCorrect(port)
        _port = 4096 * port
        address = [[0x98, _port]]
        attribute = {3: 2}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address,attribute,format)
            logging.Info("Clearing alarm queue on port: " + str(port))
            return True
        except:
            logging.Error("Clearing alarm queue on port " + str(port) + " failed!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def IsFaulted(self):
        address = [[0x92, 0]]
        attribute = [6]
        format = {6: "STRING"}
        try:
            status = messaging.setatrsingle(address, attribute, format)
            if status == "FAULTED":
                logging.Info("Drive is faulted.")
                return True
            else:
                logging.Info("Drive is not faulted.")
                return False
        except:
            logging.Error("Could not determine if drive is faulted.")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def UseParametersFromPrimaryMotorControl(self):
        """"""

        self.motor_control_mode_selected = 'Primary'
        return

    ###############################################################
    def UseParametersFromSecondaryMotorControl(self):
        """"""

        self.motor_control_mode_selected = 'Secondary'
        return

    ###############################################################
    def SetDefaultParameters(self, port, mode):
        self.CheckIfPortCorrect(port)
        if isinstance(mode, str):
            if mode == "StoreToNVS":
                mode = 1
            elif mode == "LoadFromNVS":
                mode = 2
            elif mode == "LoadWithDefaultAll":
                mode = 3
            elif mode == "LoadWithDefaultOptional":
                mode =4
            elif mode == "SystemDefault":
                mode =5

        if mode == 0:
            logging.Info("No operation is selected for port reset to default.")
            TestEnviornment.RESULT = False
        elif mode > 5:
            logging.Error("Command doesn't exist!")

        _port = 4096 * port
        address = [[0x93, _port]]
        attribute = {2: mode}
        format = {3: 'USINT'}
        try:
            messaging.setatrsingle(address,attribute,format)
            logging.Info("Parameters on port " + str(port) + " set to default.")
            return True
        except:
            logging.Error("Setting default parameters on port " + str(port) + " failed!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def ReadParameter(self, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param parameter:
        :param port:
        :return:
        """

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)

        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            logging.Info("Read Port: " + str(parameterCoordinates[0]) + ", parameter: " + str(parameter) + ",value is: " + str(parameterValue[9]))
            return parameterValue[9]
        except:
            logging.Error("Can not read parameter " + str(parameter) + " on port " + str(parameterCoordinates[0]) + "!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    def GenericCIPMessage(self, classID, instance, attribute, format):
        try:
            address = [[classID, instance]]
            attr = [attribute]
            form = {attribute: format}
            response = messaging.getAtrSingle(address, attr, form)
            return response
        except:
            print("Something went wrong...")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    def ReadParameterBit(self, bit, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param bit:
        :param parameter:
        :param port:
        :return:
        """

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            result = parameterValue[9] & 2**bit != 0
            logging.Info("Read Port: " + str(parameterCoordinates[0]) + ", parameter: " + str(parameter) + ",value is: " + str(result))
            return result
        except:
            logging.Error("Can not read parameter " + str(parameter) + " on port " + str(parameterCoordinates[0]) + "!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
            return False

    ###############################################################
    # This is a placeholder for method
    def ReadParameterMin(self):
        return

    ###############################################################
    # This is a placeholder for method
    def ReadParameterMmax(self):
        return

    ###############################################################
    # This is a placeholder for method
    def ReadParameterDefault(self):
        return

    ###############################################################
    def WriteParameter(self, value=None, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param value:
        :param parameter:
        :param port:
        :return:
        """

        if isinstance(value, basestring):
            _value = parametersByNameAFE[port]['enums'][parameter]
        else:
            _value = value

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Write Port: " + str(parameterCoordinates[0]) + ", parameter: " + str(parameter) + ", with value: " + str(_value))
        except:
            logging.Error("Problem with writing parameter " + str(parameter) + " on port " + str(parameter) + "!")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    def SetParameterBit(self, bit, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param bit:
        :param parameter:
        :param port:
        :return:
        """

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]

        currentValue = self.ReadParameter(parameterCoordinates[0], parameterCoordinates[1])
        mask = 1 << bit
        _value = currentValue | mask

        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Set bit " + str(bit) + " on parameter " + str(parameterCoordinates[0]) + ":" + str(parameter))
        except:
            logging.Error("Can not read parameter " + str(parameter) + " !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    def ClearParameterBit(self, bit, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param bit:
        :param parameter:
        :param port:
        :return:
        """

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])
        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]

        currentValue = self.ReadParameter(parameterCoordinates[0], parameterCoordinates[1])
        mask = ~(1 << bit)
        _value = currentValue & mask

        address = [[0x93, param]]
        attribute = {9: _value}
        format = {9: parameterCoordinates[2]}
        try:
            messaging.setatrsingle(address, attribute, format)
            logging.Info("Clear bit " + str(bit) + " on parameter " + str(parameterCoordinates[0]) + ":" + parameter)
        except:
            logging.Error("Can not read parameter " + str(parameter) + " !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    def VerifyParameterValue(self, value, parameter=None, port=None, tolerance = 0.5):
        """[placehlolder for short description]

        [placeholder for long description]

        :param value:
        :param parameter:
        :param port:
        :param tolerance:
        :return:
        """

        if isinstance(value, basestring):
            _value = parametersByNameAFE[port]['enums'][parameter]
        else:
            _value = value

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096*parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            if parameterValue[9] > (_value - tolerance) and parameterValue[9] < (_value + tolerance):
               logging.Pass("Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + str(parameter) + ",value is correct")
            else:
                logging.Fail("Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + str(parameter) + ", value is incorrect")
        except:
            logging.Error("Problem with verifying parameter " + str(parameter) + " !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    def VerifyParameterBitValue(self, value, bit, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param value:
        :param bit:
        :param parameter:
        :param port:
        :return:
        """

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}
        try:
            parameterValue = messaging.getatrsingle(address, attribute, format)
            result = parameterValue[9] & 2**bit != 0
            if result == value:
               logging.Pass("Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + str(parameter) + ",value is correct.")
            else:
                logging.Fail("Port: " + str(
                    parameterCoordinates[0]) + ", parameter: " + str(parameter) + ",value is incorrect.")
        except:
            logging.Error("Can not read parameter " + str(parameter) + " !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()

    ###############################################################
    # This is a placeholder for method (remove when implemented).
    # Method purpose: waits for parameter to have steady value
    def WaitUnitParameterHasSteadyValue(self, parameter=None, port=None, timeout = 60000):
        """[placehlolder for short description]

        [placeholder for long description]

        :param parameter:
        :param port:
        :param timeout:
        :return:
        """

        startTime = time.time()

        parameterCoordinates = self.DetermineParameterInstance(parameter, port)
        self.CheckIfPortCorrect(parameterCoordinates[0])

        param = parameterCoordinates[1] + 4096 * parameterCoordinates[0]
        address = [[0x93, param]]
        attribute = [9]
        format = {9: parameterCoordinates[2]}

        previousParameterValue = 0
        while (time.time() - startTime < timeout):
            try:
                currentParameterValue = messaging.getatrsingle(address, attribute, format)
                if (currentParameterValue == previousParameterValue):
                    logging.Info("Parameter " + str(parameter) + " is steady on value: " + str(currentParameterValue[9]))
                    return currentParameterValue[9]
                else:
                    previousParameterValue = currentParameterValue
                    time.sleep(0.5)
            except:
                logging.Error("Can not read parameter " + str(parameter) + " !")
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()

    def WaitUntilParameterHasValue(self, expected_value, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param expected_value:
        :param parameter:
        :param port:
        :return:
        """
        return

    def WaitUntilParameterBitHasValue(self, expected_value, bit, parameter=None, port=None):
        """[placehlolder for short description]

        [placeholder for long description]

        :param expected_value:
        :param bit:
        :param parameter:
        :param port:
        :return:
        """
        return


    ###############################################################
    #
    # This section contains drive related commands like start or
    # stop drive based on CIP messaging and PCCC protocol.
    #
    ###############################################################


    ###############################################################
    def BuildPCCCHeader(self, act):
        self.TNSWCalc()
        a = [0x07, 0x4d, 0x00, 0x44, 0x4e, 0x97, 0x3c, 0x0f, 0x00, self.TNSW, self.TNSW2, act,
             0x00, 0x00, 0x02, 0x00]  # header PCC
        self.head_pccc = "".join(chr(i) for i in a)
        return "".join(chr(i) for i in a)

    ###############################################################
    def TNSWCalc(self):
        self.TNSW = self.TNSW + 1
        if self.TNSW >= 0xff:
            self.TNSW = 0x00#
            self.TNSW2 = self.TNSW2 + 1
        if self.TNSW2 >= 0xff:#
            self.TNSW2 = 0x00

    ###############################################################
    def PCCCLoop (self, pccctimeout=10, msgsepparation=0.5):
        TIMEOUT = pccctimeout #Timeout can be adjusted if needed
        ################################## Setting Timeout ########################################
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x32, 0x3a, 0x33, 0x00, 0x99, 0x09, 0x03, 0x42, TIMEOUT, 0x00]  # body
        body = "".join(chr(i) for i in b)
        try:
            messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)  # Send msg
        except:
            logging.Error("Problem with sending PCCC message !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
        ################################## Setting Ref source ########################################
        self.BuildPCCCHeader(self.WRITE)  # Head build
        b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x99, 0x09, 0x05, 0x42, 0x10, 0x10, 0x00, 0x00]  # body
        body = "".join(chr(i) for i in b)
        try:
            messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)  # Send msg
            self.PCCstarted = True
        except:
            logging.Error("Problem with sending PCCC message !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
        while True:

            if self.close_conn:
                break
            time.sleep(msgsepparation) # PCC messages separation, can be adjusted or passed as param.
            if self.IOconnectionStarted == False:
                self.StatusWord = self.ReadStatusWord()
    ###############################################################
    def WriteCommandWord(self, Byte1=0x00, Byte2=0x00, Byte3=0x00, Byte4=0x00): # checked
        if self.IOconnectionStarted == False:
            self.BuildPCCCHeader(self.WRITE)  # Head build
            b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x99, 0x09, 0x05, 0x42, Byte1, Byte2, Byte3, Byte4] # body
            body = "".join(chr(i) for i in b)
            self.CommandWord = (Byte4 << 24) + (Byte3 << 16) + (Byte2 << 8) + Byte1
            try:
                messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)  # Send msg
                return True
            except:
                logging.Error("Problem with sending PCCC message !")
            if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                TestEnviornment.ExitTest()
                return False
        else:
            self.CommandWord = (Byte4 <<24) + (Byte3 <<16) + (Byte2 <<8) + Byte1

    ###############################################################
    def ReadStatusWord(self):
        if self.IOconnectionStarted == False:
            self.BuildPCCCHeader(self.READ)# Head build
            b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x30, 0x00, 0x02, 0x00]  # body build
            body = "".join(chr(i) for i in b)
            try:
                receive = messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)  # Send msg
                return bytearray(receive[15:19])  # return msg response
            except:
                logging.Error("Problem with sending PCCC message !")
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()
                return False
        else:
            return bytearray(struct.pack("L", self.StatusWord))

    ###############################################################
    def WriteRefference(self, refference):
        if self.IOconnectionStarted == False:
            ref = bytearray(struct.pack("f", refference))# Conversion from float to structured bytearray
            self.BuildPCCCHeader(self.WRITE)  # Head build
            b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x32, 0x00, 0x99, 0x09, 0x05, 0x42]  # body build
            body = "".join(chr(i) for i in b)
            refer = "".join(chr(i) for i in ref)
            self.Refference = ref
            try:
                messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body + refer)  # Send msg
                logging.Info("Write refference value: " + str(refference))
                return True
            except:
                logging.Error("Problem with sending PCCC message !")
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()
                return False
        else:
            ref = bytearray(struct.pack("f", refference))
            self.Refference = ref
    ###############################################################
    def ReadFeedback(self):
        if self.IOconnectionStarted == False:
            self.BuildPCCCHeader(self.READ)
            b = [0x00, 0x24, 0x4e, 0x34, 0x35, 0x3a, 0x32, 0x00, 0x02, 0x00]  # body
            body = "".join(chr(i) for i in b)
            try:
                receive = messaging.cipmessage(self.HEAD_CIP + self.head_pccc + body)  # Send msg
                flo = struct.unpack('f', bytearray(receive[15:19]))
                logging.Info("Feedback value is: " + str(round(flo[0], 2)))
                return round(flo[0], 2)
            except:
                logging.Error("Problem with sending PCCC message !")
                if TestEnviornment.ABORT_EXECUTION_ON_EXCEPTION == True:
                    TestEnviornment.ExitTest()
                return False
        else:
           return self.Feedback

    #########################################STATUS FIRST BYTE############################
    def isReady(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0]& 0x01 == 0x01

    def isActive(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x02 == 0x02

    def isCommandedForward(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0]& 0x04 == 0x04

    def isActualForward(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x08 == 0x08

    def isAccelerating(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x10 == 0x10

    def isDecelerating(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x20 == 0x20

    def isAlarm(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x40 == 0x40

    def isFault(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[0] & 0x80 == 0x80

    #########################################STATUS SECOND BYTE############################
    def isAtSpeed(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x01 == 0x01

    def isManual(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x02 == 0x02

    def SpdRefID0(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1]& 0x04 == 0x04

    def SpdRefID1(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x08 == 0x08

    def SpdRefID2(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x10 == 0x10

    def SpdRefID3(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x20 == 0x20

    def SpdRefID4(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[1] & 0x40 == 0x40

    #########################################STATUS THIRD BYTE############################
    def isRunning(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x01 == 0x01

    def isJogging(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x02 == 0x02

    def isStopping(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x04 == 0x04

    def isDCBrake(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x08 == 0x08

    def isDynamicBrakeActive(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x10 == 0x10

    def isSpeedMode(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x20 == 0x20

    def isPositionMode(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x40 == 0x40

    def isTorqueMode(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[2] & 0x80 == 0x80

    #########################################STATUS FOURTH BYTE############################
    def isAtZeroSpeed(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x01 == 0x01

    def isAtHome(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x02 == 0x02

    def isAtLimit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x04 == 0x04

    def isAtCurrentLimit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x08 == 0x08

    def isBusFreqReg(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x10 == 0x10

    def isEnableOn(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x20 == 0x20

    def isMotorOverload(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x40 == 0x40

    def isRegen(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """
        status = self.ReadStatusWord()
        return status[3] & 0x80 == 0x80

    ################################### COMMANDS ###################################################################

    ###############################################################
    # Ugly Control command parser. Feel free to make it pretty.
    def CommandWordParser(self,set, command):

        ReadedLogicRslt = self.ReadParamWithoutComment("P0 MS Logic Rslt")
        CommandByte3 = ReadedLogicRslt >> 16
        CommandByte2 = ReadedLogicRslt >> 8
        CommandByte2 = CommandByte2 & 0x000000ff
        CommandByte1 = ReadedLogicRslt & 0x000000ff
        if command <= 255:
            if set:
                self.WriteCommandWord(CommandByte1|command,CommandByte2, CommandByte3)
            else:
                self.WriteCommandWord(CommandByte1&(0xff-command),CommandByte2, CommandByte3)
        if (command > 0xff and command <= 0xff00):
            command_set = command >> 8
            if set:
                self.WriteCommandWord(CommandByte1, CommandByte2|command_set, CommandByte3)
            else:
                self.WriteCommandWord(CommandByte1, CommandByte2&(0xff-command_set), CommandByte3)
        if command > 0xff00:
            command_set = command >> 16
            if set:
                self.WriteCommandWord(CommandByte1, CommandByte2, CommandByte3|command_set)
            else:
                self.WriteCommandWord(CommandByte1, CommandByte2, CommandByte3&(0xff-command_set))
        return
    ###############################################################
    # Simply use Start command, overwrite rest ControlWord.
    def Start(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.WriteCommandWord(0x12,0x10)
        #self.WriteCommandWord(0x10,0x10)
        logging.Info("Start drive")

    ###############################################################
    # Simply use Stop command, overwrite rest ControlWord.
    def Stop(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.WriteCommandWord(0x11,0x10)
        #self.WriteCommandWord(0x10,0x10)
        logging.Info("Stop drive")

    ###############################################################
    # Simply use Clear Fault command, overwrite rest ControlWord.
    def ClearFaults(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.WriteCommandWord(0x18,0x10)
        #self.WriteCommandWord(0x10,0x10)
#        logging.Info("Clear drive faults (CommandWord)"

    ###############################################################
    # Simply use Forward command, overwrite rest ControlWord.
    def Forward(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.WriteCommandWord(0x10,0x10)
        logging.Info("Drive forward direction (CommandWord)")

    ###############################################################
    # Simply use Backward command, overwrite rest ControlWord.
    def Reverse(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.WriteCommandWord(0x20,0x10)
        logging.Info("Drive reverse direction (CommandWord)")

    ###############################################################
    def setNormalStopbit(self,):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000001)
        logging.Info("Drive Command bit NormalStop set")

    def clearNormalStopbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000001)
        logging.Info("Drive Command bit NormalStop cleared")

    ###############################################################
    def setStartbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000002)
        logging.Info("Drive Command bit Start set")

    def clearStartbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000002)
        logging.Info("Drive Command bit Start cleared")

    ###############################################################
    def setJog1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000004)
        logging.Info("Drive Command bit Jog1 set")

    def clearJog1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000004)
        logging.Info("Drive Command bit Jog1 cleared")

    ###############################################################
    def setClearFaultbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000008)
        logging.Info("Drive Command bit ClearFault set")

    def clearClearFaultbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000008)
        logging.Info("Drive Command bit ClearFault cleared")

    ###############################################################
    def setForwardbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000010)
        logging.Info("Drive Command bit Forward set")

    def clearForwardbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000010)
        logging.Info("Drive Command bit Forward cleared")

    ###############################################################
    def setReversebit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000020)
        logging.Info("Drive Command bit Reverse set")

    def clearReversebit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000020)
        logging.Info("Drive Command bit Reverse cleared")

    ###############################################################
    def setManualbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000040)
        logging.Info("Drive Command bit Manual set")

    def clearManualbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000040)
        logging.Info("Drive Command bit Manual cleared")

    ###############################################################
    def setUseAccelTime1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000100)
        logging.Info("Drive Command bit UseAccelTime1 set")

    def clearUseAccelTime1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000100)
        logging.Info("Drive Command bit UseAccelTime1 cleared")

    ###############################################################
    def setUseAccelTime2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000200)
        logging.Info("Drive Command bit UseAccelTime2 set")

    def clearUseAccelTime2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000200)
        logging.Info("Drive Command bit UseAccelTime2 cleared")

    ###############################################################
    def setUseDecelTime1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000400)
        logging.Info("Drive Command bit UseDecelTime1 set")

    def clearUseDecelTime1bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000400)
        logging.Info("Drive Command bit UseDecelTime1 cleared")

    ###############################################################
    def setUseDecelTime2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00000800)
        logging.Info("Drive Command bit UseDecelTime2 set")

    def clearUseDecelTime2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00000800)
        logging.Info("Drive Command bit UseDecelTime2 cleared")

    ###############################################################
    def setRefASelectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00001000)
        logging.Info("Drive Command bit Ref A Select set")

    def clearRefASelectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00001000)
        logging.Info("Drive Command bit Ref A Select cleared")

    ###############################################################
    def setRefBSelectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00002000)
        logging.Info("Drive Command bit Ref B Select set")

    def clearRefBSelectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00002000)
        logging.Info("Drive Command bit Ref B Select cleared")

    ###############################################################
    def setRef3Selectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00004000)
        logging.Info("Drive Command bit Ref3 Select set")

    def clearRef3Selectbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00004000)
        logging.Info("Drive Command bit Ref3 Select cleared")

    ###############################################################
    def setCoastStopbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00010000)
        logging.Info("Drive Command bit CoastStop set")

    def clearCoastStopbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00010000)
        logging.Info("Drive Command bit CoastStop cleared")

    ###############################################################
    def setCurrentLimitStopbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00020000)
        logging.Info("Drive Command bit CurrentLimitStop set")

    def clearCurrentLimitStopbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00020000)
        logging.Info("Drive Command bit CurrentLimitStop cleared")

    ###############################################################
    def setRunbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00040000)
        logging.Info("Drive Command bit Run set")

    def clearRunbit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00040000)
        logging.Info("Drive Command bit Run cleared")

    ###############################################################
    def setJog2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(True, 0x00080000)
        logging.Info("Drive Command bit Jog2 set")

    def clearJog2bit(self):
        """[placehlolder for short description]

        [placeholder for long description]

        :return:
        """

        self.CommandWordParser(False, 0x00080000)
        logging.Info("Drive Command bit Jog2 cleared")

    ######################################################################################################
    def WaitToBeAtRefference(self, timeoutS = 0.1):
        """[placehlolder for short description]

        [placeholder for long description]

        :param timeoutS:
        :return:
        """

        logging.Info("Wait drive to be at refference!")
        while not self.isAtSpeed():
            time.sleep(timeoutS)
        return

    def WaitToBeAtZeroSpeed(self, checkinterval = 0.1):
        """[placehlolder for short description]

        [placeholder for long description]

        :param checkinterval:
        :return:
        """

        logging.Info("Wait drive to be at zero speed!")
        while not self.isAtZeroSpeed():
            time.sleep(checkinterval)
        return
