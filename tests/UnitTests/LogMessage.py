import unittest

import loggingLibrary.loggingdt as logging
import Configuration.TestEnvironment as TestEnviornment

TestEnviornment.ENABLE_LOG_PRINTING = False

class TestLogMessage(unittest.TestCase):

    def test_correct_input(self):
        result = logging.LogMessage("Test Message", "Info")
        self.assertEqual(result, True)
        self.assertEqual(TestEnviornment.LOG_RECORDS[0][:6], '[INFO]')
        self.assertEqual(TestEnviornment.LOG_RECORDS[0][32:], 'Test Message')

        result = logging.LogMessage("Test Message", "Debug")
        self.assertEqual(result, True)
        self.assertEqual(TestEnviornment.LOG_RECORDS[1][:6], '[DBUG]')

        result = logging.LogMessage("Test Message", "Warning")
        self.assertEqual(result, True)
        self.assertEqual(TestEnviornment.LOG_RECORDS[2][:6], '[WARN]')

        result = logging.LogMessage("Test Message", "Error")
        self.assertEqual(result, True)
        self.assertEqual(TestEnviornment.LOG_RECORDS[3][:6], '[ERRO]')

        result = logging.LogMessage("Test Message")
        self.assertEqual(result, True)
        self.assertEqual(TestEnviornment.LOG_RECORDS[4][:6], '[INFO]')

    def test_wrong_input(self):
        with self.assertRaises(ValueError):
            logging.LogMessage("Test Message", "DEbug")
        with self.assertRaises(ValueError):
            logging.LogMessage("Test Message", "Warnigf")
        with self.assertRaises(ValueError):
            logging.LogMessage("Test Message", Warning)
        with self.assertRaises(ValueError):
            logging.LogMessage("Test Message", 'info')

if __name__ == '__main__':
    unittest.main()