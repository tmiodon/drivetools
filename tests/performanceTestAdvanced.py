from drivesLibrary.driveClass import driveBase
import time
import numpy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import os, glob

from datetime import datetime

##################################################
# Calculating basic statistic info
##################################################
def CalculateStatistics(dataInputList):
    data = {}
    data['mean'] = numpy.mean(dataInputList)
    data['median'] = numpy.median(dataInputList)
    data['min'] = dataInputList[0]
    data['max'] = dataInputList[len(dataInputList)-1]
    return data


##################################################
# Test preparation
##################################################

# Control variables
TestDriveTools = True
TestTAF = True
collectData = True

collectForReadParameter = True
collectForWriteParameter = True
collectForPcccCommand = True
collectForPcccStatus = True

generateSeparateHistogram = True
generateSeparateBoxplot = True
generateSeparateViolinplot = False
generateComparisonBoxplot = True

numberOfSamples = 100
numberOfBins = 50

# Define test timestamp
testTime = datetime.utcnow().strftime('%Y_%m_%d__%H_%M_%S.%f')[:-6]
testDirectory = os.getcwd()
# Create report directory
accesRights = 0o755
os.makedirs('PerformanceTestResults/' + str(testTime), accesRights)

# Dedtermine number of ploted variables
n = 0
if collectForPcccStatus:
    n = n + 1
if collectForPcccCommand:
    n = n + 1
if collectForWriteParameter:
    n = n + 1
if collectForReadParameter:
    n = n + 1


################################################################################
# DriveTools part
################################################################################

if collectData:
##################################################
# Data collection procedure
##################################################
    files = glob.glob('PerformanceTestData/*')
    for f in files:
        os.remove(f)

    drive = driveBase('192.168.1.11', connection=False, PCCC=True)

    # Collect data for ReadParameter function
    if collectForReadParameter:
        readParameter = []
        for i in range(numberOfSamples):
            start = time.clock()
            drive.ReadParameter('P0 Access Level')
            stop = time.clock()
            readParameter.append(1000*(stop - start))
        file = open('PerformanceTestData/dtReadParameterResults.txt', 'w')
        for record in readParameter:
            file.writelines(str(record) + '\n')
        file.close()

    # Collect data for ReadParameter function
    if collectForWriteParameter:
        writeParameter = []
        for i in range(numberOfSamples):
            start = time.clock()
            drive.WriteParameter(2, 'P0 Access Level')
            stop = time.clock()
            writeParameter.append(1000*(stop - start))
        file = open('PerformanceTestData/dtWriteParameterResults.txt', 'w')
        for record in writeParameter:
            file.writelines(str(record) + '\n')
        file.close()

    # Send 1000 ClearFaults command
    if collectForPcccCommand:
        pcccCommand = []
        for i in range(numberOfSamples):
            start = time.clock()
            drive.ClearFaults()
            stop = time.clock()
            pcccCommand.append(1000*(stop - start))
        file = open('PerformanceTestData/dtPcccCommandResults.txt', 'w')
        for record in pcccCommand:
            file.writelines(str(record) + '\n')
        file.close()

    # Send 1000 isReady command
    if collectForPcccStatus:
        pcccStatus = []
        for i in range(numberOfSamples):
            start = time.clock()
            drive.isReady()
            stop = time.clock()
            pcccStatus.append(1000*(stop - start))
        file = open('PerformanceTestData/dtPcccStatusResults.txt', 'w')
        for record in pcccStatus:
            file.writelines(str(record) + '\n')
        file.close()

    drive.Disconnect(PCCC=False)

    ##################################################
    # I want to collect time data for different command
    # types so then I can analyze that data
    ##################################################
    tafTestDirectory = testDirectory + '/TAF'
    result = os.system(
        'cd ' + tafTestDirectory + ' & START TAFCommand.lnk /R ' + tafTestDirectory + '/solution.taf')
    time.sleep(60)
################################################################################
# For the purpose of generating diagrams we load data from files
################################################################################
if TestDriveTools:
    if collectForPcccCommand:
        pcccCommandResultsFile = open('PerformanceTestData/dtPcccCommandResults.txt', 'r')
        dtPcccCommandResults = pcccCommandResultsFile.read().splitlines()
        dtPcccCommandResults = map(float, dtPcccCommandResults)

    if collectForPcccStatus:
        pcccStatusResultsFile = open('PerformanceTestData/dtPcccStatusResults.txt', 'r')
        dtPcccStatusResults = pcccStatusResultsFile.read().splitlines()
        dtPcccStatusResults = map(float, dtPcccStatusResults)
    if collectForReadParameter:
        readParameterResultsFile = open('PerformanceTestData/dtReadParameterResults.txt', 'r')
        dtReadParameterResults = readParameterResultsFile.read().splitlines()
        dtReadParameterResults = map(float, dtReadParameterResults)
    if collectForWriteParameter:
        writeParameterResultsFile = open('PerformanceTestData/dtWriteParameterResults.txt', 'r')
        dtWriteParameterResults = writeParameterResultsFile.read().splitlines()
        dtWriteParameterResults = map(float, dtWriteParameterResults)

if TestTAF:
    if collectForPcccCommand:
        pcccCommandResultsFile = open('PerformanceTestData/tafPcccCommandResults.txt', 'r')
        tafPcccCommandResults = pcccCommandResultsFile.read().splitlines()
        tafPcccCommandResults = map(int, tafPcccCommandResults)
    if collectForPcccStatus:
        pcccStatusResultsFile = open('PerformanceTestData/tafPcccStatusResults.txt', 'r')
        tafPcccStatusResults = pcccStatusResultsFile.read().splitlines()
        tafPcccStatusResults = map(int, tafPcccStatusResults)
    if collectForReadParameter:
        readParameterResultsFile = open('PerformanceTestData/tafReadParameterResults.txt', 'r')
        tafReadParameterResults = readParameterResultsFile.read().splitlines()
        tafReadParameterResults = map(int, tafReadParameterResults)
    if collectForWriteParameter:
        writeParameterResultsFile = open('PerformanceTestData/tafWriteParameterResults.txt', 'r')
        tafWriteParameterResults = writeParameterResultsFile.read().splitlines()
        tafWriteParameterResults = map(int, tafWriteParameterResults)



##################################################
# Compute and plot histogram for drivetools
##################################################
if generateSeparateHistogram and TestDriveTools:
    fig1, ax = plt.subplots(nrows=n, ncols=1, figsize=(9,9), sharey='all')
    i = 0
    if collectForReadParameter:
        ax[i].hist(dtReadParameterResults, bins=numberOfBins)
        ax[i].set_title('Read Parameter')
        i = i + 1
    if collectForWriteParameter:
        ax[i].hist(dtWriteParameterResults, bins=numberOfBins)
        ax[i].set_title('Write Parameter')
        i = i + 1
    if collectForPcccCommand:
        ax[i].hist(dtPcccCommandResults, bins=numberOfBins)
        ax[i].set_title('PCCC Command')
        i = i + 1
    if collectForPcccStatus:
        ax[i].hist(dtPcccStatusResults, bins=numberOfBins)
        ax[i].set_title('PCCC Status')
        i = i + 1
    pylab.savefig('PerformanceTestResults/' + str(testTime) + '/dt_histograms.png')

##################################################
# Computing and ploting boxplot for drivetools
##################################################
if generateSeparateBoxplot and TestDriveTools:
    fig2, ax = plt.subplots(nrows=1, ncols=n, sharey='all', figsize=(9,9))
    i = 0
    if collectForReadParameter:
        ax[i].boxplot(dtReadParameterResults, notch=True)
        ax[i].set_title('Read Parameter')
        i = i + 1
    if collectForWriteParameter:
        ax[i].boxplot(dtWriteParameterResults, notch=True)
        ax[i].set_title('Write Parameter')
        i = i + 1
    if collectForPcccCommand:
        ax[i].boxplot(dtPcccCommandResults, notch=True)
        ax[i].set_title('PCCC Command')
        i = i + 1
    if collectForPcccStatus:
        ax[i].boxplot(dtPcccStatusResults, notch=True)
        ax[i].set_title('PCCC Status')
    pylab.savefig('PerformanceTestResults/' + str(testTime) + '/dt_boxplots.png')

##################################################
# Computing and ploting violinplot for drivetools
##################################################
if generateSeparateViolinplot and TestDriveTools:
    fig3, ax = plt.subplots(nrows=1, ncols=n, sharey='all', figsize=(9,9))
    i = 0
    if collectForReadParameter:
        ax[i].violinplot(dtReadParameterResults, showmedians=True)
        ax[i].set_title('Read Parameter')
        i = i + 1
    if collectForWriteParameter:
        ax[i].violinplot(dtWriteParameterResults, showmedians=True)
        ax[i].set_title('Write Parameter')
        i = i + 1
    if collectForPcccCommand:
        ax[i].violinplot(dtPcccCommandResults, showmedians=True)
        ax[i].set_title('PCCC Command')
        i = i + 1
    if collectForPcccStatus:
        ax[i].violinplot(dtPcccStatusResults, showmedians=True)
        ax[i].set_title('PCCC Status')
    pylab.savefig('PerformanceTestResults/' + str(testTime) + '/dt_violinplots.png')


##################################################
# Computing and ploting boxplots for TAF
##################################################
if generateSeparateBoxplot and TestTAF:
    fig4, ax = plt.subplots(nrows=1, ncols=n, sharey='all', figsize=(9,9))
    i = 0
    if collectForReadParameter:
        ax[i].boxplot(tafReadParameterResults, notch=True)
        ax[i].set_title('Read Parameter')
        i = i + 1
    if collectForWriteParameter:
        ax[i].boxplot(tafWriteParameterResults, notch=True)
        ax[i].set_title('Write Parameter')
        i = i + 1
    if collectForPcccCommand:
        ax[i].boxplot(tafPcccCommandResults, notch=True)
        ax[i].set_title('PCCC Command')
        i = i + 1
    if collectForPcccStatus:
        ax[i].boxplot(tafPcccStatusResults, notch=True)
        ax[i].set_title('PCCC Status')
    pylab.savefig('PerformanceTestResults/' + str(testTime) + '/taf_boxplots.png')

##################################################
# Computing and ploting comparison boxplots
##################################################
if generateComparisonBoxplot and TestTAF and TestDriveTools:
    if collectForReadParameter:
        readParameterArray = [dtReadParameterResults, tafReadParameterResults]
        fig, ax = plt.subplots(figsize=(8,12))
        ax.set_title('Read Message')
        ax.boxplot(readParameterArray, labels=['Python', 'TAF'])
        fig.savefig('PerformanceTestResults/' + str(testTime) + '/read_parameter_boxplots.png')
    if collectForWriteParameter:
        writeParameterArray = [dtWriteParameterResults, tafWriteParameterResults]
        fig, ax = plt.subplots(figsize=(8,12))
        ax.set_title('Write Message')
        ax.boxplot(writeParameterArray, labels=['Python', 'TAF'])
        fig.savefig('PerformanceTestResults/' + str(testTime) + '/write_parameter_boxplots.png')
    if collectForPcccCommand:
        pcccCommandArray = [dtPcccCommandResults, tafPcccCommandResults]
        fig, ax = plt.subplots(figsize=(8,12))
        ax.set_title('Pccc Command')
        ax.boxplot(pcccCommandArray, labels=['Python', 'TAF'])
        fig.savefig('PerformanceTestResults/' + str(testTime) + '/pccc_command_boxplots.png')
    if collectForPcccStatus:
        pcccStatusArray = [dtPcccStatusResults, tafPcccStatusResults]
        fig, ax = plt.subplots(figsize=(8,12))
        ax.set_title('Pccc Status')
        ax.boxplot(pcccStatusArray, labels=['Python', 'TAF'])
        fig.savefig('PerformanceTestResults/' + str(testTime) + '/pccc_status_boxplots.png')


##################################################
# Not sure whether we want to do that
##################################################
# if TestDriveTools:
#     header =  "Type of message \t| Mean \t\t| Median \t| Min \t\t| Max"
#     line = '--------------------+-----------+-----------+-----------+-----------'
#     if collectForReadParameter:
#         readParameterRow = str("%.6f" % dataForReadParameter['mean']) + ' \t| ' + str("%.6f" % dataForReadParameter['median']) + ' \t| ' + str("%.6f" % dataForReadParameter['min']) + ' \t| ' + str("%.6f" % dataForReadParameter['max'])
#     if collectForWriteParameter:
#         writeParameterRow = str("%.6f" % dataForWriteParameter['mean']) + ' \t| ' + str("%.6f" % dataForWriteParameter['median']) + ' \t| ' + str("%.6f" % dataForWriteParameter['min']) + ' \t| ' + str("%.6f" % dataForWriteParameter['max'])
#     if collectForPcccCommand:
#         pcccCommandRow = str("%.6f" % dataForPcccCommand['mean']) + ' \t| ' + str("%.6f" % dataForPcccCommand['median']) + ' \t| ' + str("%.6f" % dataForPcccCommand['min']) + ' \t| ' + str("%.6f" % dataForPcccCommand['max'])
#     if collectForPcccStatus:
#         pcccStatusRow = str("%.6f" % dataForPcccStatus['mean']) + ' \t| ' + str("%.6f" % dataForPcccStatus['median']) + ' \t| ' + str("%.6f" % dataForPcccStatus['min']) + ' \t| ' + str("%.6f" % dataForPcccStatus['max'])
#
#     file = open('PerformanceTestResults/' + str(testTime) + '/log.txt', 'w')
#     file.write(str(testTime) + ' - TEST INFO AND RESULTS\n\n')
#     file.write("Sample number = " + str(numberOfSamples) + '\n')
#     if includeDescription:
#         file.write('Description:\n')
#         file.write(description + '\n')
#     file.write('\n' + header + '\n')
#     file.write(line + '\n')
#     if collectForReadParameter:
#         file.write("Read Parameter \t\t| " + readParameterRow + '\n')
#     if collectForWriteParameter:
#         file.write("Write Parameter \t| " + writeParameterRow + '\n')
#     if collectForPcccCommand:
#         file.write("Pccc Command \t\t| " + pcccCommandRow + '\n')
#     if collectForPcccStatus:
#         file.write("Pccc Status \t\t| " + pcccStatusRow + '\n')
#     file.close()