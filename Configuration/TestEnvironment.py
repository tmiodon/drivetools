# -------------------------------------------------------------------
#
# FILE NAME:
# VERSION:
# CONTRIBUTORS:
#
# START DATE:
#
# DESCRIPTION:
#
# -------------------------------------------------------------------

import os
import sys
from datetime import datetime

import loggingLibrary.loggingdt as logging

# Generat test informations
TESTER = None                   # placeholder
INPUT_FIRMWARE_VERSION = None         # placeholder
TEST_DATE = None                # placeholder
RESULT = False
ABORT_EXECUTION_ON_EXCEPTION = False

# Path variables
CURRENT_TEST_DIRECTORY = os.getcwd()
TEMP_LOG_DIRECTORY = 'C:/TestLogs/log.txt'       # placeholder


# Logging variables
LOG_RECORDS = []
ENABLE_LOG_PRINTING = True

PASS_COUNT = None
FAIL_COUNT = None
ERROR_COUNT = None
WARNING_COUNT = None

# Time variables
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
START_TIME = None
STOP_TIME = None
TEST_EXECUTION_TIME = None

def SetTestEnviornment():
    global START_TIME
    global PASS_COUNT
    global FAIL_COUNT
    global ERROR_COUNT
    global WARNING_COUNT
    global LOG_RECORDS

    START_TIME = datetime.utcnow().strftime(TIME_FORMAT)

    PASS_COUNT = 0
    FAIL_COUNT = 0
    ERROR_COUNT = 0
    WARNING_COUNT = 0

    LOG_RECORDS = []

    # Log path creation if not exists
    if os.path.isdir("C:/TestLogs") is not True:
        os.mkdir("C:/TestLogs")

def StopTest():
    global START_TIME
    global STOP_TIME
    global TEST_EXECUTION_TIME
    STOP_TIME = datetime.utcnow().strftime(TIME_FORMAT)
    TEST_EXECUTION_TIME = datetime.strptime(STOP_TIME, TIME_FORMAT) - datetime.strptime(START_TIME, TIME_FORMAT)
    logging.GenerateReport()

def AbortExecutionOnException():
    global ABORT_EXECUTION_ON_EXCEPTION
    ABORT_EXECUTION_ON_EXCEPTION = True
    return

def DoNotAbortExecutionOnException():
    global ABORT_EXECUTION_ON_EXCEPTION
    ABORT_EXECUTION_ON_EXCEPTION = False
    return

def ExitTest(message="Execution aborted due to exception."):
    StopTest()
    sys.exit(message)