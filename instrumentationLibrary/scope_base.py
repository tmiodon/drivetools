# -------------------------------------------------------------------
#
# FILE NAME:
# VERSION:
# CONTRIBUTORS:
# START DATE:
#
# DESCRIPTION:
#
# NOTES:
#
# -------------------------------------------------------------------

import socket

import loggingLibrary.loggingdt as logging


class Scope:
    """Base class for scope instruments.

    **placeholder**

    """

    TEK_PORT = 4000
    TEK_END_LINE = '\n'

    def __init__(self):
        self.ip_address = ''
        self.connected = False
        self.manufacturer = ''
        self.model = ''
        self.socket = None
        return

    def Set(self, message):

        if isinstance(message, basestring) is False:
            logging.Error('Message is not string!')
            return
        try:
            self.socket.sendall(message + self.TEK_END_LINE)
            logging.Info('Message sent successfully.')
        except socket.error, exc:
            logging.Error('Message failed: ' + str(exc))

    def Query(self, message):

        if isinstance(message, basestring) is False:
            logging.Error('Message is not string!')
            return
        try:
            self.socket.sendall(message + self.TEK_END_LINE)
            logging.Info('Message sent successfully.')
            return self.socket.recv(512)
        except socket.error, exc:
            logging.Error('Message failed: ' + str(exc))
        return

    def Connect(self, ConnectionString='', ConnectionMode='Ethernet', IdQuerry=False, Reset=False, Trace=False):

        if self.connected is True:
            logging.Info('Instrument already connected.')
            return

        if ConnectionMode == 'Ethernet':
            try:
                self.ip_address = ConnectionString
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((ConnectionString, self.TEK_PORT))
                self.connected = True
                logging.Info('Connected to instrument ' + self.model + ' on: ' + ConnectionString)
            except socket.error, exc:
                logging.Error("Connection failed: " + str(exc))

        # Not yet implemented!
        elif ConnectionMode == "USB":
            return
        else:
            logging.Error("Wrong connection mode selected!")

        self.socket.sendall("*IDN?\n")
        scope_identity = self.socket.recv(512)
        scope_identity = scope_identity.split(',')
        self.manufacturer = scope_identity[0]
        self.model = scope_identity[1]
        return

    def Disconnect(self):


        if self.connected is False:
            logging.Info('Instrument already disconnected.')
            return

        try:
            self.socket.close()
            self.connected = False
            logging.Info('Instrument disconnected successfully.')
        except socket.error, exc:
            logging.Error("Connection failed: " + str(exc))
        finally:
            return

    def AutoSet(self):
        try:
            self.socket.sendall('AUTOSet EXECute' + self.TEK_END_LINE)
        except socket.error, exc:
            logging.Error("Connection failed: " + str(exc))

    def AcquisitionRecord(self, StartTime=1, MinRecordLenght=5000, TimePerRecord=1):
        return

    # Originally this actor is either set or read. Perhaps splitting int two
    # separate methods would be better.
    def AcquisitionType(self, Mode='Read', AcquisitionType='Normal'):
        return

    # Same case as above.
    def ContinuousAcquisition(self, Mode='Read', IsContinuous='False'):
        return

    def Interpolation(self, Mode='Read', Interpolation='SineX'):
        return

    def NumberOfAverages(self, Mode='Read', NumberOfAverages=16):
        return

    def NumberOfEnvelopes(self, Mode='Read', NumberOfEnvelopes=10):
        return

    # This needs to return 'int'.
    def ActualRecordLength(self):
        return

    # This needs to return 'int'.
    def ActualSampleMode(self):
        return

    # This needs to return 'int'.
    def ActualSampleRate(self):
        return

    # Returns probe attenuation value.
    def AutoProbeSenseValue(self, Channel=1):
        return

    def EdgeTriggerSource(self, TriggerLevel=0, TriggerSlope='Rise', TriggerSource='Ch1'):
        return

    def GlitchTriggerSource(self, Condition='LessThan', Level=0, Polarity='Positive', Source='Ch1', Width='2ns'):
        return

    def RuntTriggerSource(self, HighTreshold=1.2, LowTreshold=0.8, Polarity='Positive', Source='Ch1'):
        return

    # Return two variables
    def Trigger(self, Mode='Read', TriggerHoldOffTime='1.5us', Type='Edge'):
        return

    def TriggerCoupling(self, Mode='Read', Coupling='DC'):
        return

    def TriggerMode(self, TriggerMode='Auto'):
        return

    def WidthTriggerSource(self, TriggerLevel=0, COndition='Inside', LowerLimit='2ns',
                           UpperLimit='2ns', Polarity='Positive', Source='Ch1'):
        return

    def Cannel(self, Channel='Ch1', Coupling='DC', Enabled=False, Offset=0, Attenuation=1, Range=10):
        return

    def ChannelCharacteristics(self, Channel='Ch1', InputFreqMax='500MHz', InputImpedance='1MOhm'):
        return

    # Returns Acquisition Status
    def AbortMeasurement(self):
        return

    # Returns Acquisition Status as well, but without aborting.
    def AcquisitionStatus(self):
        return

    # This returns 3 different variables, InitialX, Waveform and X Increment.
    def CaptureWaveform(self, Channel='Ch1', EnableChannelIfDisabled=False, MaxTime='1s'):
        return

    # This one returns 4 variables...
    def ReadMinMaxWaveform(self, Channel='Ch1', EnableChannelIfDisabled=False, MaxTime='1s'):
        return

    def ReferenceLevels(self, High=0, Low=0, Mid=0):
        return

    # Returns one variable.
    def WaveformMeasurements(self, channel='Ch1', EnableChannelIfDisabled=False, MaxTime='10ms',
                             MeasFunction='Risetime'):
        return

    # This one neither takes arguments or return result.
    def Disable(self):
        return

    # Returns two variables, error code & error message.
    def ErrorQuery(self):
        return

    # Returns channel name.
    def GetChannelName(self, Channel=1):
        return

    # This one neither takes arguments or return result.
    def InvalidateAllAttributes(self):
        return

    # This returns if element is valid as bool output.
    def IsValidWaveformElement(self, Element=0):
        return

    # Resets scope to known state. No arguments and returns.
    def Reset(self):
        return

    # Resets, but also defaults.
    def ResetWithDefaults(self):
        return

    # Not sure if worth implementing.
    # Returns drive and scope revision.
    def RevisionQuery(self):
        return

    # Returns test message and test rerult. Not sure what kind of test this is.
    def SelfTest(self):
        return

    def TimeBase(self, time_base):

        time_unit = ''.join(c for c in time_base if  c.isalpha())
        time_value = ''.join(c for c in time_base if  c.isdigit())

        _time_base = time_base.replace(time_unit, '').replace(time_value, '')

        if _time_base != '':
            logging.Error('Time base input is incorrect!')
            return

        if time_unit == 's':
            new_time_base = 'e-0'
        elif time_unit == 'ms':
            new_time_base = 'e-3'
        elif time_unit == 'us':
            new_time_base = 'e-6'
        elif time_unit == 'ns':
            new_time_base = 'e-9'
        else:
            logging.Error('Wrong time units!')
            return

        value_to_write = time_value + new_time_base

        try:
            self.socket.sendall('HOR:MODE:SCA ' + value_to_write + self.TEK_END_LINE)
            logging.Info(self.model + " scope time base set to: " + time_base)
        except socket.error, exc:
            logging.Error("Connection failed: " + str(exc))
